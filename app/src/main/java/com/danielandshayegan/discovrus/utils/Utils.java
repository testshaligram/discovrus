package com.danielandshayegan.discovrus.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.EditText;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.reactivex.disposables.CompositeDisposable;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getGeneralSettings;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getLikeListData;
import static com.google.android.gms.common.util.IOUtils.copyStream;

public class Utils {

    public static final String CLIENT_ID = "cd88ba4ed5be41878c08d4a8cedf5323";
    public static final String CLIENT_SECRATE_ID = "2bc39e1b55ab4af19a7df25d9419a221";
    public static final String REDIRECT_URI = "http://www.shaligraminfotech.com/";
    public static String fontName = "";
    public static double latitude;
    public static double longitude;

    public static boolean FROM_SPLASH = false;

    public static int selectedPosition;
    public static CompositeDisposable compositeDisposable;

    public static final String MM_DD_YYYY = "MMM dd, yyyy";
    public static final String DD_MMM = "dd MMM";
    public static final String DD_MMMM = "dd MMMM";
    public static final String HH_MM = "HH:mmm";

    public static boolean isNetworkAvailable(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        boolean is = (netInfo != null && netInfo.isConnectedOrConnecting());
        return is;
    }

    public static String convertLikes(int likes) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        if (likes >= 1000) {
            Double division = Double.valueOf((likes * 1.00) / 1000);
            return String.valueOf(formatter.format(division)) + "k";
        } else if (likes >= 1000000) {
            Double division = Double.valueOf((likes * 1.00) / 1000000);
            return String.valueOf(formatter.format(division)) + "m";
        } else {
            return String.valueOf(likes);
        }
    }

    public static String convertToOriginalCount(String likes) {
        if (likes.contains("k")) {
            Double likeToCOnvert = Double.valueOf(likes.toString().replaceAll("k", ""));
            return String.valueOf(likeToCOnvert * 1000);
        } else if (likes.contains("m")) {
            Double likeToCOnvert = Double.valueOf(likes.toString().replaceAll("m", ""));
            return String.valueOf(likeToCOnvert * 1000000);
        } else {
            return String.valueOf(likes);
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static void saveImageToExternal(Context me, String imgName, Bitmap bm) throws IOException {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        path.mkdirs();
        File imageFile = new File(path, imgName + ".jpg"); // Imagename.jpg
        FileOutputStream out = new FileOutputStream(imageFile);
        try {
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out); // Compress Image
            out.flush();
            out.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(me, new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });
        } catch (Exception e) {
            throw new IOException();
        }
    }

    public static void saveVideoToExternal(Context me, Uri videoUri) throws IOException {
        try {
            String Video_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
            File storeDirectory = new File(Video_DIRECTORY);

            try {
                if (storeDirectory.exists() == false) {
                    storeDirectory.mkdirs();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            File storeDirectory12 = new File(storeDirectory, System.currentTimeMillis() + ".mp4");
            InputStream inputStream = me.getContentResolver().openInputStream(videoUri);
            FileOutputStream fileOutputStream = new FileOutputStream(storeDirectory12);
            copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();

            MediaScannerConnection.scanFile(me, new String[]{storeDirectory.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });
        } catch (FileNotFoundException e) {
            Log.e("Exception", "" + e);
        } catch (IOException e) {
            Log.e("Exception", "" + e);
        }
    }

    public static void callGetGeneralSettings(Context me, int userId, final CallbackTask callBack) {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(me).
                        create(WebserviceBuilder.class).
                        getGeneralSettings(userId)
                , getCompositeDisposable(), getGeneralSettings, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onSuccess(o);
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onFailure(throwable);
                    }
                });
    }

    public static CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

   /* public static GeneralSettingsData callGetGeneralSettings(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        Gson gson = new Gson();
        Type type = new TypeToken<GeneralSettingsData>() {
        }.getType();

        return gson.fromJson(prefs.getString("GeneralSettings", ""), type);
    }

    public static void setGeneralSettingsData(Context context, GeneralSettingsData mData) {
        Gson gson = new Gson();
        Type type = new TypeToken<GeneralSettingsData>() {
        }.getType();
        String strObject = gson.toJson(mData, type);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString("GeneralSettings", strObject).apply();
    }*/


    public static String getGeneralSettingsData(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

/*        Gson gson = new Gson();
        Type type = new TypeToken<GeneralSettingsData>() {
        }.getType();*/

        //return gson.fromJson(prefs.getString("GeneralSettingsObj", ""), type);
        return prefs.getString("GeneralSettingsObj", "");
    }

    public static void setGeneralSettingsData(Context context, JSONObject mData) {
/*        Gson gson = new Gson();
        Type type = new TypeToken<GeneralSettingsData>() {
        }.getType();
        String strObject = gson.toJson(mData, type);*/

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString("GeneralSettingsObj", mData.toString()).apply();
    }

    public static HashMap<String, String> getGeneralSettingsDataKeys(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>() {
        }.getType();

        return gson.fromJson(prefs.getString("GeneralSettingsKeys", ""), type);
    }

    public static void setGeneralSettingsDataKeys(Context context, HashMap<String, String> settingsList) {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>() {
        }.getType();
        String strObject = gson.toJson(settingsList, type);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString("GeneralSettingsKeys", strObject).apply();
    }

    public static void getLikeListData(Context me, int userId, final CallbackTask callBack) {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(me).
                        create(WebserviceBuilder.class).
                        getLikeListData(App_pref.getAuthorizedUser(me).getData().getUserId(), userId)
                , getCompositeDisposable(), getLikeListData, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onSuccess(o);
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onFailure(throwable);
                    }
                });
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    Log.e("Network", "NETWORK NAME: " + networkInfo.getTypeName());
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Log.e("Network", "NETWORK NAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void setSelectionEnd(EditText et) {
        et.setSelection(et.getText().length());
    }

    public static String getAddressFromLatLong(Context me, double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(me, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.get(0).getAddressLine(0) != null) {
                return addresses.get(0).getAddressLine(0);
            } else {
                return addresses.get(0).getAddressLine(0) + addresses.get(0).getLocality() + " " +
                        addresses.get(0).getAdminArea() + " " + addresses.get(0).getCountryName();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getformatteDate(String inputDate) {
        if (inputDate != null) {
            Date parsed = null;
            String outputDate = "";

            SimpleDateFormat df_input = new SimpleDateFormat(MM_DD_YYYY, Locale.getDefault());
            SimpleDateFormat df_output = new SimpleDateFormat(DD_MMM, Locale.getDefault());

            try {
                parsed = df_input.parse(inputDate);
                outputDate = df_output.format(parsed);
            } catch (ParseException e) {
                Log.d("date", e.getMessage() + " formateDateFromstring");
            }
            return outputDate;
        } else {
            return "";
        }
    }

    public static String getformatteDate1(String inputDate) {
        if (inputDate != null) {
            Date parsed = null;
            String outputDate = "";

            SimpleDateFormat df_input = new SimpleDateFormat(MM_DD_YYYY, Locale.getDefault());
            SimpleDateFormat df_output = new SimpleDateFormat(DD_MMMM, Locale.getDefault());

            try {
                parsed = df_input.parse(inputDate);
                outputDate = df_output.format(parsed);
            } catch (ParseException e) {
                Log.d("date", e.getMessage() + " formateDateFromstring");
            }
            return outputDate;
        } else {
            return "";
        }
    }

    public static String getFormattedPrice(String amount) {
        return getFormattedPrice(amount, true);
    }

    public static String getFormattedPrice(double amount) {
        return getFormattedPrice(amount, true);
    }

    public static String getFormattedPrice(String amount, boolean appendSymbol) {
        if (amount != null && amount.length() > 0) {
            try {
                double value = Double.parseDouble(amount);
                return getFormattedPrice(value, appendSymbol);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return "";
        }
    }

    public static String getFormattedPrice(double amount, boolean appendSymbol) {
        DecimalFormat formatter = new DecimalFormat("0.00");
        return appendSymbol ? "£".concat(" ").concat(formatter.format(amount)) : formatter.format(amount);
    }
}