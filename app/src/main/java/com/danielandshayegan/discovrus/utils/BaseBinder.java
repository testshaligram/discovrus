package com.danielandshayegan.discovrus.utils;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.danielandshayegan.discovrus.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.danielandshayegan.discovrus.webservice.APIs.BASE_IMAGE_PATH;

public class BaseBinder {

    @BindingAdapter({"app:setImageUri", "app:setImageUrl"})
    public static void setImage(ImageView iv, Uri imageUri, String Url) {
        if (Url == null || Url.length() == 0) {
            setImageUri(iv, imageUri);
        } else {
            setImageUrl(iv, Url);
        }
    }

    @BindingAdapter({"app:setImageUri"})
    public static void setImageUri(ImageView iv, Uri imageUri) {
        if (imageUri != null)
            Glide.with(iv.getContext())
                    .load(imageUri.getPath())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Log.e("glide", "exception");
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder_image)
                            .error(R.drawable.placeholder_image)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .transforms(new CenterCrop(), new RoundedCorners(36)))
                    .transition(withCrossFade(300))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.placeholder_image);
    }

    @BindingAdapter({"app:setImageUrl"})
    public static void setImageUrl(ImageView iv, String imageUrl) {
        if (imageUrl != null)
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + imageUrl)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder_image)
                            .error(R.drawable.placeholder_image)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .transforms(new CenterCrop(), new RoundedCorners(36)))
                    .transition(withCrossFade(300))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.placeholder_image);
    }

    @BindingAdapter({"app:setUserImageUri", "app:setUserImageUrl"})
    public static void setUserImage(ImageView iv, Uri imageUri, String Url) {
        if (Url == null || Url.length() == 0) {
            setUserImageUri(iv, imageUri);
        } else {
            setUserImageUrl(iv, Url);
        }
    }

    @BindingAdapter({"app:setUserImageUri"})
    public static void setUserImageUri(ImageView iv, Uri imageUri) {
        if (imageUri != null)
            Glide.with(iv.getContext())
                    .load(imageUri.getPath())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_placeholder)
                            .error(R.drawable.user_placeholder))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.user_placeholder);
    }

    @BindingAdapter({"app:setUserImageUrl"})
    public static void setUserImageUrl(ImageView iv, String imageUrl) {
        if (imageUrl != null)
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + imageUrl)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_placeholder)
                            .error(R.drawable.user_placeholder))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.user_placeholder);
    }
}