
package com.danielandshayegan.discovrus.utils;

import android.content.Context;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.models.ChartDataList;
import com.danielandshayegan.discovrus.ui.activity.ProfileDetailActivity;
import com.danielandshayegan.discovrus.ui.fragment.BusinessProfileFragment;
import com.danielandshayegan.discovrus.ui.fragment.IndividualProfileFragment;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

/**
 * Custom implementation of the MarkerView.
 *
 * @author Philipp Jahoda
 */
public class MyMarkerView extends MarkerView {

    private TextView txtDate, txtView, txtLike, txtComment;

    public MyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        txtDate = (TextView) findViewById(R.id.txt_date);
        txtView = (TextView) findViewById(R.id.txt_view);
        txtLike = (TextView) findViewById(R.id.txt_like);
        txtComment = (TextView) findViewById(R.id.txt_comment);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        if (e instanceof CandleEntry) {

            CandleEntry ce = (CandleEntry) e;

            txtView.setText("Post Views: " + Utils.formatNumber(ce.getHigh(), 0, true));
        } else {
            ChartDataList.DataBean dataBean = null;
            if (ProfileDetailActivity.profileDetailActivity != null) {
                if (ProfileDetailActivity.profileDetailActivity.getFrom().equalsIgnoreCase("Individual"))
                    dataBean = IndividualProfileFragment.individualProfileFragment.chartDataList.get((int) highlight.getX());
                else if (ProfileDetailActivity.profileDetailActivity.getFrom().equalsIgnoreCase("Business"))
                    dataBean = BusinessProfileFragment.businessProfileFragment.chartDataList.get((int) highlight.getX());
            }
//            txtView.setText("" + Utils.formatNumber(e.getY(), 0, true));
            if (dataBean != null) {
                txtView.setText("Post Views: " + dataBean.getViewCount());
                txtLike.setText("Likes: " + dataBean.getLikeCount());
                txtComment.setText("Comments: " + dataBean.getCommentCount());
                txtDate.setText(dataBean.getDate());
            }
        }

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
