package com.danielandshayegan.discovrus.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.TrendingNowAdapter;
import com.danielandshayegan.discovrus.databinding.DialogTrendingNowBinding;
import com.danielandshayegan.discovrus.models.TrendingNowModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class TrendingNowDialog extends DialogFragment implements TrendingNowAdapter.TrensingDataClicListner,
        GestureDetector.OnGestureListener {

    //region Member Variables
    DialogTrendingNowBinding mBinding;
    private LinearLayoutManager layoutManager;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<TrendingNowModel.DataBean> trendingDataList = new ArrayList<>();
    TrendingNowAdapter trendingNowAdapter;
    MyGestureDetector gestDetec;
    GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;
    private GestureDetectorCompat mDetector;

    // endregion

    public TrendingNowDialog() {
    }

    public static TrendingNowDialog newInstance() {
        TrendingNowDialog frag = new TrendingNowDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_trending_now, container, false);
        View rootView = mBinding.getRoot();

        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        mDetector = new GestureDetectorCompat(getActivity(), this);
        gestDetec = new MyGestureDetector();    //inital setup
        gestureDetector = new GestureDetector(gestDetec);
        gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        };


        if (getArguments() != null) {
           /* VehicleReservationId = getArguments().getInt("VehicleReservationId");
            vehicleId = getArguments().getInt("vehicleId");
            mileage = getArguments().getInt("Mileage");
            date = getArguments().getString("date");
            time = getArguments().getString("time");*/
        }
        return rootView;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.recyclerTrendingNow.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        mBinding.recyclerTrendingNow.setOnFlingListener(null);
        mBinding.recyclerTrendingNow.clearOnScrollListeners();
        snapHelper.attachToRecyclerView(mBinding.recyclerTrendingNow);

     //   trendingNowAdapter = new TrendingNowAdapter(trendingDataList, getActivity(), this);

        mBinding.recyclerTrendingNow.setItemAnimator(new SlideInUpAnimator());
        mBinding.recyclerTrendingNow.setAdapter(trendingNowAdapter);
        mBinding.btnTrendingClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
      /*  mBinding.trendingBlurLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int action = MotionEventCompat.getActionMasked(event);
                switch (action) {
                    case (MotionEvent.ACTION_DOWN):
                        Log.i("touchEvent", "down");
                        return true;
                    // getDialog().dismiss();
                    case (MotionEvent.ACTION_MOVE):
                        getDialog().dismiss();
                        Log.i("touchEvent", "move");
                        return true;

                    case (MotionEvent.ACTION_UP):
                        Log.i("touchEvent", "up");
                        break;

                    case (MotionEvent.ACTION_CANCEL):
                        Log.i("touchEvent", "cancel");
                        break;
                    case (MotionEvent.ACTION_OUTSIDE):
                        Log.i("touchEvent", "outside");
                        break;
                    default:
                }
                return false;
            }
        });*/
//        callAPI();
    }





    public void callAPI() {
        try {
//            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getTrendingList().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<TrendingNowModel>() {
                    @Override
                    public void onSuccess(TrendingNowModel value) {

                        if (value.isSuccess() && value.getData().size() > 0) {
                            trendingDataList.clear();
                            trendingDataList.addAll(value.getData());
                            //  trendingNowAdapter = new TrendingNowAdapter(trendingDataList, getActivity(), getDI);
                            trendingNowAdapter.notifyDataSetChanged();
                            try {
//                               mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
//                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
//                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }
                }));
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onItemSelected(TrendingNowModel.DataBean trendingNowModel) {

        /*getDialog().dismiss();
        Intent intent = new Intent(getActivity(), TrendingNowDetailActivity.class);
        startActivity(intent);*/
    }

    @Override
    public boolean onDown(MotionEvent e) {
        Log.i("touchEvent", "downImaplemet");
        getDialog().dismiss();
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        Log.i("touchEvent", "showPress");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        Log.i("touchEvent", "singleTaP");
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Log.i("touchEvent", "upImplement");
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            Log.i("touchEvent", "" + e1 + e2);
            return false;
        }
    }


}
