package com.danielandshayegan.discovrus.dialog;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DialogSelectLocationBinding;
import com.danielandshayegan.discovrus.models.HiddenDataListModel;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostImageFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostTextFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostVideoFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import io.reactivex.disposables.CompositeDisposable;

public class LocationSelectDialog extends DialogFragment {

    DialogSelectLocationBinding mBinding;
    View view;
    SelectLocationListner selectLocationListner;
    private static final int REQUEST_SELECT_PLACE = 1000;

    private CompositeDisposable disposable = new CompositeDisposable();

    public LocationSelectDialog() {
    }

    public void setListener(SelectLocationListner selectLocationListner) {
        this.selectLocationListner = selectLocationListner;
    }

    public static LocationSelectDialog newInstance(SelectLocationListner selectLocationListner) {
        LocationSelectDialog frag = new LocationSelectDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_location, container, false);
        view = mBinding.getRoot();
        setClick();
        return view;
    }

    public void setClick() {
        mBinding.txtCancel.setOnClickListener(view -> {
            getDialog().dismiss();
        });
        mBinding.txtLiveLocation.setOnClickListener(view -> {
            if (selectLocationListner != null) {
                selectLocationListner.onCurrentSelectLocationListner();
                getDialog().dismiss();
            }
        });

        mBinding.txtManualLocation.setOnClickListener(view -> {
            if (selectLocationListner != null) {
                selectLocationListner.onMenualSelectLocationListner();
                dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public interface SelectLocationListner {
        void onMenualSelectLocationListner();

        void onCurrentSelectLocationListner();
    }
}
