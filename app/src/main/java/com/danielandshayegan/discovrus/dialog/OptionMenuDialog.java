package com.danielandshayegan.discovrus.dialog;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DialogOptionMenuBinding;
import com.danielandshayegan.discovrus.models.AddToHighlight;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.activity.EditPostActivity;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostImageFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostTextFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostVideoFragment;

import static android.content.Context.DOWNLOAD_SERVICE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveBlockUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addToFavourite;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addToHighlight;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deletePost;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePost;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

public class OptionMenuDialog extends DialogFragment {

    DialogOptionMenuBinding mBinding;
    View view;
    PostListData.Post postData;
    Context context;
    String from = "";
    boolean isSave = false;

    public OptionMenuDialog() {
    }

    public static OptionMenuDialog newInstance() {
        OptionMenuDialog frag = new OptionMenuDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_option_menu, container, false);
        view = mBinding.getRoot();
        context = inflater.getContext();
        getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        Bundle args = getArguments();
        if (args != null) {
            postData = args.getParcelable("postDetails");
            from = args.getString("from");
            if (postData != null) {
                if (postData.isSave()) {
                    mBinding.txtSavePost.setText(R.string.unsave_post);
                } else {
                    mBinding.txtSavePost.setText(R.string.save_post);
                }
                if (postData.isHighLight())
                    mBinding.txtAddToHighlights.setText(R.string.remove_from_highlight);
                else
                    mBinding.txtAddToHighlights.setText(R.string.add_to_highlights);

                if (postData.isFollowing()) {
                    mBinding.txtFollow.setText("Unfollow");
                    mBinding.txtFollow.setTextColor(getActivity().getResources().getColor(R.color.red));
                } else {
                    mBinding.txtFollow.setText("Follow");
                    mBinding.txtFollow.setTextColor(getActivity().getResources().getColor(R.color.discovr_total_title));
                }

                if (postData.getType().equalsIgnoreCase("Photo") || postData.getType().equalsIgnoreCase("PhotoAndText")) {
                    mBinding.txtSavePicture.setVisibility(View.VISIBLE);
                    mBinding.viewSavePicture.setVisibility(View.VISIBLE);
                } else {
                    mBinding.txtSavePicture.setVisibility(View.GONE);
                    mBinding.viewSavePicture.setVisibility(View.GONE);
                }

            }
        }

        if (postData != null && postData.getUserID() == App_pref.getAuthorizedUser(getActivity()).getData().getUserId()) {
            mBinding.tvEditPost.setVisibility(View.VISIBLE);
            mBinding.vwEditPost.setVisibility(View.VISIBLE);
            mBinding.tvBlock.setVisibility(View.GONE);
            mBinding.view5.setVisibility(View.VISIBLE);
            mBinding.txtDeletePost.setVisibility(View.VISIBLE);
            mBinding.txtAddToHighlights.setVisibility(View.VISIBLE);
            mBinding.viewHighLight.setVisibility(View.VISIBLE);
            mBinding.txtFollow.setVisibility(View.GONE);
            mBinding.viewFollow.setVisibility(View.GONE);
        } else {
            mBinding.tvEditPost.setVisibility(View.GONE);
            mBinding.vwEditPost.setVisibility(View.GONE);
            mBinding.view4.setVisibility(View.VISIBLE);
            mBinding.tvBlock.setVisibility(View.VISIBLE);
            mBinding.view5.setVisibility(View.GONE);
            mBinding.txtDeletePost.setVisibility(View.GONE);
            mBinding.txtAddToHighlights.setVisibility(View.GONE);
            mBinding.viewHighLight.setVisibility(View.GONE);
            mBinding.txtFollow.setVisibility(View.VISIBLE);
            mBinding.viewFollow.setVisibility(View.VISIBLE);
        }

        setClick();

        return view;
    }

    public void download(String url) {
        Log.e("download: ", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

        //Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //Set whether this download may proceed over a roaming connection.
        request.setAllowedOverRoaming(false);
        //Set the title of this download, to be displayed in notifications (if enabled).
        request.setTitle("Downloading");
        //Set a description of this download, to be displayed in notifications (if enabled)
        request.setDescription("Downloading File");
        //Set the local destination for the downloaded file to a path within the application's external files directory
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //Enqueue a new download and same the referenceId
        long downloadReference = downloadManager.enqueue(request);
    }

    private BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getActivity(), "Download complete.", Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(onComplete);
    }

    public void setClick() {
        mBinding.txtCancel.setOnClickListener(view -> {
            getDialog().dismiss();
        });

        mBinding.tvEditPost.setOnClickListener(view -> {
            context.startActivity(new Intent(context, EditPostActivity.class).putExtra("PostData", postData));
            getDialog().dismiss();
        });

        mBinding.txtSavePost.setOnClickListener(view -> {
            call_SavePost();
        });

        mBinding.txtAddToFav.setOnClickListener(view -> {
            call_AddToFavouritesOrHighlights(true);
        });

        mBinding.txtAddToHighlights.setOnClickListener(view -> {
            call_AddToHighlights();
        });

        mBinding.txtDeletePost.setOnClickListener(view -> call_DeletePost());

        mBinding.tvBlock.setOnClickListener(view -> {
            callBlockUserApi();
        });

        mBinding.txtCopyLink.setOnClickListener(view -> {
            if (from.equalsIgnoreCase("ClickPostImageFragment"))
                ClickPostImageFragment.postImageFragment.copyLink();
            else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                ClickPostTextFragment.postTextFragment.copyLink();
            else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                ClickPostVideoFragment.postVideoFragment.copyLink();
            getDialog().dismiss();
        });

        mBinding.txtShareOnLinkedin.setOnClickListener(view -> {
            if (from.equalsIgnoreCase("ClickPostImageFragment"))
                ClickPostImageFragment.postImageFragment.shareToLinkedIn();
            else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                ClickPostTextFragment.postTextFragment.shareToLinkedIn();
            else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                ClickPostVideoFragment.postVideoFragment.shareToLinkedIn();
            getDialog().dismiss();
        });

        mBinding.txtShareWithProfile.setOnClickListener(view -> {
            if (from.equalsIgnoreCase("ClickPostImageFragment"))
                ClickPostImageFragment.postImageFragment.shareDeepLink();
            else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                ClickPostTextFragment.postTextFragment.shareDeepLink();
            else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                ClickPostVideoFragment.postVideoFragment.shareDeepLink();
            getDialog().dismiss();
        });

        mBinding.txtSavePicture.setOnClickListener(view -> {
            if (postData.getPostPath() != null)
                download(ApiClient.WebService.imageUrl + postData.getPostPath());
            getDialog().dismiss();
        });

        mBinding.tvReport.setOnClickListener(view -> {
            ReportOptionsDialog optionDialog = new ReportOptionsDialog();
            Bundle bundle = new Bundle();
            bundle.putParcelable("postDetails", postData);
            bundle.putString("from", from);
            optionDialog.setArguments(bundle);
            optionDialog.show(getFragmentManager(), "option menu");
            getDialog().dismiss();
        });

    }

    private void call_SavePost() {
        boolean isSave = !postData.isSave();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                savePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId(), isSave)
                        , getCompositeDisposable(), savePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                if (from.equalsIgnoreCase("ClickPostImageFragment"))
                                    ClickPostImageFragment.postImageFragment.dataBean.setSave(isSave);
                                else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                                    ClickPostTextFragment.postTextFragment.dataBean.setSave(isSave);
                                else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                                    ClickPostVideoFragment.postVideoFragment.dataBean.setSave(isSave);
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");

                            }
                        });
    }

    private void call_DeletePost() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deletePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId())
                        , getCompositeDisposable(), deletePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                if (from.equalsIgnoreCase("ClickPostImageFragment"))
                                    ClickPostImageFragment.postImageFragment.simpleMethod();
                                else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                                    ClickPostTextFragment.postTextFragment.simpleMethod();
                                else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                                    ClickPostVideoFragment.postVideoFragment.simpleMethod();
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("onFailure: ", "Failed");

                            }
                        });
    }

    private void call_AddToFavouritesOrHighlights(Boolean isFav) {

        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                addToFavourite(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId(), !isFav, isFav)
                        , getCompositeDisposable(), addToFavourite, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                if (isFav)
                                    Toast.makeText(context, "Added to Favourites", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(context, "Added to Highlights", Toast.LENGTH_SHORT).show();
                                Log.e("onSingleSuccess: ", "Success");
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }

    private void call_AddToHighlights() {

        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                addToHighlight(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId())
                        , getCompositeDisposable(), addToHighlight, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                AddToHighlight addToHighlight = (AddToHighlight) o;
                                if (addToHighlight != null && addToHighlight.getMessage() != null) {
                                    ClickPostImageFragment.postImageFragment.dataBean.setHighLight(!postData.isHighLight());
                                    Toast.makeText(context, addToHighlight.getMessage().get(0), Toast.LENGTH_LONG).show();
                                }
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void callBlockUserApi() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                SaveBlockUserDetails(postData.getUserID(), App_pref.getAuthorizedUser(getActivity()).getData().getUserId())
                        , getCompositeDisposable(), SaveBlockUserDetails, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                builder.setMessage(R.string.user_blocked_successfully);
                                builder.setPositiveButton(getString(R.string.Ok), (dialog, which) -> {
                                    dialog.dismiss();
                                    getActivity().finish();
                                    getDialog().dismiss();
                                });
                                builder.show();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                            }
                        });
    }
}
