package com.danielandshayegan.discovrus.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DialogLogoutBinding;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.services.NotificationToken;
import com.danielandshayegan.discovrus.ui.activity.SplashActivity;
import com.facebook.login.LoginManager;

import static android.support.constraint.Constraints.TAG;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveBlockUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.logOut;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

public class LogoutDialog extends DialogFragment {

    View view;
    DialogLogoutBinding mBinding;
    int userId;

    public LogoutDialog() {
    }

    public static LogoutDialog newInstance() {
        LogoutDialog frag = new LogoutDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_logout, null, false);
            view = mBinding.getRoot();
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();

        mBinding.btnSignOut.setOnClickListener(view -> {
//            callLogoutApi();
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            if (App_pref.getAuthorizedUser(getActivity()) != null) {
                ApplicationClass.OnlineUserListAPI(getActivity(), false);
            }

            App_pref.signOut(getActivity());

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.apply();

            LoginManager.getInstance().logOut();
            Intent intentLogout = new Intent(getActivity(), SplashActivity.class);
            startActivity(intentLogout);
            getActivity().finish();
        });
        mBinding.btnSignOutCancel.setOnClickListener(view -> {
            LogoutDialog.this.getDialog().dismiss();
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        });
        return view;
    }

    private void callLogoutApi() {
        NotificationToken notificationToken = App_pref.getNotificationToken(getActivity());
        String AndroidDeviceID = notificationToken.getAccessToken();
        Log.e(TAG, "storedRegId: " + AndroidDeviceID);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                logOut(userId, AndroidDeviceID, "")
                        , getCompositeDisposable(), logOut, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                if (App_pref.getAuthorizedUser(getActivity()) != null) {
                                    ApplicationClass.OnlineUserListAPI(getActivity(), false);
                                }

                                App_pref.signOut(getActivity());

                                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                SharedPreferences.Editor editor = sharedPrefs.edit();
                                editor.clear();
                                editor.apply();

                                LoginManager.getInstance().logOut();
                                Intent intentLogout = new Intent(getActivity(), SplashActivity.class);
                                startActivity(intentLogout);
                                getActivity().finish();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                            }
                        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setView(view);

        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());

        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        return dialog;
    }
}
