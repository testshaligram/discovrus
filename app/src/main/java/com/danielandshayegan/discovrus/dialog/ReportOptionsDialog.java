package com.danielandshayegan.discovrus.dialog;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BaseAdapter;
import com.danielandshayegan.discovrus.adapter.CardListAdapter;
import com.danielandshayegan.discovrus.adapter.ReportReasonListAdapter;
import com.danielandshayegan.discovrus.databinding.DialogOptionMenuBinding;
import com.danielandshayegan.discovrus.databinding.DialogReportOptionsBinding;
import com.danielandshayegan.discovrus.models.AddToHighlight;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.ReasonData;
import com.danielandshayegan.discovrus.models.ReportProblemData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostImageFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostTextFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostVideoFragment;
import com.stripe.android.Stripe;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.DOWNLOAD_SERVICE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveBlockUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addToFavourite;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addToHighlight;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deletePost;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getReason;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.reportProblem;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePost;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

public class ReportOptionsDialog extends DialogFragment {

    DialogReportOptionsBinding mBinding;
    View view;
    PostListData.Post postData;
    Context context;
    String from = "";
    List<ReasonData.DataBean> reasonList = new ArrayList<>();
    ReportReasonListAdapter reasonListAdapter;
    int userId;
    String email;

    public ReportOptionsDialog() {
    }

    public static ReportOptionsDialog newInstance() {
        ReportOptionsDialog frag = new ReportOptionsDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_report_options, container, false);
        view = mBinding.getRoot();
        context = inflater.getContext();
        Bundle args = getArguments();
        if (args != null) {
            postData = args.getParcelable("postDetails");
            from = args.getString("from");
        }
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        email = App_pref.getAuthorizedUser(getActivity()).getData().getEmail();

        setClick();
        callGetReason();

        return view;
    }

    private BaseAdapter.OnItemClickListener listener = (position, view) -> {
        String msg = "", reason = "";
        if (reasonList.get(position).getReportReason().contains("I think it's "))
            reason = reasonList.get(position).getReportReason().replaceAll("I think it's ", "");
        else
            reason = reasonList.get(position).getReportReason();

        msg = "You're reporting that this is " + reason;
        new AlertDialog.Builder(getActivity())
                .setTitle("Confirm your report")
                .setMessage(msg)
                .setPositiveButton(R.string.Ok, (dialog, whichButton) -> {
                    callReportProblem(reasonList.get(position).getReasonID());
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
        getDialog().dismiss();
    };

    public void setClick() {
        mBinding.imgClose.setOnClickListener(view -> {
            getDialog().dismiss();
        });

    }

    private void callGetReason() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getReason()
                        , getCompositeDisposable(), getReason, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                ReasonData reasonData = (ReasonData) o;
                                if (reasonData != null && reasonData.isSuccess()) {
                                    reasonList = reasonData.getData();
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    mBinding.recyclerReporting.setLayoutManager(linearLayoutManager);
                                    reasonListAdapter = new ReportReasonListAdapter(getActivity(), reasonList, listener, "reportDialog");
                                    mBinding.recyclerReporting.setAdapter(reasonListAdapter);
                                    reasonListAdapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }

    private void callReportProblem(int reasonId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                reportProblem(String.valueOf(postData.getPostId()), userId, reasonId, email, String.valueOf(postData.getUserID()), "")
                        , getCompositeDisposable(), reportProblem, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                ReportProblemData reportProblemData= (ReportProblemData) o;
                                if (reportProblemData != null) {
//                                    Toast.makeText(getActivity(), reportProblemData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
