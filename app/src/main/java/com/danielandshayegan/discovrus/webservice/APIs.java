package com.danielandshayegan.discovrus.webservice;

import com.danielandshayegan.discovrus.network.ApiClient;

public class APIs {

    static final String BASE_URL = ApiClient.WebService.baseUrl;
    public static final String BASE_IMAGE_PATH = ApiClient.WebService.imageUrl;

    public static final String GET_GENERAL_SETTINGS = BASE_URL + "GeneralSettingsAPI/GetGeneralSettings";
    public static final String SET_GENERAL_SETTINGS = BASE_URL + "GeneralSettingsAPI/SaveGeneralSettings";
    public static final String EDIT_POST = BASE_URL + "PostAPI/EditPost";
    public static final String GET_PAYMENTS = BASE_URL + "PaymentAPI/GetPaymentPagging";
}