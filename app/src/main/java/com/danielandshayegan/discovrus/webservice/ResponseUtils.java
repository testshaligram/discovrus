package com.danielandshayegan.discovrus.webservice;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.danielandshayegan.discovrus.R;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Response;

public class ResponseUtils {

    private static void printLogs(String webUrl, String reqParam, String method) {
        char[] chars = reqParam.toCharArray();
        String reqParameter = "";
        for (char aChar : chars) {
            if ('&' == aChar) {
                reqParameter += "\n";
            } else {
                reqParameter += aChar;
            }
        }
        Log.e("Web URl ==>", String.valueOf(webUrl));
        Log.e("Method ==>", String.valueOf(method));
        Log.e("Request Parameters ==>>", String.valueOf(reqParameter));
    }

    private static void printResponse(String response) {
        Log.e("Response ==>", String.valueOf(response));
    }


    public static String getResponseData(Response<JsonObject> response) {
        try {
            if (response != null)
                return response.body().get("data").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isSuccess(JsonObject jsonObject) {
        try {
            if (jsonObject != null && jsonObject.get("api_status").getAsString().equals("200"))
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getSuccessMessage(Context mContext, String response) {
        try {
            JSONObject jObj = new JSONObject(response);
            return jObj.getString("Message");
        } catch (Exception e) {
            e.printStackTrace();
            return "Success";
        }
    }

    public static String getFailMessage(Context mContext, String failMessage) {
        String msg = "";
        try {
            JSONObject jObj = new JSONObject(failMessage);
            return jObj.getString("Message");
        } catch (Exception e) {
            e.printStackTrace();
            msg = mContext.getResources().getString(R.string.something_went_wrong);
        }
        return msg;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    Log.d("Network", "NETWORK NAME: " + networkInfo.getTypeName());
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Log.d("Network", "NETWORK NAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
