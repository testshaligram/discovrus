package com.danielandshayegan.discovrus.eventbus;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sit107 on 05-01-2018.
 */

public class GlobalBus {

    private static EventBus sBus;
    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}
