package com.danielandshayegan.discovrus.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.activity.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationMessagingService extends FirebaseMessagingService {

    private static final String TAG = NotificationMessagingService.class.getSimpleName();

    private NotificationUtility notificationUtils;
    private String title, message, timestamp, image;
    Intent pushNotification;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        storeRegIdInPref(s);
    }

    private void storeRegIdInPref(String token) {
        NotificationToken notificationToken = new NotificationToken(token);
        App_pref.saveNotificationToken(getApplicationContext(), notificationToken);
        Log.e(TAG, "storeRegIdInPref: " + notificationToken.getAccessToken());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        sendNotification(remoteMessage.getNotification().getBody());
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("FCM Message")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private void handleDataMessage(JSONObject json) {
        try {
            JSONObject data = json.getJSONObject("data");
            title = data.getString("title");
            message = data.getString("message");
            timestamp = data.getString("timestamp");
            image = data.getString("image");

//            image = VMService.BASE_NOTIFICATION_IMAGE_URL + image;

            getNotification(title, message, timestamp, image);

        } catch (JSONException e) {
        } catch (Exception e) {
        }
    }

    private void getNotification(String title, String message, String timestamp, String image) {

        pushNotification = new Intent(getApplicationContext(), SplashActivity.class);
        pushNotification.putExtra("title", title);
        pushNotification.putExtra("message", message);
        pushNotification.putExtra("timestamp", timestamp);

//        showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);

        if (TextUtils.isEmpty(image) && image == null) {
            showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);
        } else {
            // image is present, show notification with image
            // image = VMService.BASE_NOTIFICATION_IMAGE_URL + image;
            showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, pushNotification, image);
        }
    }

    /**
     * re
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtility(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtility(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

}
