package com.danielandshayegan.discovrus.services;

public class NotificationToken {

    // region Fields
    private String accessToken;
    // endregion

    // region Constructors
    public NotificationToken(String accessToken) {
        this.accessToken = accessToken;
    }
    // endregion

    // region Getters
    public String getAccessToken() {
        return accessToken;
    }
    // endregion
}



