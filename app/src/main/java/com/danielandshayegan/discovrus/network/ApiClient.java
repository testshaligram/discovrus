package com.danielandshayegan.discovrus.network;

import android.content.Context;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    private static final int OKHTTP_TIMEOUT = 60; // seconds
    private static Retrofit retrofit = null;
    private static OkHttpClient okHttpClient;
    private static Context mcontext;

    /**
     * You can create multiple methods for different BaseURL
     *
     * @return {@link Retrofit} object
     */
    public static Retrofit getClient(Context context) {
        if (retrofit == null) {
            mcontext = context;
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(WebService.baseUrl)
                    .client(getOKHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }


    /**
     * settings like caching, Request Timeout, Logging can be configured here.
     *
     * @return {@link OkHttpClient}
     */
    private static OkHttpClient getOKHttpClient() {
        if (okHttpClient == null) {
            Cache cache = new Cache(new File(ApplicationClass.getInstance().getCacheDir(), "http")
                    , DISK_CACHE_SIZE);
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)
                    .connectTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                    .cache(cache);


            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(loggingInterceptor);
            }

//            setupBasicAuth(builder);
            okHttpClient = builder.build();
        }
        return okHttpClient;
    }

    /*private static void setupBasicAuth(OkHttpClient.Builder builder) {
     *//*if (!TextUtils.isEmpty(WebService.API_USERNAME) && !TextUtils.isEmpty(WebService.API_PASSWORD))*//*
        {
            String credentials = WebService.API_USERNAME + ":" + WebService.API_PASSWORD;
            String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            if (App_pref.getAuthorizedUser(mcontext) != null) {

                basic = App_pref.getAuthorizedUser(mcontext).getData().get(0).getToken();
            }

            String finalBasic = "amx " + basic;
            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", finalBasic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }
    }*/

    public interface WebService {
//        String baseUrl = "http://27.109.4.86:2012/API/";
//        public String imageUrl = "http://27.109.4.86:2012";
        String baseUrl = "http://5.9.90.104:2018/API/";
        public String imageUrl = "http://5.9.90.104:2018";
        String Version = "1.0";
        String API_USERNAME = null;
        String API_PASSWORD = null;
    }
}