package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowMapCategoryBinding;
import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;

import java.util.List;

public class MapCategoryAdapter  extends ArrayAdapter<CategoriesTableModel> {

    private List<CategoriesTableModel> dataSet;
    Context mContext;

    public MapCategoryAdapter(List<CategoriesTableModel> data, Context context) {
        super(context, R.layout.row_map_category, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.row_map_category, null);

        TextView txtCategoryName = v.findViewById(R.id.txt_category_name);
        ImageView imgSelectedCategory = v.findViewById(R.id.img_selected_category);

        txtCategoryName.setText(dataSet.get(position).getCategoryName());
        if (dataSet.get(position).getIsSelected().equalsIgnoreCase("1"))
            imgSelectedCategory.setBackgroundResource(R.drawable.ic_category_select);
        else
            imgSelectedCategory.setBackgroundResource(R.drawable.ic_category_unselect);

        imgSelectedCategory.setOnClickListener(view -> {
            if (dataSet.get(position).getIsSelected().equalsIgnoreCase("1")) {
                imgSelectedCategory.setBackgroundResource(R.drawable.ic_category_unselect);
                dataSet.get(position).setIsSelected("0");
            } else {
                imgSelectedCategory.setBackgroundResource(R.drawable.ic_category_select);
                dataSet.get(position).setIsSelected("1");
            }
        });
        // Return the completed view to render on screen
        return v;
    }
}
