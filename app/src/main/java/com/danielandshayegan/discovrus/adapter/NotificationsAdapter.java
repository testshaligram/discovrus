package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.LoadingBarBinding;
import com.danielandshayegan.discovrus.databinding.RowCardBinding;
import com.danielandshayegan.discovrus.databinding.RowNotificationsBinding;
import com.danielandshayegan.discovrus.models.Book;

import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter {
    private List<Book> dataSet;
    private Context context;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public NotificationsAdapter(List<Book> data, Context context, RecyclerView recyclerView) {
        this.dataSet = data;
        this.context = context;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    loading = true;
                    // End has been reached
                    // Do something
                    Log.i("AdapterScrolled", "onScrolled: End reached");
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }

                }
            }
        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RowNotificationsBinding mBinding;

        public MyViewHolder(RowNotificationsBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinding = mBinder;
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return dataSet.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            RowNotificationsBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                    .from(parent.getContext()), R.layout.row_notifications, parent, false);

            vh = new MyViewHolder(mBinder);
        } else {
            LoadingBarBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                    .from(parent.getContext()), R.layout.loading_bar, parent, false);

            vh = new ProgressViewHolder(mBinder);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
//                Binding Views...
            Book book = dataSet.get(position);
           /* ((MyViewHolder) holder).profilePic.setImageDrawable(ContextCompat.getDrawable(context, book.imageId));
            ((MyViewHolder) holder).title.setText(book.title);
            ((MyViewHolder) holder).author.setText(book.author);
            ((MyViewHolder) holder).description.setText(book.description);*/
        } else {
            ((ProgressViewHolder) holder).mBinding.progressBar.setVisibility(View.VISIBLE);
            ((ProgressViewHolder) holder).mBinding.progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
       LoadingBarBinding mBinding;

        public ProgressViewHolder(LoadingBarBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinding = mBinder;
        }
    }
}
