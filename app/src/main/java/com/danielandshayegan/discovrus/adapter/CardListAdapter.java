package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowCardBinding;
import com.danielandshayegan.discovrus.models.CardListEnvelope;

import java.util.List;

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.CardViewHolder> {

    private Context context;
    private List<CardListEnvelope.DataBean> cardData;
    private int position = 0;
    private BaseAdapter.OnItemClickListener listener;

    public CardListAdapter(Context context, List<CardListEnvelope.DataBean> cardData, int position, BaseAdapter.OnItemClickListener listener) {
        this.context = context;
        this.cardData = cardData;
        this.position = position;
        this.listener = listener;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowCardBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_card, parent, false);

        CardViewHolder holder = new CardViewHolder(mBinder);
        return holder;
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, final int position) {
        setTitle(holder.mBinding.cardNumberTv, cardData.get(position).getLast4());
        if (position == this.position)
            holder.mBinding.imgSelectedCard.setBackgroundResource(R.drawable.ic_radio_button_on);
        else
            holder.mBinding.imgSelectedCard.setBackgroundResource(R.drawable.ic_radio_off_button);

        setCardBrand(cardData.get(position).getBrand(), holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(holder.getAdapterPosition(), holder.itemView);
            }
        });
        holder.mBinding.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(holder.getAdapterPosition(), holder.mBinding.imgDelete);
            }
        });
    }

    private void setCardBrand(String brand, CardViewHolder holder) {
        Drawable drawable = null;
        switch (brand) {
            case "Visa":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_visa);
                break;
            case "MasterCard":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_mastercard);
                break;
            case "Discover":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_discover);
                break;
            case "Diners Club":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_diners);
                break;
            case "Maestro":
                drawable = ContextCompat.getDrawable(context, R.drawable.maestro);
                break;
            case "JCB":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_jcb);
                break;
            case "American Express":
                drawable = ContextCompat.getDrawable(context, R.drawable.ic_amex);
                break;
            default:
                drawable = null;
                break;
        }
        holder.mBinding.imgCardType.setImageDrawable(drawable);
    }

    private void setTitle(TextView textView, String data) {
        if (data != null) {
            if (!TextUtils.isEmpty(data)) {
                textView.setText("Card ending with " + data);
            }
        }
    }

    @Override
    public int getItemCount() {
        return cardData.size();
    }

    public void setPosition(int selectPosition) {
        position = selectPosition;
        notifyDataSetChanged();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        RowCardBinding mBinding;

        public CardViewHolder(RowCardBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinding = mBinder;
        }

    }
}
