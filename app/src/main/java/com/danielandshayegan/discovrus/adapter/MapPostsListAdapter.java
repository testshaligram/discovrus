package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.databinding.RowCardBinding;
import com.danielandshayegan.discovrus.databinding.RowMapPostsListBinding;
import com.danielandshayegan.discovrus.models.CardListEnvelope;
import com.danielandshayegan.discovrus.models.MapPostDetailByLatLong;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.fragment.FragmentNavigation;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class MapPostsListAdapter extends RecyclerView.Adapter<MapPostsListAdapter.PostViewHolder> {

    private Context mContext;
    private List<MapPostDetailByLatLong.DataBean> postDataList;

    public MapPostsListAdapter(Context context, List<MapPostDetailByLatLong.DataBean> postDataList) {
        this.mContext = context;
        this.postDataList = postDataList;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowMapPostsListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_map_posts_list, parent, false);

        PostViewHolder holder = new PostViewHolder(mBinder);
        return holder;
    }

    @Override
    public void onBindViewHolder(final PostViewHolder holder, final int position) {
        MapPostDetailByLatLong.DataBean postData = postDataList.get(position);

        if (postData.getType().equalsIgnoreCase("Text")) {
            holder.mBinding.imgPost.setVisibility(View.GONE);
            holder.mBinding.textConstraint.setVisibility(View.VISIBLE);
            String title = "";
            try {
                title = URLDecoder.decode(postData.getTitle(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String desc = "";
            try {
                desc = URLDecoder.decode(postData.getDescription(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            holder.mBinding.titleTv.setText(title);
            holder.mBinding.descTv.setText(desc);
        } else {
            holder.mBinding.imgPost.setVisibility(View.VISIBLE);
            holder.mBinding.textConstraint.setVisibility(View.GONE);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .error(R.drawable.placeholder_image);

            Glide.with(mContext)
                    .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                    .apply(options)
                    .into(holder.mBinding.imgPost);
        }

        holder.mBinding.mainCard.setOnClickListener(view -> {
//            FragmentNavigation.fragmentNavigation.setDetails(postDataList.get(position));
        });

    }

    @Override
    public int getItemCount() {
        return postDataList.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        RowMapPostsListBinding mBinding;

        public PostViewHolder(RowMapPostsListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinding = mBinder;
        }

    }
}
