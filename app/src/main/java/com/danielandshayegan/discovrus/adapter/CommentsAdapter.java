package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowCommentsBinding;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.activity.PostCommentReplyActivity;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {


    List<ClickPostCommentData.DataBean> dataBeanList;
    Context context;
    int postId;
    public static String IMAGE_PATH = ApiClient.WebService.imageUrl;
    private DeletePosCommentListner clickPosCommentListner;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    boolean isDeleted = false;


    public CommentsAdapter(int postId, List<ClickPostCommentData.DataBean> dataBeanList, Context context, DeletePosCommentListner clickPosCommentListner) {
        this.postId = postId;
        this.dataBeanList = dataBeanList;
        this.context = context;
        this.clickPosCommentListner = clickPosCommentListner;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowCommentsBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_comments, parent, false);
        return new MyViewHolder(mBinder);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (dataBeanList.get(position).getUserImagePath() != null) {
            Picasso.with(context).
                    load(IMAGE_PATH + dataBeanList.get(position).getUserImagePath()).
                    into(holder.mBinder.imgCommentUserProile);
        }
        String comment = "";
        try {
            comment = URLDecoder.decode(dataBeanList.get(position).getComment(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        holder.mBinder.txtCommentTitle.setText(comment);
        String dateStr = dataBeanList.get(position).getCommentDate();
        if (dateStr.length() <= 10) {
            holder.mBinder.txtCommentDuration.setText(dateStr + "  - " + dataBeanList.get(position).getUserName());
        } else {
            if (dateStr != null) {
                /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");*/
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date value = null;
                try {
                    value = formatter.parse(dateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm aa"); //this format changeable
                dateFormatter.setTimeZone(TimeZone.getDefault());
                dateStr = dateFormatter.format(value);
                if (!TextUtils.isEmpty(dateStr)) {
                    holder.mBinder.txtCommentDuration.setText(dateStr + " - " + dataBeanList.get(position).getUserName());
                }
            }
        }
        if (dataBeanList.get(position).getCommentReplyCount() != 0)
        holder.mBinder.txtSeeReply.setText("See replies [" + dataBeanList.get(position).getCommentReplyCount() + "]");

        holder.mBinder.txtSeeReply.setOnClickListener(view ->
                context.startActivity(new Intent(context, PostCommentReplyActivity.class)
                        .putExtra("commentId", dataBeanList.get(position).getCommentId()).putExtra("postId", postId)));

        binderHelper.bind(holder.mBinder.swipeLayout, String.valueOf(dataBeanList.get(position).getCommentId()));
        holder.bind();

        if (dataBeanList.get(position).isCanDelete())
            holder.mBinder.commentDelete.setVisibility(View.VISIBLE);
        else
            holder.mBinder.commentDelete.setVisibility(View.GONE);

    }


    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowCommentsBinding mBinder;

        public MyViewHolder(RowCommentsBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;


        }

        public void bind() {
            mBinder.commentDelete.setOnClickListener(v -> new AlertDialog.Builder(context)
                    .setTitle("Delete Alert")
                    .setMessage("Are you sure you want to delete this comment?")
                    .setPositiveButton(R.string.Ok, (dialog, whichButton) -> {
                        dialog.dismiss();
                        clickPosCommentListner.onCommentDeleted(dataBeanList.get(getAdapterPosition()));
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dataBeanList.remove(getAdapterPosition());
                                notifyItemRemoved(getAdapterPosition());
                            }
                        }, 500);
                    })
                    .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    })
                    .show());

            mBinder.commentAns.setOnClickListener(view -> clickPosCommentListner.onCommentReplyClick(dataBeanList.get(getAdapterPosition())));

        }
    }

    public interface DeletePosCommentListner {

        void onCommentDeleted(ClickPostCommentData.DataBean clickpostData);

        void onCommentReplyClick(ClickPostCommentData.DataBean clickpostData);
    }

}
