package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemPostInnerListBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.fragment.PostManagementFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class PostInnerListAdapter extends BaseAdapter<PostListData.Post> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    static PostManagementFragment mContext;
    private int selectedPosition = 0;
    /*static PlaybackLocation location;
    static CastSession castSession;
    static PlaybackState playbackState;
    private static long currentPosition = 0;*/
    // endregion

    // region Constructors
    public PostInnerListAdapter() {
        super();
    }

    public PostInnerListAdapter(PostManagementFragment context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemPostInnerListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_post_inner_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext.getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 2;

        //if you need 4-5-6 anything fix imageview in height
//            int deviceheight = displaymetrics.heightPixels / 4;

//        holder.mBinder.mainCard.getLayoutParams().width = devicewidth - dpToPx(20);

        //if you need same height as width you can set devicewidth in holder.image_view.getLayoutParams().height
//        card.getLayoutParams().height = devicewidth - 50;

        final PostListData.Post post = getItem(position);
        if (post != null) {
            holder.bind(post, (int) (devicewidth - mContext.getResources().getDimension(R.dimen._10sdp)), (int) (displaymetrics.widthPixels - mContext.getResources().getDimension(R.dimen._10sdp)));
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new PostListData.Post());
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    // region Inner Classes
    public static class PostInnerViewHolder extends RecyclerView.ViewHolder {
        ItemPostInnerListBinding mBinder;
        private DefaultTrackSelector trackSelector;

        public PostInnerViewHolder(ItemPostInnerListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(PostListData.Post postData, int deviceHalfWidth, int deviceFullWidth) {

            String like = Utils.convertLikes(postData.getLikeCount());
            String commentCount = Utils.convertLikes(postData.getCommentCount());
            String viewCount = Utils.convertLikes(postData.getViewCount());

            mBinder.commentsCountTv.setText(commentCount);
            mBinder.favoritesCountTv.setText(like);
            mBinder.viewsCountTv.setText(viewCount);
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(R.drawable.placeholder_image)
                    .error(R.drawable.placeholder_image);

            switch (postData.getType()) {
                case "Photo":
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams.width = deviceHalfWidth;
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.onlyImageIv);

                    break;
                case "Video":
                    ConstraintLayout.LayoutParams layoutParams1 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams1.width = deviceFullWidth;
                    layoutParams1.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams1);

                    mBinder.videoConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    RequestOptions options1 = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_video)
                            .error(R.drawable.placeholder_video);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getThumbFileName())
                            .apply(options1)
                            .into(mBinder.videoThumbIv);

                    /*ImageView thumbnailImageView = mBinder.getRoot().findViewById(R.id.exo_thumbnail);
                    TextView castInfoTextView = mBinder.getRoot().findViewById(R.id.cast_info_tv);
                    View shutterView = mBinder.getRoot().findViewById(R.id.exo_shutter);
                    SeekBar exoSeekBar = mBinder.getRoot().findViewById(R.id.exo_progress);
                    ImageButton exoPlayButton = mBinder.getRoot().findViewById(R.id.exo_play);
                    ImageButton exoPauseButton = mBinder.getRoot().findViewById(R.id.exo_pause);
                    ImageButton replayImageButton = mBinder.getRoot().findViewById(R.id.exo_replay);
                    FrameLayout exoButtonsFrameLayout = mBinder.getRoot().findViewById(R.id.exo_btns_fl);
                    LinearLayout controlViewLinearLayout = mBinder.getRoot().findViewById(R.id.control_view_ll);
                    MediaRouteButton mediaRouteButton = mBinder.getRoot().findViewById(R.id.mrb);
                    AspectRatioFrameLayout aspectRatioFrameLayout = mBinder.getRoot().findViewById(R.id.exo_content_frame);

                    playbackState = PlaybackState.PLAYING;
                    exoSeekBar.setThumbTintList(ColorStateList.valueOf(mContext.getResources().getColor(R.color.colorAccent)));
                    exoSeekBar.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY));

                    // Create a default TrackSelector
                    TrackSelector trackSelector = createTrackSelector();

                    // Create a default LoadControl
                    LoadControl loadControl = new DefaultLoadControl();

                    // Create the player
                    SimpleExoPlayer simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector, loadControl);
                    simpleExoPlayer.setPlayWhenReady(true);

                    simpleExoPlayer.addListener(new Player.EventListener() {
                        @Override
                        public void onTimelineChanged(Timeline timeline, Object manifest) {
                        }

                        @Override
                        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                        }

                        @Override
                        public void onLoadingChanged(boolean isLoading) {
                        }

                        @Override
                        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                            switch (playbackState) {
                                case ExoPlayer.STATE_BUFFERING:
//                                    loadingImageView.setVisibility(View.VISIBLE);
                                    break;

                                case ExoPlayer.STATE_READY:
                                    replayImageButton.setVisibility(View.GONE);
                                    exoButtonsFrameLayout.setVisibility(View.VISIBLE);
//                                    loadingImageView.setVisibility(View.GONE);
                                    break;
                                case ExoPlayer.STATE_ENDED:
                                    exoButtonsFrameLayout.setVisibility(View.GONE);
                                    replayImageButton.setVisibility(View.VISIBLE);
//                                    loadingImageView.setVisibility(View.GONE);
                                    break;
                                default:
                                    break;
                            }
                        }

                        @Override
                        public void onRepeatModeChanged(int repeatMode) {

                        }

                        @Override
                        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                        }

                        @Override
                        public void onPlayerError(ExoPlaybackException error) {

                        }

                        @Override
                        public void onPositionDiscontinuity(int reason) {

                        }

                        @Override
                        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                        }

                        @Override
                        public void onSeekProcessed() {

                        }

                    });

                    mBinder.exoplayer.setPlayer(simpleExoPlayer);
                    *//*mBinder.exoplayer.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
                        @Override
                        public void onVisibilityChange(int visibility) {
                            if (visibility == View.GONE) {
                                hidePortraitSystemUI();
                            } else {
                                showPortraitSystemUI();
                            }
                        }
                    });*//*
                    exoSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (fromUser) {
                                currentPosition = (long) (simpleExoPlayer.getDuration() * (progress / 1000.0D));
                                simpleExoPlayer.seekTo(currentPosition);
                            } else {
                                playbackState = PlaybackState.PLAYING;
                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });

                    *//*VideoSavedState videoSavedState = getVideoSavedState();
                    if (videoSavedState != null && !TextUtils.isEmpty(videoSavedState.getVideoUrl())) {
//            loadingImageView.setVisibility(View.GONE);
                        long currentPosition = videoSavedState.getCurrentPosition();
                        videoUrl = videoSavedState.getVideoUrl();
                        playbackState = videoSavedState.getPlaybackState();
//            castInfoTextView.setText(videoSavedState.getCastInfo());
                        castInfoTextView.setVisibility(View.VISIBLE);

                        boolean autoPlay = false;
                        switch (playbackState) {
                            case PLAYING:
                                autoPlay = true;
                                break;
                            case PAUSED:
                                autoPlay = false;
                                break;
                            default:
                                break;
                        }

                        playLocalVideo(currentPosition, autoPlay);
                        updateLocalVideoVolume(videoSavedState.getCurrentVolume());
                    }*//*

                    if (location == PlaybackLocation.LOCAL) {
                        thumbnailImageView.animate().alpha(0.0f).setDuration(1000);
                        castInfoTextView.setVisibility(View.GONE);
                    }
                    simpleExoPlayer.seekTo(0);
                    // Prepare the player with the source.
                    // Produces DataSource instances through which media data is loaded.
                    DataSource.Factory dataSourceFactory =
                            new DefaultDataSourceFactory(mContext, Util.getUserAgent(mContext, "Discovrus"));

                    // Produces Extractor instances for parsing the media data.
                    ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                    // This is the MediaSource representing the media to be played.
                    Uri videoUri = Uri.parse(ApiClient.WebService.imageUrl + postData.getPostPath());
                    MediaSource videoSource = new ExtractorMediaSource(videoUri, dataSourceFactory, extractorsFactory, null, null);

                    simpleExoPlayer.prepare(videoSource);
                    simpleExoPlayer.setPlayWhenReady(true);

                    exoPlayButton.setOnClickListener(view -> {
                        playbackState = PlaybackState.PLAYING;
                        exoPlayButton.setVisibility(View.GONE);
                        simpleExoPlayer.setPlayWhenReady(true);
//                        if (location == PlaybackLocation.REMOTE) {
//                            castInfoTextView.setText(String.format("Casting to %s", castSession.getCastDevice().getFriendlyName()));
//                        }
                    });

                    exoPauseButton.setOnClickListener(view -> {
                        playbackState = PlaybackState.PAUSED;
                        exoPlayButton.setVisibility(View.GONE);
                        simpleExoPlayer.setPlayWhenReady(false);
                    });

                    replayImageButton.setOnClickListener(view -> {
                        replayImageButton.setVisibility(View.GONE);
                        exoButtonsFrameLayout.setVisibility(View.VISIBLE);
                        simpleExoPlayer.seekTo(0);
                    });*/

                    /*
                    // Create a default TrackSelector
                    BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                    TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
                    TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                    //Initialize the player
                    SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector);
                    //Set media controller
                    mBinder.exoplayer.setUseController(true);
                    mBinder.exoplayer.requestFocus();
                    mBinder.exoplayer.setControllerAutoShow(false);
                    player.setRepeatMode(0);
                    //Initialize simpleExoPlayerView
                    mBinder.exoplayer.setPlayer(player);
//                    ImageButton imageButtonReply = mBinder.getRoot().findViewById(R.id.exo_replay);
//                    ImageButton imageButtonPause = mBinder.getRoot().findViewById(R.id.exo_pause);
//                    ImageButton imageButtonPlay = mBinder.getRoot().findViewById(R.id.exo_play);
//                    imageButtonReply.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            player.seekTo(0);
//                            imageButtonReply.setVisibility(View.GONE);
//                            imageButtonPause.setVisibility(View.VISIBLE);
//                        }
//                    });
                    mBinder.exoplayer.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
                        @Override
                        public void onVisibilityChange(int visibility) {
//                            if (imageButtonReply.getVisibility() == View.VISIBLE) {
//                                imageButtonPause.setVisibility(View.GONE);
//                                imageButtonPlay.setVisibility(View.GONE);
//                            }
                        }
                    });
                    player.addListener(new Player.EventListener() {
                        @Override
                        public void onTimelineChanged(Timeline timeline, Object manifest) {

                        }

                        @Override
                        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                        }

                        @Override
                        public void onLoadingChanged(boolean isLoading) {

                        }

                        @Override
                        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                            switch (playbackState) {
                                case SimpleExoPlayer.STATE_ENDED:
//                                    imageButtonReply.setVisibility(View.VISIBLE);
//                                    imageButtonPause.setVisibility(View.GONE);
                                    break;
                            }

                        }

                        @Override
                        public void onRepeatModeChanged(int repeatMode) {

                        }

                        @Override
                        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                        }

                        @Override
                        public void onPlayerError(ExoPlaybackException error) {

                        }

                        @Override
                        public void onPositionDiscontinuity(int reason) {

                        }

                        @Override
                        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                        }

                        @Override
                        public void onSeekProcessed() {
//                            imageButtonReply.setVisibility(View.GONE);
//                            imageButtonPause.setVisibility(View.VISIBLE);
                        }
                    });

                    // Produces DataSource instances through which media data is loaded.
                    DataSource.Factory dataSourceFactory =
                            new DefaultDataSourceFactory(mContext, Util.getUserAgent(mContext, "Discovrus"));

                    // Produces Extractor instances for parsing the media data.
                    ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                    // This is the MediaSource representing the media to be played.
                    Uri videoUri = Uri.parse(ApiClient.WebService.imageUrl + postData.getPostPath());
                    MediaSource videoSource = new ExtractorMediaSource(videoUri, dataSourceFactory, extractorsFactory, null, null);

                    // Prepare the player with the source.
                    player.prepare(videoSource);
                    player.setPlayWhenReady(true);*/

                    break;
                case "Text":
                    ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams2.width = deviceHalfWidth;
                    layoutParams2.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams2);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.VISIBLE);

                    String title = "";
                    try {
                        title = URLDecoder.decode(postData.getTitle(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String desc = "";
                    try {
                        desc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mContext.mActivity.getApplicationContext(), "fonts");
                    mBinder.titleTv.setText(CustomHtml.fromHtml(title, fontLoader));
                    mBinder.descTv.setText(CustomHtml.fromHtml(desc, fontLoader));
                    if (desc.contains("<ul>")) {
                        mBinder.descTv.setText(Html.fromHtml(desc));
                        switch (Utils.fontName) {
                            case "dancing_script_bold":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                                break;
                            case "hanaleifill_regular":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                                break;
                            case "merienda_bold":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                                break;
                            case "opificio_light_rounded":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                                break;
                            default:
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                                break;
                        }
                    }
//                    mBinder.titleTv.setText(postData.getTitle());
//                    mBinder.descTv.setText(postData.getDescription());

                    break;
                case "PhotoAndText":
                    ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams3.width = deviceHalfWidth;
                    layoutParams3.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams3);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.VISIBLE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.imageWithTextIv);
//                    mBinder.imgTitleTv.setText(postData.getTitle());
//                    mBinder.imgDescTv.setText(postData.getDescription());
                    String ttl = "";
                    try {
                        ttl = URLDecoder.decode(postData.getTitle(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String dsc = "";
                    try {
                        dsc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    final CaseInsensitiveAssetFontLoader fontLoader1 = new CaseInsensitiveAssetFontLoader(mContext.mActivity.getApplicationContext(), "fonts");
                    mBinder.imgTitleTv.setText(CustomHtml.fromHtml(ttl, fontLoader1));
                    mBinder.imgDescTv.setText(CustomHtml.fromHtml(dsc, fontLoader1));
                    if (dsc.contains("<ul>")) {
                        mBinder.imgDescTv.setText(Html.fromHtml(dsc));
                        switch (Utils.fontName) {
                            case "dancing_script_bold":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                                break;
                            case "hanaleifill_regular":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                                break;
                            case "merienda_bold":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                                break;
                            case "opificio_light_rounded":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                                break;
                            default:
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                                break;
                        }
                    }
                    break;
            }

            if (postData.isLiked())
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            else
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

            if (postData.isCommented())
                mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
            else
                mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));


            mBinder.favoriteIv.setOnClickListener(v -> {
                if (!PostManagementFragment.isDisable) {
                    boolean isActive = !postData.isLiked();
                    if (isActive) {
                        // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                        int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinder.favoritesCountTv.getText().toString()));
                        int addCount = likeCount + 1;
                        String countToPrint = Utils.convertLikes(addCount);
                        mBinder.favoritesCountTv.setText(countToPrint);
                        postData.setLikeCount(Integer.valueOf(countToPrint));
                        postData.setLiked(isActive);
                        mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
                    } else {
                        // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                        int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinder.favoritesCountTv.getText().toString()));
                        if (likeCount != 0) {
                            int minusLikeCount = likeCount - 1;
                            String countToPrint = Utils.convertLikes(minusLikeCount);
                            mBinder.favoritesCountTv.setText(countToPrint);
                            postData.setLikeCount(Integer.valueOf(countToPrint));
                            postData.setLiked(isActive);
                            mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));
                        }
                    }
                    PostManagementFragment.managementFragment.saveLikePost(postData.getPostId(), isActive, postData.getUserID());
                } else if (PostManagementFragment.isDisable) {
                    PostManagementFragment.managementFragment.trendinUi();
                }
            });

            mBinder.mainCard.setOnClickListener(v -> {
                if (!PostManagementFragment.isDisable) {
                    int count = Integer.parseInt(Utils.convertToOriginalCount(mBinder.viewsCountTv.getText().toString()));
                    int addCount = count + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinder.viewsCountTv.setText(countToPrint);
                    postData.setViewCount(Integer.valueOf(countToPrint));
                    String postType = postData.getType();
                    switch (postType) {
                        case "Video":
                            PostManagementFragment.managementFragment.getPostVideoDetails(postData);
                            break;
                        case "Photo":
                            PostManagementFragment.managementFragment.getPostImageDetails(postData);
                            break;
                        case "Text":
                            PostManagementFragment.managementFragment.getPostTextDetails(postData, getAdapterPosition());
                            break;
                        case "PhotoAndText":
                            PostManagementFragment.managementFragment.getPostImageDetails(postData);
                            break;


                    }
                   /* if (postData.getType().equalsIgnoreCase("Video")) {
                        mContext.startActivity(new Intent(mContext, VideoDetailsActivity.class).putExtra("videoUrl", postData.getPostPath()));
                    }*/

                } else if (PostManagementFragment.isDisable) {
                    PostManagementFragment.managementFragment.trendinUi();
                }
            });

            mBinder.viewIv.setOnClickListener(v -> {
                if (!PostManagementFragment.isDisable) {
                /*int viewCount = Integer.parseInt(mBinder.viewsCountTv.getText().toString());
                postData.setViewCount(postData.getViewCount() + 1);
                mBinder.viewsCountTv.setText(String.valueOf(postData.getViewCount()));
                PostManagementFragment.businessProfileFragment.savePostView(postData.getPostId());*/
                    int count = Integer.parseInt(Utils.convertToOriginalCount(mBinder.viewsCountTv.getText().toString()));
                    int addCount = count + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinder.viewsCountTv.setText(countToPrint);
                    postData.setViewCount(Integer.valueOf(countToPrint));

                } else if (PostManagementFragment.isDisable) {
                    PostManagementFragment.managementFragment.trendinUi();
                }
            });
        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }

    /**
     * indicates whether we are doing a local or a remote playback
     *//*
    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    *//**
     * List of various states that we can be in
     *//*
    public enum PlaybackState {
        PLAYING, PAUSED, BUFFERING, IDLE
    }


    private static TrackSelector createTrackSelector() {
        // Create a default TrackSelector
        // Measures bandwidth during playback. Can be null if not required.
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        return trackSelector;
    }*/

}
