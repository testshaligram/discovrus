package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemFollowersListBinding;

public class FollowersListAdapter extends BaseAdapter<String> {

    // region Member Variables
    private PostInnerListAdapter.FooterViewHolder footerViewHolder;
    static Context mContext;
    // endregion

    // region Constructors
    public FollowersListAdapter() {
        super();
    }

    public FollowersListAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemFollowersListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_followers_list, parent, false);

        final FollowersViewHolder holder = new FollowersViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final PostInnerListAdapter.FooterViewHolder holder = new PostInnerListAdapter.FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final FollowersViewHolder holder = (FollowersViewHolder) viewHolder;

        final String follower = getItem(position);
        if (follower != null) {
            holder.bind(follower);
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        PostInnerListAdapter.FooterViewHolder holder = (PostInnerListAdapter.FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new String());
    }

    // region Inner Classes
    public static class FollowersViewHolder extends RecyclerView.ViewHolder {
        ItemFollowersListBinding mBinder;

        public FollowersViewHolder(ItemFollowersListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(String followerData) {
            if (followerData.equalsIgnoreCase("Add new")) {
                mBinder.addIv.setImageResource(R.drawable.add_gradient_button);
                mBinder.addIv.setVisibility(View.VISIBLE);
                mBinder.userProfileIv.setVisibility(View.GONE);
                mBinder.img.setVisibility(View.GONE);
            } else {
                mBinder.addIv.setVisibility(View.GONE);
                mBinder.userProfileIv.setVisibility(View.VISIBLE);
                mBinder.img.setVisibility(View.VISIBLE);
            }

           /* RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.mipmap.user_placeholder);

            Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.onlyImageIv);*/

        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }
}