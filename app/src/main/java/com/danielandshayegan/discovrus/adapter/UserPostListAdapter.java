package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemPostInnerListBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;

public class UserPostListAdapter extends BaseAdapter<PostListData.Post> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    static Context mContext;
    private int selectedPosition = 0;
    /*static PlaybackLocation location;
    static CastSession castSession;
    static PlaybackState playbackState;
    private static long currentPosition = 0;*/
    // endregion

    // region Constructors
    public UserPostListAdapter() {
        super();
    }

    public UserPostListAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemPostInnerListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_post_inner_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 2;

        final PostListData.Post post = getItem(position);
        if (post != null) {
            holder.bind(post, (int) (devicewidth - mContext.getResources().getDimension(R.dimen._15sdp)), (int) (displaymetrics.widthPixels - mContext.getResources().getDimension(R.dimen._15sdp)));
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new PostListData.Post());
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    // region Inner Classes
    public static class PostInnerViewHolder extends RecyclerView.ViewHolder {
        ItemPostInnerListBinding mBinder;
        private DefaultTrackSelector trackSelector;

        public PostInnerViewHolder(ItemPostInnerListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(PostListData.Post postData, int deviceHalfWidth, int deviceFullWidth) {
            mBinder.commentsCountTv.setText(String.valueOf(postData.getCommentCount()));
            mBinder.favoritesCountTv.setText(String.valueOf(postData.getLikeCount()));
            mBinder.viewsCountTv.setText(String.valueOf(postData.getViewCount()));
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder);

            switch (postData.getType()) {
                case "Photo":
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams.width = deviceHalfWidth;
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._7sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.onlyImageIv);

                    break;
                case "Video":
                    ConstraintLayout.LayoutParams layoutParams1 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams1.width = deviceFullWidth;
                    layoutParams1.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._7sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams1);

                    mBinder.videoConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getThumbFileName())
                            .apply(options)
                            .into(mBinder.videoThumbIv);

                    break;
                case "Text":
                    ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams2.width = deviceHalfWidth;
                    layoutParams2.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._7sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams2);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.VISIBLE);

                    mBinder.titleTv.setText(postData.getTitle());
                    mBinder.descTv.setText(postData.getDescription());

                    break;
                case "PhotoAndText":
                    ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams3.width = deviceHalfWidth;
                    layoutParams3.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._7sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams3);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.VISIBLE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.imageWithTextIv);
                    mBinder.imgTitleTv.setText(postData.getTitle());
                    mBinder.imgDescTv.setText(postData.getDescription());

                    break;
            }

            if (postData.isLiked())
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            else
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }
}
