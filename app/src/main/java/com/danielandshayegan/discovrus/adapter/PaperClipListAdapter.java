package com.danielandshayegan.discovrus.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.touchhelper.SwipeAndDragHelper;
import com.danielandshayegan.discovrus.databinding.RowPaperclippedChatBinding;
import com.danielandshayegan.discovrus.models.PaperClippedListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PaperClipListAdapter extends RecyclerView.Adapter<PaperClipListAdapter.MyViewHolder> implements
        SwipeAndDragHelper.ActionCompletionContract {

    List<PaperClippedListData.DataBean> dataBeanList;
    Context context;
    private ItemTouchHelper touchHelper;
    MyViewHolder viewHolder;
    RowPaperclippedChatBinding mBinder;


    public PaperClipListAdapter(List<PaperClippedListData.DataBean> dataBeanList, Context context) {
        this.dataBeanList = dataBeanList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_paperclipped_chat, parent, false);
        viewHolder = new MyViewHolder(mBinder);
        return viewHolder;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (dataBeanList.get(position).getUserImagePath() != null) {
            Picasso.with(context).
                    load(ApiClient.WebService.imageUrl + dataBeanList.get(position).getUserImagePath()).
                    into(holder.mBinder.imgUserPaperclipped);
        }
        holder.mBinder.txtUserName.setText(dataBeanList.get(position).getUserName());
        holder.mBinder.imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBeanList.remove(position);
                notifyItemRemoved(position);
            }
        });

        holder.mBinder.imgUserPaperclipped.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
            /*@Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    touchHelper.startDrag(holder);
                    // holder.mBinder.imgCross.setVisibility(View.GONE);
                    holder.mBinder.imgCross.setVisibility(View.VISIBLE);
                }
                return false;
            }*/
        });

       /* holder.mBinder.imgUserPaperclipped.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    touchHelper.startDrag(holder);
                    // Toast.makeText(context, "down", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });*/


       /* holder.mBinder.imgUserPaperclipped.setOnTouchListener(new OnSwipeTouchListener(context) {
            public void onSwipeTop() {
                touchHelper.startDrag(holder);
                // holder.mBinder.imgCross.setVisibility(View.GONE);
                holder.mBinder.imgCross.setVisibility(View.VISIBLE);
            }
        });*/

    }

    @Override
    public int getItemCount() {

        return dataBeanList.size();
    }

    @Override
    public void onViewMoved(int oldPosition, int newPosition) {
        Toast.makeText(context, "moved", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewSwiped(int position) {
        Toast.makeText(context, "swiped", Toast.LENGTH_SHORT).show();
    }

    /*@Override
    public void isLongPress(boolean isPressed) {
        Toast.makeText(context, "" + isPressed, Toast.LENGTH_SHORT).show();
        viewHolder.mBinder.imgCross.setVisibility(View.VISIBLE);

    }*/


    public void setTouchHelper(ItemTouchHelper touchHelper) {

        this.touchHelper = touchHelper;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowPaperclippedChatBinding mBinder;

        @SuppressLint("ClickableViewAccessibility")
        public MyViewHolder(RowPaperclippedChatBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;

            mBinder.imgUserPaperclipped.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mBinder.imgCross.getVisibility() == View.VISIBLE)
                        mBinder.imgCross.setVisibility(View.GONE);
                }
            });

            mBinder.imgUserPaperclipped.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    if (mBinder.imgCross.getVisibility() == View.GONE)
                        mBinder.imgCross.setVisibility(View.VISIBLE);
                    else
                        mBinder.imgCross.setVisibility(View.GONE);


                  /*  Toast.makeText(context, "" + "long", Toast.LENGTH_SHORT).show();
                    mBinder.imgCross.setVisibility(View.VISIBLE);*/
                    return true;
                }
            });
        }

    }

}
