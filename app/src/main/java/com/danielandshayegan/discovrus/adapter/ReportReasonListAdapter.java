package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowReasonBinding;
import com.danielandshayegan.discovrus.models.ReasonData;

import java.util.List;

public class ReportReasonListAdapter extends RecyclerView.Adapter<ReportReasonListAdapter.reasonViewHolder> {

    private Context context;
    private List<ReasonData.DataBean> reasonData;
    private BaseAdapter.OnItemClickListener listener;
    private String from;

    public ReportReasonListAdapter(Context context, List<ReasonData.DataBean> reasonData, BaseAdapter.OnItemClickListener listener, String from) {
        this.context = context;
        this.reasonData = reasonData;
        this.listener = listener;
        this.from = from;
    }

    @Override
    public reasonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowReasonBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_reason, parent, false);

        reasonViewHolder holder = new reasonViewHolder(mBinder);
        return holder;
    }

    @Override
    public void onBindViewHolder(final reasonViewHolder holder, final int position) {
        setTitle(holder.mBinding.tvReason, reasonData.get(position).getReportReason());
        if (from.equalsIgnoreCase("settings")) {
            holder.mBinding.viewTitle.setVisibility(View.GONE);
            holder.mBinding.ivRightArrow.setVisibility(View.VISIBLE);
            holder.mBinding.viewForSetting.setVisibility(View.VISIBLE);
        } else {
            if (position == reasonData.size() - 1)
                holder.mBinding.viewTitle.setVisibility(View.GONE);
            else
                holder.mBinding.viewTitle.setVisibility(View.VISIBLE);
            holder.mBinding.ivRightArrow.setVisibility(View.GONE);
            holder.mBinding.viewForSetting.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(view -> listener.onItemClick(holder.getAdapterPosition(), holder.itemView));

    }

    private void setTitle(TextView textView, String data) {
        if (data != null) {
            if (!TextUtils.isEmpty(data)) {
                textView.setText(data);
            }
        }
    }

    @Override
    public int getItemCount() {
        return reasonData.size();
    }

    public class reasonViewHolder extends RecyclerView.ViewHolder {
        RowReasonBinding mBinding;

        public reasonViewHolder(RowReasonBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinding = mBinder;
        }

    }

}
