package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowMultiplePostBinding;
import com.danielandshayegan.discovrus.models.MapPostDetailByLatLong;
import com.danielandshayegan.discovrus.models.ReasonData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.prefs.App_pref;

import java.util.List;

public class MultiplePostListAdapter extends RecyclerView.Adapter<MultiplePostListAdapter.postViewHolder> {

    private Context context;
    private List<MapPostDetailByLatLong.DataBean> postList;
    private BaseAdapter.OnItemClickListener listener;

    public MultiplePostListAdapter(Context context, List<MapPostDetailByLatLong.DataBean> postList, BaseAdapter.OnItemClickListener listener) {
        this.context = context;
        this.postList = postList;
        this.listener = listener;
    }

    @Override
    public postViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowMultiplePostBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_multiple_post, parent, false);

        postViewHolder holder = new postViewHolder(mBinder);
        return holder;
    }

    @Override
    public void onBindViewHolder(final postViewHolder holder, final int position) {
        MapPostDetailByLatLong.DataBean postData = postList.get(position);
        if (postList.get(position).getUserType().equalsIgnoreCase("Individual")) {
            setTitle(holder.mBinding.txtUserName, postData.getUserName());
            holder.mBinding.txtType.setVisibility(View.GONE);
        } else {
            setTitle(holder.mBinding.txtUserName, postData.getBusinessName());
            setTitle(holder.mBinding.txtType, postData.getCategoryName());
            holder.mBinding.txtType.setVisibility(View.VISIBLE);
        }
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder);

        Glide.with(context)
                .load(ApiClient.WebService.imageUrl + postData.getUserImagePath())
                .apply(options)
                .into(holder.mBinding.imgUserProfile);

        holder.itemView.setOnClickListener(view -> listener.onItemClick(holder.getAdapterPosition(), holder.itemView));

    }

    private void setTitle(TextView textView, String data) {
        if (data != null) {
            if (!TextUtils.isEmpty(data)) {
                textView.setText(data);
            }
        }
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class postViewHolder extends RecyclerView.ViewHolder {
        RowMultiplePostBinding mBinding;

        public postViewHolder(RowMultiplePostBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinding = mBinder;
        }

    }

}
