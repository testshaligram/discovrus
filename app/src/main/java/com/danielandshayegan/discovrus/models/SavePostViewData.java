package com.danielandshayegan.discovrus.models;

import java.util.List;

public class SavePostViewData {

    /**
     * Data : {"PostViewID":110,"PostID":52,"UserID":18,"ViewCount":4}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostViewID : 110
         * PostID : 52
         * UserID : 18
         * ViewCount : 4
         */

        private int PostViewID;
        private int PostID;
        private int UserID;
        private int ViewCount;

        public int getPostViewID() {
            return PostViewID;
        }

        public void setPostViewID(int PostViewID) {
            this.PostViewID = PostViewID;
        }

        public int getPostID() {
            return PostID;
        }

        public void setPostID(int PostID) {
            this.PostID = PostID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }
    }
}
