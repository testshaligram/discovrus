package com.danielandshayegan.discovrus.models;

import java.util.List;

public class MapPostDetail {

    /**
     * Data : [{"PostId":157,"UserID":17,"Lat":51.5136143,"Long":-0.1365486,"UserName":"Nitin Patel","UserImagePath":"/Files/User/user_20181128_050925472_ProfileImage.png","PostPath":"/Files/NewsFeed/Photo/9197f7b4-d2b0-42d6-8f35-793bce29f3c8.png","ThumbFileName":"","Type":"Photo","UserType":"Business","BusinessName":"Satva","Location":"London, United Kingdom","Title":"","Description":"","Distance":5728.067,"PostCreatedDate":"12/05/2018 11:59:25","PostTimeAgo":"6 days ago","CommentCount":0,"ViewCount":37,"LikeCount":0,"Commented":false,"Liked":false,"PostCountInWeek":4}]
     * Success : true
     * Message : ["Success"]
     * Count :
     * TotalRecord :
     */

    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostId : 157
         * UserID : 17
         * Lat : 51.5136143
         * Long : -0.1365486
         * UserName : Nitin Patel
         * UserImagePath : /Files/User/user_20181128_050925472_ProfileImage.png
         * PostPath : /Files/NewsFeed/Photo/9197f7b4-d2b0-42d6-8f35-793bce29f3c8.png
         * ThumbFileName :
         * Type : Photo
         * UserType : Business
         * BusinessName : Satva
         * Location : London, United Kingdom
         * Title :
         * Description :
         * Distance : 5728.067
         * PostCreatedDate : 12/05/2018 11:59:25
         * PostTimeAgo : 6 days ago
         * CommentCount : 0
         * ViewCount : 37
         * LikeCount : 0
         * Commented : false
         * Liked : false
         * PostCountInWeek : 4
         */

        private int PostId;
        private int UserID;
        private double Lat;
        private double Long;
        private String UserName;
        private String UserImagePath;
        private String PostPath;
        private String ThumbFileName;
        private String Type;
        private String UserType;
        private String BusinessName;
        private String Location;
        private String Title;
        private String Description;
        private double Distance;
        private String PostCreatedDate;
        private String PostTimeAgo;
        private int CommentCount;
        private int ViewCount;
        private int LikeCount;
        private boolean Commented;
        private boolean Liked;
        private int PostCountInWeek;

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public double getLat() {
            return Lat;
        }

        public void setLat(double Lat) {
            this.Lat = Lat;
        }

        public double getLong() {
            return Long;
        }

        public void setLong(double Long) {
            this.Long = Long;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getPostPath() {
            return PostPath;
        }

        public void setPostPath(String PostPath) {
            this.PostPath = PostPath;
        }

        public String getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(String ThumbFileName) {
            this.ThumbFileName = ThumbFileName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getPostCreatedDate() {
            return PostCreatedDate;
        }

        public void setPostCreatedDate(String PostCreatedDate) {
            this.PostCreatedDate = PostCreatedDate;
        }

        public String getPostTimeAgo() {
            return PostTimeAgo;
        }

        public void setPostTimeAgo(String PostTimeAgo) {
            this.PostTimeAgo = PostTimeAgo;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }

        public boolean isCommented() {
            return Commented;
        }

        public void setCommented(boolean Commented) {
            this.Commented = Commented;
        }

        public boolean isLiked() {
            return Liked;
        }

        public void setLiked(boolean Liked) {
            this.Liked = Liked;
        }

        public int getPostCountInWeek() {
            return PostCountInWeek;
        }

        public void setPostCountInWeek(int PostCountInWeek) {
            this.PostCountInWeek = PostCountInWeek;
        }
    }
}
