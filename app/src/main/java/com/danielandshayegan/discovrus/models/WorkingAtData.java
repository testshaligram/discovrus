package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class WorkingAtData extends Observable {


    /**
     * Data : ["Shaligram"]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    private boolean Success;
    private String Count;
    private List<String> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getData() {
        return Data;
    }

    public void setData(List<String> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }
}
