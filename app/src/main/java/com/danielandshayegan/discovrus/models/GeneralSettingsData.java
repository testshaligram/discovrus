package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeneralSettingsData {

    @SerializedName("LoginId")
    @Expose
    private String LoginId;

    @SerializedName("Comments")
    @Expose
    private String Comments;

    @SerializedName("Likes")
    @Expose
    private String Likes;

    @SerializedName("Mentions")
    @Expose
    private String Mentions;

    @SerializedName("NewFollowers")
    @Expose
    private String NewFollowers;

    @SerializedName("References")
    @Expose
    private String References;

    @SerializedName("Stories")
    @Expose
    private String Stories;

    @SerializedName("TrendingNow")
    @Expose
    private String TrendingNow;

    @SerializedName("SavePhotos")
    @Expose
    private String SavePhotos;

    @SerializedName("SaveVideos")
    @Expose
    private String SaveVideos;

    @SerializedName("AllowStorySharing")
    @Expose
    private String AllowStorySharing;

    @SerializedName("SaveStories")
    @Expose
    private String SaveStories;

    @SerializedName("IsCommentSetting")
    @Expose
    private String IsCommentSetting;

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getLikes() {
        return Likes;
    }

    public void setLikes(String likes) {
        Likes = likes;
    }

    public String getMentions() {
        return Mentions;
    }

    public void setMentions(String mentions) {
        Mentions = mentions;
    }

    public String getNewFollowers() {
        return NewFollowers;
    }

    public void setNewFollowers(String newFollowers) {
        NewFollowers = newFollowers;
    }

    public String getReferences() {
        return References;
    }

    public void setReferences(String references) {
        References = references;
    }

    public String getStories() {
        return Stories;
    }

    public void setStories(String stories) {
        Stories = stories;
    }

    public String getTrendingNow() {
        return TrendingNow;
    }

    public void setTrendingNow(String trendingNow) {
        TrendingNow = trendingNow;
    }

    public String getSavePhotos() {
        return SavePhotos;
    }

    public void setSavePhotos(String savePhotos) {
        SavePhotos = savePhotos;
    }

    public String getSaveVideos() {
        return SaveVideos;
    }

    public void setSaveVideos(String saveVideos) {
        SaveVideos = saveVideos;
    }

    public String getAllowStorySharing() {
        return AllowStorySharing;
    }

    public void setAllowStorySharing(String allowStorySharing) {
        AllowStorySharing = allowStorySharing;
    }

    public String getSaveStories() {
        return SaveStories;
    }

    public void setSaveStories(String saveStories) {
        SaveStories = saveStories;
    }

    public String getIsCommentSetting() {
        return IsCommentSetting;
    }

    public void setIsCommentSetting(String isCommentSetting) {
        IsCommentSetting = isCommentSetting;
    }
}
