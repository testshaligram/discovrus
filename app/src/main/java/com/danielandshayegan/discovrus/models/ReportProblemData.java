package com.danielandshayegan.discovrus.models;

import java.util.List;

public class ReportProblemData {

    /**
     * Data : {"PostID":140,"LoginID":23,"ReasonID":3,"Email":"pooja.u@sgit.in","UserID":0,"FeedBack":null,"Reason":"Something Isn't Working"}
     * Success : true
     * Message : ["Successfully."]
     * Count :
     * TotalRecord :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostID : 140
         * LoginID : 23
         * ReasonID : 3
         * Email : pooja.u@sgit.in
         * UserID : 0
         * FeedBack : null
         * Reason : Something Isn't Working
         */

        private int PostID;
        private int LoginID;
        private int ReasonID;
        private String Email;
        private int UserID;
        private Object FeedBack;
        private String Reason;

        public int getPostID() {
            return PostID;
        }

        public void setPostID(int PostID) {
            this.PostID = PostID;
        }

        public int getLoginID() {
            return LoginID;
        }

        public void setLoginID(int LoginID) {
            this.LoginID = LoginID;
        }

        public int getReasonID() {
            return ReasonID;
        }

        public void setReasonID(int ReasonID) {
            this.ReasonID = ReasonID;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public Object getFeedBack() {
            return FeedBack;
        }

        public void setFeedBack(Object FeedBack) {
            this.FeedBack = FeedBack;
        }

        public String getReason() {
            return Reason;
        }

        public void setReason(String Reason) {
            this.Reason = Reason;
        }
    }
}
