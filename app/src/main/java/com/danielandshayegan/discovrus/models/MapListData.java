package com.danielandshayegan.discovrus.models;

import java.util.List;

public class MapListData {
    /**
     * type : FeatureCollection
     * features : [{"type":"Feature","properties":{"Lat":23.039204,"Long":72.575575,"Distance":null,"PostId":1248,"CategoryId":8,"CategoryName":"Government","RadiansDistance":6.064390292449751,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.575575,23.039204]}},{"type":"Feature","properties":{"Lat":23.039204,"Long":72.575575,"Distance":null,"PostId":1248,"CategoryId":8,"CategoryName":"Government","RadiansDistance":6.064390292449751,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.575575,23.039204]}},{"type":"Feature","properties":{"Lat":23.039204,"Long":72.575575,"Distance":null,"PostId":1248,"CategoryId":8,"CategoryName":"Government","RadiansDistance":6.064390292449751,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.575575,23.039204]}},{"type":"Feature","properties":{"Lat":22.978436,"Long":72.603945,"Distance":null,"PostId":1253,"CategoryId":8,"CategoryName":"Government","RadiansDistance":11.061685963406186,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.603945,22.978436]}},{"type":"Feature","properties":{"Lat":22.978436,"Long":72.603945,"Distance":null,"PostId":1253,"CategoryId":8,"CategoryName":"Government","RadiansDistance":11.061685963406186,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.603945,22.978436]}},{"type":"Feature","properties":{"Lat":22.978436,"Long":72.603945,"Distance":null,"PostId":1253,"CategoryId":8,"CategoryName":"Government","RadiansDistance":11.061685963406186,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.603945,22.978436]}},{"type":"Feature","properties":{"Lat":23.03523,"Long":72.54391,"Distance":null,"PostId":1256,"CategoryId":8,"CategoryName":"Government","RadiansDistance":2.8229937862387637,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.54391,23.03523]}},{"type":"Feature","properties":{"Lat":23.03523,"Long":72.54391,"Distance":null,"PostId":1256,"CategoryId":8,"CategoryName":"Government","RadiansDistance":2.8229937862387637,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.54391,23.03523]}},{"type":"Feature","properties":{"Lat":23.03523,"Long":72.54391,"Distance":null,"PostId":1256,"CategoryId":8,"CategoryName":"Government","RadiansDistance":2.8229937862387637,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.54391,23.03523]}},{"type":"Feature","properties":{"Lat":23.022505,"Long":72.5713621,"Distance":null,"PostId":1711,"CategoryId":8,"CategoryName":"Government","RadiansDistance":5.845617063770624,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.5713621,23.022505]}},{"type":"Feature","properties":{"Lat":23.022505,"Long":72.5713621,"Distance":null,"PostId":1711,"CategoryId":8,"CategoryName":"Government","RadiansDistance":5.845617063770624,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.5713621,23.022505]}},{"type":"Feature","properties":{"Lat":23.022505,"Long":72.5713621,"Distance":null,"PostId":1711,"CategoryId":8,"CategoryName":"Government","RadiansDistance":5.845617063770624,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.5713621,23.022505]}},{"type":"Feature","properties":{"Lat":23.027988,"Long":72.558245,"Distance":null,"PostId":1712,"CategoryId":8,"CategoryName":"Government","RadiansDistance":4.393997923972262,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.558245,23.027988]}},{"type":"Feature","properties":{"Lat":23.027988,"Long":72.558245,"Distance":null,"PostId":1712,"CategoryId":8,"CategoryName":"Government","RadiansDistance":4.393997923972262,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.558245,23.027988]}},{"type":"Feature","properties":{"Lat":23.027988,"Long":72.558245,"Distance":null,"PostId":1712,"CategoryId":8,"CategoryName":"Government","RadiansDistance":4.393997923972262,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.558245,23.027988]}},{"type":"Feature","properties":{"Lat":23.021352,"Long":72.543831,"Distance":null,"PostId":1713,"CategoryId":8,"CategoryName":"Government","RadiansDistance":3.2896978392008727,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.543831,23.021352]}},{"type":"Feature","properties":{"Lat":23.021352,"Long":72.543831,"Distance":null,"PostId":1713,"CategoryId":8,"CategoryName":"Government","RadiansDistance":3.2896978392008727,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.543831,23.021352]}},{"type":"Feature","properties":{"Lat":23.021352,"Long":72.543831,"Distance":null,"PostId":1713,"CategoryId":8,"CategoryName":"Government","RadiansDistance":3.2896978392008727,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.543831,23.021352]}},{"type":"Feature","properties":{"Lat":23.00492,"Long":72.533021,"Distance":null,"PostId":1715,"CategoryId":8,"CategoryName":"Government","RadiansDistance":3.9265547457616146,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.533021,23.00492]}},{"type":"Feature","properties":{"Lat":23.00492,"Long":72.533021,"Distance":null,"PostId":1715,"CategoryId":8,"CategoryName":"Government","RadiansDistance":3.9265547457616146,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.533021,23.00492]}},{"type":"Feature","properties":{"Lat":23.00492,"Long":72.533021,"Distance":null,"PostId":1715,"CategoryId":8,"CategoryName":"Government","RadiansDistance":3.9265547457616146,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.533021,23.00492]}},{"type":"Feature","properties":{"Lat":23.021668,"Long":72.599938,"Distance":null,"PostId":1729,"CategoryId":8,"CategoryName":"Government","RadiansDistance":8.71425385436912,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.599938,23.021668]}},{"type":"Feature","properties":{"Lat":23.021668,"Long":72.599938,"Distance":null,"PostId":1729,"CategoryId":8,"CategoryName":"Government","RadiansDistance":8.71425385436912,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.599938,23.021668]}},{"type":"Feature","properties":{"Lat":23.021668,"Long":72.599938,"Distance":null,"PostId":1729,"CategoryId":8,"CategoryName":"Government","RadiansDistance":8.71425385436912,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.599938,23.021668]}},{"type":"Feature","properties":{"Lat":23.022505,"Long":72.5713621,"Distance":null,"PostId":1730,"CategoryId":8,"CategoryName":"Government","RadiansDistance":5.845617063770624,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.5713621,23.022505]}},{"type":"Feature","properties":{"Lat":23.022505,"Long":72.5713621,"Distance":null,"PostId":1730,"CategoryId":8,"CategoryName":"Government","RadiansDistance":5.845617063770624,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.5713621,23.022505]}},{"type":"Feature","properties":{"Lat":23.022505,"Long":72.5713621,"Distance":null,"PostId":1730,"CategoryId":8,"CategoryName":"Government","RadiansDistance":5.845617063770624,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.5713621,23.022505]}},{"type":"Feature","properties":{"Lat":23.001602,"Long":72.580549,"Distance":null,"PostId":1742,"CategoryId":8,"CategoryName":"Government","RadiansDistance":7.641996352620011,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.580549,23.001602]}},{"type":"Feature","properties":{"Lat":23.001602,"Long":72.580549,"Distance":null,"PostId":1742,"CategoryId":8,"CategoryName":"Government","RadiansDistance":7.641996352620011,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.580549,23.001602]}},{"type":"Feature","properties":{"Lat":23.001602,"Long":72.580549,"Distance":null,"PostId":1742,"CategoryId":8,"CategoryName":"Government","RadiansDistance":7.641996352620011,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.580549,23.001602]}},{"type":"Feature","properties":{"Lat":23.02546,"Long":72.521693,"Distance":null,"PostId":1748,"CategoryId":8,"CategoryName":"Government","RadiansDistance":1.3668081986145724,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.521693,23.02546]}},{"type":"Feature","properties":{"Lat":23.02546,"Long":72.521693,"Distance":null,"PostId":1748,"CategoryId":8,"CategoryName":"Government","RadiansDistance":1.3668081986145724,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.521693,23.02546]}},{"type":"Feature","properties":{"Lat":23.02546,"Long":72.521693,"Distance":null,"PostId":1748,"CategoryId":8,"CategoryName":"Government","RadiansDistance":1.3668081986145724,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.521693,23.02546]}},{"type":"Feature","properties":{"Lat":23.045523,"Long":72.528387,"Distance":null,"PostId":1749,"CategoryId":8,"CategoryName":"Government","RadiansDistance":1.570633668682803,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.528387,23.045523]}},{"type":"Feature","properties":{"Lat":23.045523,"Long":72.528387,"Distance":null,"PostId":1749,"CategoryId":8,"CategoryName":"Government","RadiansDistance":1.570633668682803,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.528387,23.045523]}},{"type":"Feature","properties":{"Lat":23.045523,"Long":72.528387,"Distance":null,"PostId":1749,"CategoryId":8,"CategoryName":"Government","RadiansDistance":1.570633668682803,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.528387,23.045523]}},{"type":"Feature","properties":{"Lat":23.031937,"Long":72.60148,"Distance":null,"PostId":1755,"CategoryId":8,"CategoryName":"Government","RadiansDistance":8.725493576431406,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.60148,23.031937]}},{"type":"Feature","properties":{"Lat":23.031937,"Long":72.60148,"Distance":null,"PostId":1755,"CategoryId":8,"CategoryName":"Government","RadiansDistance":8.725493576431406,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.60148,23.031937]}},{"type":"Feature","properties":{"Lat":23.031937,"Long":72.60148,"Distance":null,"PostId":1755,"CategoryId":8,"CategoryName":"Government","RadiansDistance":8.725493576431406,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.60148,23.031937]}},{"type":"Feature","properties":{"Lat":23.036203,"Long":72.538337,"Distance":null,"PostId":1759,"CategoryId":8,"CategoryName":"Government","RadiansDistance":2.24852816837579,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.538337,23.036203]}},{"type":"Feature","properties":{"Lat":23.036203,"Long":72.538337,"Distance":null,"PostId":1759,"CategoryId":8,"CategoryName":"Government","RadiansDistance":2.24852816837579,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.538337,23.036203]}},{"type":"Feature","properties":{"Lat":23.036203,"Long":72.538337,"Distance":null,"PostId":1759,"CategoryId":8,"CategoryName":"Government","RadiansDistance":2.24852816837579,"Icon":"/files/category/Icon/Government.png"},"geometry":{"type":"Point","coordinates":[72.538337,23.036203]}},{"type":"Feature","properties":{"Lat":23.0342822,"Long":72.508993199999,"Distance":null,"PostId":1854,"CategoryId":9,"CategoryName":"Health","RadiansDistance":0.8026572451907654,"Icon":"/files/category/Icon/Health.png"},"geometry":{"type":"Point","coordinates":[72.508993199999,23.0342822]}},{"type":"Feature","properties":{"Lat":22.9961698,"Long":72.5995843,"Distance":null,"PostId":1856,"CategoryId":9,"CategoryName":"Health","RadiansDistance":9.637042209934547,"Icon":"/files/category/Icon/Health.png"},"geometry":{"type":"Point","coordinates":[72.5995843,22.9961698]}},{"type":"Feature","properties":{"Lat":23.0523843,"Long":72.5337182,"Distance":null,"PostId":1857,"CategoryId":9,"CategoryName":"Health","RadiansDistance":2.4857777397563274,"Icon":"/files/category/Icon/Health.png"},"geometry":{"type":"Point","coordinates":[72.5337182,23.0523843]}},{"type":"Feature","properties":{"Lat":23.037892269298,"Long":72.507678852651,"Distance":null,"PostId":2068,"CategoryId":1,"CategoryName":"Beauty","RadiansDistance":0.8986988952914139,"Icon":"/files/category/Icon/Beauty.png"},"geometry":{"type":"Point","coordinates":[72.507678852651,23.037892269298]}},{"type":"Feature","properties":{"Lat":23.037892269298,"Long":72.507678852651,"Distance":null,"PostId":2068,"CategoryId":1,"CategoryName":"Beauty","RadiansDistance":0.8986988952914139,"Icon":"/files/category/Icon/Beauty.png"},"geometry":{"type":"Point","coordinates":[72.507678852651,23.037892269298]}},{"type":"Feature","properties":{"Lat":23.037892269298,"Long":72.507678852651,"Distance":null,"PostId":2068,"CategoryId":1,"CategoryName":"Beauty","RadiansDistance":0.8986988952914139,"Icon":"/files/category/Icon/Beauty.png"},"geometry":{"type":"Point","coordinates":[72.507678852651,23.037892269298]}},{"type":"Feature","properties":{"Lat":23.037858810349,"Long":72.507706922395,"Distance":null,"PostId":2157,"CategoryId":0,"CategoryName":"","RadiansDistance":0.89532914360749,"Icon":"/files/category/Icon/location-pin.png"},"geometry":{"type":"Point","coordinates":[72.507706922395,23.037858810349]}}]
     * crs : {"type":"name","properties":{"name":""}}
     */

    private String type;
    private CrsBean crs;
    private List<FeaturesBean> features;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CrsBean getCrs() {
        return crs;
    }

    public void setCrs(CrsBean crs) {
        this.crs = crs;
    }

    public List<FeaturesBean> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeaturesBean> features) {
        this.features = features;
    }

    public static class CrsBean {
        /**
         * type : name
         * properties : {"name":""}
         */

        private String type;
        private PropertiesBean properties;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public PropertiesBean getProperties() {
            return properties;
        }

        public void setProperties(PropertiesBean properties) {
            this.properties = properties;
        }

        public static class PropertiesBean {
            /**
             * name :
             */

            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

    public static class FeaturesBean {
        /**
         * type : Feature
         * properties : {"Lat":23.039204,"Long":72.575575,"Distance":null,"PostId":1248,"CategoryId":8,"CategoryName":"Government","RadiansDistance":6.064390292449751,"Icon":"/files/category/Icon/Government.png"}
         * geometry : {"type":"Point","coordinates":[72.575575,23.039204]}
         */

        private String type;
        private PropertiesBeanX properties;
        private GeometryBean geometry;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public PropertiesBeanX getProperties() {
            return properties;
        }

        public void setProperties(PropertiesBeanX properties) {
            this.properties = properties;
        }

        public GeometryBean getGeometry() {
            return geometry;
        }

        public void setGeometry(GeometryBean geometry) {
            this.geometry = geometry;
        }

        public static class PropertiesBeanX {
            /**
             * Lat : 23.039204
             * Long : 72.575575
             * Distance : null
             * PostId : 1248
             * CategoryId : 8
             * CategoryName : Government
             * RadiansDistance : 6.064390292449751
             * Icon : /files/category/Icon/Government.png
             */

            private double Lat;
            private double Long;
            private Object Distance;
            private int PostId;
            private int CategoryId;
            private String CategoryName;
            private double RadiansDistance;
            private String Icon;

            public double getLat() {
                return Lat;
            }

            public void setLat(double Lat) {
                this.Lat = Lat;
            }

            public double getLong() {
                return Long;
            }

            public void setLong(double Long) {
                this.Long = Long;
            }

            public Object getDistance() {
                return Distance;
            }

            public void setDistance(Object Distance) {
                this.Distance = Distance;
            }

            public int getPostId() {
                return PostId;
            }

            public void setPostId(int PostId) {
                this.PostId = PostId;
            }

            public int getCategoryId() {
                return CategoryId;
            }

            public void setCategoryId(int CategoryId) {
                this.CategoryId = CategoryId;
            }

            public String getCategoryName() {
                return CategoryName;
            }

            public void setCategoryName(String CategoryName) {
                this.CategoryName = CategoryName;
            }

            public double getRadiansDistance() {
                return RadiansDistance;
            }

            public void setRadiansDistance(double RadiansDistance) {
                this.RadiansDistance = RadiansDistance;
            }

            public String getIcon() {
                return Icon;
            }

            public void setIcon(String Icon) {
                this.Icon = Icon;
            }
        }

        public static class GeometryBean {
            /**
             * type : Point
             * coordinates : [72.575575,23.039204]
             */

            private String type;
            private List<Double> coordinates;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public List<Double> getCoordinates() {
                return coordinates;
            }

            public void setCoordinates(List<Double> coordinates) {
                this.coordinates = coordinates;
            }
        }
    }
}
