package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CardListEnvelope {

    /**
     * Data : [{"object":"card","AccountId":null,"address_city":null,"address_country":null,"address_line1":"ahmdabad","address_line1_check":"pass","address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"available_payout_methods":null,"brand":"Visa","country":"US","currency":null,"CustomerId":"cus_DSSKu2zwM5yWBZ","cvc_check":"pass","default_for_currency":false,"dynamic_last4":null,"exp_month":4,"exp_year":2042,"fingerprint":"DBmJBHZvgIOehASJ","funding":"credit","last4":"4242","metadata":{},"name":"hiren","RecipientId":null,"three_d_secure":null,"tokenization_method":null,"id":"card_1D1XYVHDuTzFCAhXMOr5eEZF","StripeResponse":null},{"object":"card","AccountId":null,"address_city":null,"address_country":null,"address_line1":"ahmdabad","address_line1_check":"pass","address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"available_payout_methods":null,"brand":"Visa","country":"US","currency":null,"CustomerId":"cus_DSSKu2zwM5yWBZ","cvc_check":"pass","default_for_currency":false,"dynamic_last4":null,"exp_month":12,"exp_year":2023,"fingerprint":"DBmJBHZvgIOehASJ","funding":"credit","last4":"4242","metadata":{},"name":"hiren","RecipientId":null,"three_d_secure":null,"tokenization_method":null,"id":"card_1D4la8HDuTzFCAhXcxO9oBpO","StripeResponse":null},{"object":"card","AccountId":null,"address_city":null,"address_country":null,"address_line1":"ahmdabad","address_line1_check":"pass","address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"available_payout_methods":null,"brand":"Visa","country":"US","currency":null,"CustomerId":"cus_DSSKu2zwM5yWBZ","cvc_check":"pass","default_for_currency":false,"dynamic_last4":null,"exp_month":12,"exp_year":2023,"fingerprint":"DBmJBHZvgIOehASJ","funding":"credit","last4":"4242","metadata":{},"name":"hiren","RecipientId":null,"three_d_secure":null,"tokenization_method":null,"id":"card_1D4k64HDuTzFCAhX5v3Zz5QT","StripeResponse":null},{"object":"card","AccountId":null,"address_city":null,"address_country":null,"address_line1":"ahmdabad","address_line1_check":"pass","address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"available_payout_methods":null,"brand":"Visa","country":"US","currency":null,"CustomerId":"cus_DSSKu2zwM5yWBZ","cvc_check":"pass","default_for_currency":false,"dynamic_last4":null,"exp_month":12,"exp_year":2023,"fingerprint":"DBmJBHZvgIOehASJ","funding":"credit","last4":"4242","metadata":{},"name":"hiren","RecipientId":null,"three_d_secure":null,"tokenization_method":null,"id":"card_1D4k1WHDuTzFCAhXuBWVeV9V","StripeResponse":null},{"object":"card","AccountId":null,"address_city":null,"address_country":null,"address_line1":"ahmdabad","address_line1_check":"pass","address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"available_payout_methods":null,"brand":"MasterCard","country":"US","currency":null,"CustomerId":"cus_DSSKu2zwM5yWBZ","cvc_check":"pass","default_for_currency":false,"dynamic_last4":null,"exp_month":12,"exp_year":2033,"fingerprint":"W56hMjlYhLMF4pIZ","funding":"credit","last4":"4444","metadata":{},"name":"hiren","RecipientId":null,"three_d_secure":null,"tokenization_method":null,"id":"card_1D4jvfHDuTzFCAhXGVr6BZvB","StripeResponse":null},{"object":"card","AccountId":null,"address_city":null,"address_country":null,"address_line1":"ahmdabad","address_line1_check":"pass","address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"available_payout_methods":null,"brand":"MasterCard","country":"US","currency":null,"CustomerId":"cus_DSSKu2zwM5yWBZ","cvc_check":"pass","default_for_currency":false,"dynamic_last4":null,"exp_month":12,"exp_year":2033,"fingerprint":"W56hMjlYhLMF4pIZ","funding":"credit","last4":"4444","metadata":{},"name":"hiren","RecipientId":null,"three_d_secure":null,"tokenization_method":null,"id":"card_1D4jsBHDuTzFCAhXzi8iTIAc","StripeResponse":null}]
     * Success : true
     * Message : []
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> message) {
        Message = message;
    }

    public static class DataBean {
        /**
         * object : card
         * address_city : null
         * address_country : null
         * address_line1 : ahmdabad
         * address_line2 : null
         * address_state : null
         * address_zip : null
         * brand : Visa
         * country : US
         * currency : null
         * CustomerId : cus_DSSKu2zwM5yWBZ
         * exp_month : 4
         * exp_year : 2042
         * fingerprint : DBmJBHZvgIOehASJ
         * funding : credit
         * last4 : 4242
         * name : hiren
         * id : card_1D1XYVHDuTzFCAhXMOr5eEZF
         */

        @SerializedName("object")
        private String object;
        @SerializedName("address_city")
        private Object addressCity;
        @SerializedName("address_country")
        private Object addressCountry;
        @SerializedName("address_line1")
        private String addressLine1;
        @SerializedName("address_line2")
        private Object addressLine2;
        @SerializedName("address_state")
        private Object addressState;
        @SerializedName("address_zip")
        private Object addressZip;
        @SerializedName("brand")
        private String brand;
        @SerializedName("country")
        private String country;
        @SerializedName("currency")
        private Object currency;
        @SerializedName("CustomerId")
        private String CustomerId;
        @SerializedName("exp_month")
        private int expMonth;
        @SerializedName("exp_year")
        private int expYear;
        @SerializedName("fingerprint")
        private String fingerprint;
        @SerializedName("funding")
        private String funding;
        @SerializedName("last4")
        private String last4;
        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private String id;

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public Object getAddressCity() {
            return addressCity;
        }

        public void setAddressCity(Object addressCity) {
            this.addressCity = addressCity;
        }

        public Object getAddressCountry() {
            return addressCountry;
        }

        public void setAddressCountry(Object addressCountry) {
            this.addressCountry = addressCountry;
        }

        public String getAddressLine1() {
            return addressLine1;
        }

        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        public Object getAddressLine2() {
            return addressLine2;
        }

        public void setAddressLine2(Object addressLine2) {
            this.addressLine2 = addressLine2;
        }

        public Object getAddressState() {
            return addressState;
        }

        public void setAddressState(Object addressState) {
            this.addressState = addressState;
        }

        public Object getAddressZip() {
            return addressZip;
        }

        public void setAddressZip(Object addressZip) {
            this.addressZip = addressZip;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Object getCurrency() {
            return currency;
        }

        public void setCurrency(Object currency) {
            this.currency = currency;
        }

        public String getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(String CustomerId) {
            this.CustomerId = CustomerId;
        }

        public int getExpMonth() {
            return expMonth;
        }

        public void setExpMonth(int expMonth) {
            this.expMonth = expMonth;
        }

        public int getExpYear() {
            return expYear;
        }

        public void setExpYear(int expYear) {
            this.expYear = expYear;
        }

        public String getFingerprint() {
            return fingerprint;
        }

        public void setFingerprint(String fingerprint) {
            this.fingerprint = fingerprint;
        }

        public String getFunding() {
            return funding;
        }

        public void setFunding(String funding) {
            this.funding = funding;
        }

        public String getLast4() {
            return last4;
        }

        public void setLast4(String last4) {
            this.last4 = last4;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }
}
