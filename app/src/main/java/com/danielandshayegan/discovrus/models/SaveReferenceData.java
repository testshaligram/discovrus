package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class SaveReferenceData extends Observable {


    /**
     * Data : null
     * Success : true
     * Message : ["User Reference added successfully."]
     * Count :
     */

    private Object Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public Object getData() {
        return Data;
    }

    public void setData(Object Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }
}
