package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleAddressResult {

    /**
     * results : [{"address_components":[{"long_name":"395004","short_name":"395004","types":["postal_code"]},{"long_name":"Surat","short_name":"Surat","types":["locality","political"]},{"long_name":"Surat","short_name":"Surat","types":["administrative_area_level_2","political"]},{"long_name":"Gujarat","short_name":"GJ","types":["administrative_area_level_1","political"]},{"long_name":"India","short_name":"IN","types":["country","political"]}],"formatted_address":"Surat, Gujarat 395004, India","geometry":{"bounds":{"northeast":{"lat":21.256691,"lng":72.8468979},"southwest":{"lat":21.2067763,"lng":72.79251889999999}},"location":{"lat":21.2299596,"lng":72.8209298},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":21.256691,"lng":72.8468979},"southwest":{"lat":21.2067763,"lng":72.79251889999999}}},"place_id":"ChIJ5_ddI7pO4DsR3902FcRA3As","types":["postal_code"]}]
     * status : OK
     */

    @SerializedName("status")
    private String status;
    @SerializedName("results")
    private List<ResultsBean> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * address_components : [{"long_name":"395004","short_name":"395004","types":["postal_code"]},{"long_name":"Surat","short_name":"Surat","types":["locality","political"]},{"long_name":"Surat","short_name":"Surat","types":["administrative_area_level_2","political"]},{"long_name":"Gujarat","short_name":"GJ","types":["administrative_area_level_1","political"]},{"long_name":"India","short_name":"IN","types":["country","political"]}]
         * formatted_address : Surat, Gujarat 395004, India
         * geometry : {"bounds":{"northeast":{"lat":21.256691,"lng":72.8468979},"southwest":{"lat":21.2067763,"lng":72.79251889999999}},"location":{"lat":21.2299596,"lng":72.8209298},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":21.256691,"lng":72.8468979},"southwest":{"lat":21.2067763,"lng":72.79251889999999}}}
         * place_id : ChIJ5_ddI7pO4DsR3902FcRA3As
         * types : ["postal_code"]
         */

        @SerializedName("formatted_address")
        private String formattedAddress;
        @SerializedName("geometry")
        private GeometryBean geometry;
        @SerializedName("place_id")
        private String placeId;
        @SerializedName("address_components")
        private List<AddressComponentsBean> addressComponents;
        @SerializedName("types")
        private List<String> types;

        public String getFormattedAddress() {
            return formattedAddress;
        }

        public void setFormattedAddress(String formattedAddress) {
            this.formattedAddress = formattedAddress;
        }

        public GeometryBean getGeometry() {
            return geometry;
        }

        public void setGeometry(GeometryBean geometry) {
            this.geometry = geometry;
        }

        public String getPlaceId() {
            return placeId;
        }

        public void setPlaceId(String placeId) {
            this.placeId = placeId;
        }

        public List<AddressComponentsBean> getAddressComponents() {
            return addressComponents;
        }

        public void setAddressComponents(List<AddressComponentsBean> addressComponents) {
            this.addressComponents = addressComponents;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }

        public static class GeometryBean {
            /**
             * bounds : {"northeast":{"lat":21.256691,"lng":72.8468979},"southwest":{"lat":21.2067763,"lng":72.79251889999999}}
             * location : {"lat":21.2299596,"lng":72.8209298}
             * location_type : APPROXIMATE
             * viewport : {"northeast":{"lat":21.256691,"lng":72.8468979},"southwest":{"lat":21.2067763,"lng":72.79251889999999}}
             */

            @SerializedName("bounds")
            private BoundsBean bounds;
            @SerializedName("location")
            private LocationBean location;
            @SerializedName("location_type")
            private String locationType;
            @SerializedName("viewport")
            private ViewportBean viewport;

            public BoundsBean getBounds() {
                return bounds;
            }

            public void setBounds(BoundsBean bounds) {
                this.bounds = bounds;
            }

            public LocationBean getLocation() {
                return location;
            }

            public void setLocation(LocationBean location) {
                this.location = location;
            }

            public String getLocationType() {
                return locationType;
            }

            public void setLocationType(String locationType) {
                this.locationType = locationType;
            }

            public ViewportBean getViewport() {
                return viewport;
            }

            public void setViewport(ViewportBean viewport) {
                this.viewport = viewport;
            }

            public static class BoundsBean {
                /**
                 * northeast : {"lat":21.256691,"lng":72.8468979}
                 * southwest : {"lat":21.2067763,"lng":72.79251889999999}
                 */

                @SerializedName("northeast")
                private NortheastBean northeast;
                @SerializedName("southwest")
                private SouthwestBean southwest;

                public NortheastBean getNortheast() {
                    return northeast;
                }

                public void setNortheast(NortheastBean northeast) {
                    this.northeast = northeast;
                }

                public SouthwestBean getSouthwest() {
                    return southwest;
                }

                public void setSouthwest(SouthwestBean southwest) {
                    this.southwest = southwest;
                }

                public static class NortheastBean {
                    /**
                     * lat : 21.256691
                     * lng : 72.8468979
                     */

                    @SerializedName("lat")
                    private double lat;
                    @SerializedName("lng")
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class SouthwestBean {
                    /**
                     * lat : 21.2067763
                     * lng : 72.79251889999999
                     */

                    @SerializedName("lat")
                    private double lat;
                    @SerializedName("lng")
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }

            public static class LocationBean {
                /**
                 * lat : 21.2299596
                 * lng : 72.8209298
                 */

                @SerializedName("lat")
                private double lat;
                @SerializedName("lng")
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class ViewportBean {
                /**
                 * northeast : {"lat":21.256691,"lng":72.8468979}
                 * southwest : {"lat":21.2067763,"lng":72.79251889999999}
                 */

                @SerializedName("northeast")
                private NortheastBeanX northeast;
                @SerializedName("southwest")
                private SouthwestBeanX southwest;

                public NortheastBeanX getNortheast() {
                    return northeast;
                }

                public void setNortheast(NortheastBeanX northeast) {
                    this.northeast = northeast;
                }

                public SouthwestBeanX getSouthwest() {
                    return southwest;
                }

                public void setSouthwest(SouthwestBeanX southwest) {
                    this.southwest = southwest;
                }

                public static class NortheastBeanX {
                    /**
                     * lat : 21.256691
                     * lng : 72.8468979
                     */

                    @SerializedName("lat")
                    private double lat;
                    @SerializedName("lng")
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class SouthwestBeanX {
                    /**
                     * lat : 21.2067763
                     * lng : 72.79251889999999
                     */

                    @SerializedName("lat")
                    private double lat;
                    @SerializedName("lng")
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }
        }

        public static class AddressComponentsBean {
            /**
             * long_name : 395004
             * short_name : 395004
             * types : ["postal_code"]
             */

            @SerializedName("long_name")
            private String longName;
            @SerializedName("short_name")
            private String shortName;
            @SerializedName("types")
            private List<String> types;

            public String getLongName() {
                return longName;
            }

            public void setLongName(String longName) {
                this.longName = longName;
            }

            public String getShortName() {
                return shortName;
            }

            public void setShortName(String shortName) {
                this.shortName = shortName;
            }

            public List<String> getTypes() {
                return types;
            }

            public void setTypes(List<String> types) {
                this.types = types;
            }
        }
    }
}
