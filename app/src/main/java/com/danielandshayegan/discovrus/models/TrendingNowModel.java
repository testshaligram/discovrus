package com.danielandshayegan.discovrus.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Observable;

public class TrendingNowModel extends Observable{


    /**
     * Data : [{"TrendingID":3,"UserID":19,"UserName":"Pooja Ishan","isActive":true,"UserImageName":null,"UserImagePath":"/Files/User/user_20180625_023053617_cropped21753888.jpg","TrendingImagePath":"/Files/TrendingNow/images.png","Type":"","BusinessName":"","PostCreatedDate":"2018-07-02T12:20:35.81","ModifiedDate":"2018-07-05T11:35:47.843","Key":null,"Title":"this is trending now gffh","Description":"&lt;b&gt;description here&lt;/b&gt;"},{"TrendingID":2,"UserID":19,"UserName":"Pooja Ishan","isActive":true,"UserImageName":null,"UserImagePath":"/Files/User/user_20180625_023053617_cropped21753888.jpg","TrendingImagePath":"/Files/TrendingNow/courselist.png","Type":"","BusinessName":"","PostCreatedDate":"2018-07-02T12:20:18.937","ModifiedDate":"2018-07-05T11:36:55.79","Key":null,"Title":"this is title1","Description":"&lt;b&gt;this is desc&lt;b&gt;"},{"TrendingID":1,"UserID":32,"UserName":"Pooja Dixit","isActive":true,"UserImageName":null,"UserImagePath":"/Files/User/user_20180626_043617714_Desert.jpg","TrendingImagePath":"/Files/TrendingNow/Comapnylogo.jpg","Type":"","BusinessName":"","PostCreatedDate":"2018-07-02T12:20:07.54","ModifiedDate":"2018-07-05T11:37:25.783","Key":null,"Title":"this is title","Description":"this is description "}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean implements Parcelable {
        /**
         * TrendingID : 3
         * UserID : 19
         * UserName : Pooja Ishan
         * isActive : true
         * UserImageName : null
         * UserImagePath : /Files/User/user_20180625_023053617_cropped21753888.jpg
         * TrendingImagePath : /Files/TrendingNow/images.png
         * Type :
         * BusinessName :
         * PostCreatedDate : 2018-07-02T12:20:35.81
         * ModifiedDate : 2018-07-05T11:35:47.843
         * Key : null
         * Title : this is trending now gffh
         * Description : &lt;b&gt;description here&lt;/b&gt;
         */

        private int TrendingID;
        private int UserID;
        private String UserName;
        private boolean isActive;
        private Object UserImageName;
        private String UserImagePath;
        private String TrendingImagePath;
        private String Type;
        private String BusinessName;
        private String PostCreatedDate;
        private String ModifiedDate;
        private Object Key;
        private String Title;
        private String Description;

        protected DataBean(Parcel in) {
            TrendingID = in.readInt();
            UserID = in.readInt();
            UserName = in.readString();
            isActive = in.readByte() != 0;
            UserImagePath = in.readString();
            TrendingImagePath = in.readString();
            Type = in.readString();
            BusinessName = in.readString();
            PostCreatedDate = in.readString();
            ModifiedDate = in.readString();
            Title = in.readString();
            Description = in.readString();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public int getTrendingID() {
            return TrendingID;
        }

        public void setTrendingID(int TrendingID) {
            this.TrendingID = TrendingID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public Object getUserImageName() {
            return UserImageName;
        }

        public void setUserImageName(Object UserImageName) {
            this.UserImageName = UserImageName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getTrendingImagePath() {
            return TrendingImagePath;
        }

        public void setTrendingImagePath(String TrendingImagePath) {
            this.TrendingImagePath = TrendingImagePath;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getPostCreatedDate() {
            return PostCreatedDate;
        }

        public void setPostCreatedDate(String PostCreatedDate) {
            this.PostCreatedDate = PostCreatedDate;
        }

        public String getModifiedDate() {
            return ModifiedDate;
        }

        public void setModifiedDate(String ModifiedDate) {
            this.ModifiedDate = ModifiedDate;
        }

        public Object getKey() {
            return Key;
        }

        public void setKey(Object Key) {
            this.Key = Key;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(TrendingID);
            dest.writeInt(UserID);
            dest.writeString(UserName);
            dest.writeByte((byte) (isActive ? 1 : 0));
            dest.writeString(UserImagePath);
            dest.writeString(TrendingImagePath);
            dest.writeString(Type);
            dest.writeString(BusinessName);
            dest.writeString(PostCreatedDate);
            dest.writeString(ModifiedDate);
            dest.writeString(Title);
            dest.writeString(Description);
        }
    }
}
