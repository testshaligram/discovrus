package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class SaveLikeData extends Observable {

    /**
     * Data : {"LikeID":1130,"PostID":0,"UserID":12,"LoginID":160,"IsActive":true,"LikeCount":1,"Message":"Like"}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * LikeID : 1130
         * PostID : 0
         * UserID : 12
         * LoginID : 160
         * IsActive : true
         * LikeCount : 1
         * Message : Like
         */

        private int LikeID;
        private int PostID;
        private int UserID;
        private int LoginID;
        private boolean IsActive;
        private int LikeCount;
        private String Message;

        public int getLikeID() {
            return LikeID;
        }

        public void setLikeID(int LikeID) {
            this.LikeID = LikeID;
        }

        public int getPostID() {
            return PostID;
        }

        public void setPostID(int PostID) {
            this.PostID = PostID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getLoginID() {
            return LoginID;
        }

        public void setLoginID(int LoginID) {
            this.LoginID = LoginID;
        }

        public boolean isIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }
    }
}
