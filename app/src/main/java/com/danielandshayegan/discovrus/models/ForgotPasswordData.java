package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class ForgotPasswordData extends Observable {


    /**
     * Data : {"AdminUserId":10,"CategoryId":0,"CategoryName":null,"Description":null,"ImagePath":null,"FirstName":"vpI","LastName":"user1","Email":"pooja.d@sgit.in","Password":"XgQc9BHq14ONsN3WGlu2Lg==","Phone":null,"Location":null,"CreatedBy":null,"ModifiedBy":null,"AdminUserRoleId":null,"AdminUserRoleName":null,"UserRole":null,"Key":null,"DealerRights":null,"strDealerRights":null,"Status":null,"SocialmediaId":null,"IsSocialmediaId":false,"MailSubject":null,"MailBody":null,"IsMobileAppAccess":null,"MobileAppAccessSpan":null,"IsBlock":null,"BusinessName":null,"WorkingAt":null,"ImageName":null,"Message":null,"IsEmail":true,"IsLinkedin":false,"IsInstagram":null,"EmailId":473,"IsVerified":null,"UserImagePath":null}
     * Success : true
     * Message : ["Your reset password link has been sent to your email address. Please check your email."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * AdminUserId : 10
         * CategoryId : 0
         * CategoryName : null
         * Description : null
         * ImagePath : null
         * FirstName : vpI
         * LastName : user1
         * Email : pooja.d@sgit.in
         * Password : XgQc9BHq14ONsN3WGlu2Lg==
         * Phone : null
         * Location : null
         * CreatedBy : null
         * ModifiedBy : null
         * AdminUserRoleId : null
         * AdminUserRoleName : null
         * UserRole : null
         * Key : null
         * DealerRights : null
         * strDealerRights : null
         * Status : null
         * SocialmediaId : null
         * IsSocialmediaId : false
         * MailSubject : null
         * MailBody : null
         * IsMobileAppAccess : null
         * MobileAppAccessSpan : null
         * IsBlock : null
         * BusinessName : null
         * WorkingAt : null
         * ImageName : null
         * Message : null
         * IsEmail : true
         * IsLinkedin : false
         * IsInstagram : null
         * EmailId : 473
         * IsVerified : null
         * UserImagePath : null
         */

        private int AdminUserId;
        private int CategoryId;
        private Object CategoryName;
        private Object Description;
        private Object ImagePath;
        private String FirstName;
        private String LastName;
        private String Email;
        private String Password;
        private Object Phone;
        private Object Location;
        private Object CreatedBy;
        private Object ModifiedBy;
        private Object AdminUserRoleId;
        private Object AdminUserRoleName;
        private Object UserRole;
        private Object Key;
        private Object DealerRights;
        private Object strDealerRights;
        private Object Status;
        private Object SocialmediaId;
        private boolean IsSocialmediaId;
        private Object MailSubject;
        private Object MailBody;
        private Object IsMobileAppAccess;
        private Object MobileAppAccessSpan;
        private Object IsBlock;
        private Object BusinessName;
        private Object WorkingAt;
        private Object ImageName;
        private Object Message;
        private boolean IsEmail;
        private boolean IsLinkedin;
        private Object IsInstagram;
        private int EmailId;
        private Object IsVerified;
        private Object UserImagePath;

        public int getAdminUserId() {
            return AdminUserId;
        }

        public void setAdminUserId(int AdminUserId) {
            this.AdminUserId = AdminUserId;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public Object getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(Object CategoryName) {
            this.CategoryName = CategoryName;
        }

        public Object getDescription() {
            return Description;
        }

        public void setDescription(Object Description) {
            this.Description = Description;
        }

        public Object getImagePath() {
            return ImagePath;
        }

        public void setImagePath(Object ImagePath) {
            this.ImagePath = ImagePath;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public Object getPhone() {
            return Phone;
        }

        public void setPhone(Object Phone) {
            this.Phone = Phone;
        }

        public Object getLocation() {
            return Location;
        }

        public void setLocation(Object Location) {
            this.Location = Location;
        }

        public Object getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(Object CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public Object getModifiedBy() {
            return ModifiedBy;
        }

        public void setModifiedBy(Object ModifiedBy) {
            this.ModifiedBy = ModifiedBy;
        }

        public Object getAdminUserRoleId() {
            return AdminUserRoleId;
        }

        public void setAdminUserRoleId(Object AdminUserRoleId) {
            this.AdminUserRoleId = AdminUserRoleId;
        }

        public Object getAdminUserRoleName() {
            return AdminUserRoleName;
        }

        public void setAdminUserRoleName(Object AdminUserRoleName) {
            this.AdminUserRoleName = AdminUserRoleName;
        }

        public Object getUserRole() {
            return UserRole;
        }

        public void setUserRole(Object UserRole) {
            this.UserRole = UserRole;
        }

        public Object getKey() {
            return Key;
        }

        public void setKey(Object Key) {
            this.Key = Key;
        }

        public Object getDealerRights() {
            return DealerRights;
        }

        public void setDealerRights(Object DealerRights) {
            this.DealerRights = DealerRights;
        }

        public Object getStrDealerRights() {
            return strDealerRights;
        }

        public void setStrDealerRights(Object strDealerRights) {
            this.strDealerRights = strDealerRights;
        }

        public Object getStatus() {
            return Status;
        }

        public void setStatus(Object Status) {
            this.Status = Status;
        }

        public Object getSocialmediaId() {
            return SocialmediaId;
        }

        public void setSocialmediaId(Object SocialmediaId) {
            this.SocialmediaId = SocialmediaId;
        }

        public boolean isIsSocialmediaId() {
            return IsSocialmediaId;
        }

        public void setIsSocialmediaId(boolean IsSocialmediaId) {
            this.IsSocialmediaId = IsSocialmediaId;
        }

        public Object getMailSubject() {
            return MailSubject;
        }

        public void setMailSubject(Object MailSubject) {
            this.MailSubject = MailSubject;
        }

        public Object getMailBody() {
            return MailBody;
        }

        public void setMailBody(Object MailBody) {
            this.MailBody = MailBody;
        }

        public Object getIsMobileAppAccess() {
            return IsMobileAppAccess;
        }

        public void setIsMobileAppAccess(Object IsMobileAppAccess) {
            this.IsMobileAppAccess = IsMobileAppAccess;
        }

        public Object getMobileAppAccessSpan() {
            return MobileAppAccessSpan;
        }

        public void setMobileAppAccessSpan(Object MobileAppAccessSpan) {
            this.MobileAppAccessSpan = MobileAppAccessSpan;
        }

        public Object getIsBlock() {
            return IsBlock;
        }

        public void setIsBlock(Object IsBlock) {
            this.IsBlock = IsBlock;
        }

        public Object getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(Object BusinessName) {
            this.BusinessName = BusinessName;
        }

        public Object getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(Object WorkingAt) {
            this.WorkingAt = WorkingAt;
        }

        public Object getImageName() {
            return ImageName;
        }

        public void setImageName(Object ImageName) {
            this.ImageName = ImageName;
        }

        public Object getMessage() {
            return Message;
        }

        public void setMessage(Object Message) {
            this.Message = Message;
        }

        public boolean isIsEmail() {
            return IsEmail;
        }

        public void setIsEmail(boolean IsEmail) {
            this.IsEmail = IsEmail;
        }

        public boolean isIsLinkedin() {
            return IsLinkedin;
        }

        public void setIsLinkedin(boolean IsLinkedin) {
            this.IsLinkedin = IsLinkedin;
        }

        public Object getIsInstagram() {
            return IsInstagram;
        }

        public void setIsInstagram(Object IsInstagram) {
            this.IsInstagram = IsInstagram;
        }

        public int getEmailId() {
            return EmailId;
        }

        public void setEmailId(int EmailId) {
            this.EmailId = EmailId;
        }

        public Object getIsVerified() {
            return IsVerified;
        }

        public void setIsVerified(Object IsVerified) {
            this.IsVerified = IsVerified;
        }

        public Object getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(Object UserImagePath) {
            this.UserImagePath = UserImagePath;
        }
    }
}
