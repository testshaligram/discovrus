package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class PaperClippedListData extends Observable {


    /**
     * Data : [{"ID":18,"LoginID":19,"UserId":22,"IsActive":true,"UserName":"Pooja Mistri ","UserImagePath":"/Files/User/user_20180625_033931637_ProfileImage.png"},{"ID":17,"LoginID":19,"UserId":15,"IsActive":true,"UserName":"Prakash Parmar","UserImagePath":"/Files/User/user_20180625_124830964_cropped3439667019326222880.jpg"},{"ID":16,"LoginID":19,"UserId":12,"IsActive":true,"UserName":"hiren dixit","UserImagePath":"/Files/User/user_20180625_120336140_cropped1603633929.jpg"},{"ID":15,"LoginID":19,"UserId":14,"IsActive":true,"UserName":"tester vop1","UserImagePath":"/Files/User/user_20180625_122847942_image.png"},{"ID":14,"LoginID":19,"UserId":3,"IsActive":true,"UserName":"Jess1 Joe1","UserImagePath":"/Files/User/user_20180622_073454486_ProfileImage.png"},{"ID":10,"LoginID":19,"UserId":26,"IsActive":true,"UserName":"Shal Gram","UserImagePath":"/Files/User/user_20180625_050108643_ProfileImage.png"}]
     * Success : true
     * Message : ["Success"]
     * Count : 6
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * ID : 18
         * LoginID : 19
         * UserId : 22
         * IsActive : true
         * UserName : Pooja Mistri
         * UserImagePath : /Files/User/user_20180625_033931637_ProfileImage.png
         */

        private int ID;
        private int LoginID;
        private int UserId;
        private boolean IsActive;
        private String UserName;
        private String UserImagePath;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getLoginID() {
            return LoginID;
        }

        public void setLoginID(int LoginID) {
            this.LoginID = LoginID;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean isIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }
    }
}
