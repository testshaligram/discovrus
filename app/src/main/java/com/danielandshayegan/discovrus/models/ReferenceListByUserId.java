package com.danielandshayegan.discovrus.models;

import java.util.List;

public class ReferenceListByUserId {

    /**
     * Data : [{"FromUserID":2,"ToUserID":17,"ReferenceID":"2","UpdatedDate":"13 days ago","UserImagePath":"/Files/User/user_20181128_033846585_ProfileImage.png","UserName":"Tushar Amkar","Location":"Ahmedabad, Ahmadabad, Gujarat, India, 380058","BusinessName":"","UserType":"Individual","Liked":false,"LikeCount":1,"TotalRecord":0,"LoginId":2,"ReferenceText":[{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]},{"FromUserID":29,"ToUserID":17,"ReferenceID":"3","UpdatedDate":"4 days ago","UserImagePath":"/Files/User/user_20181129_082226386_ProfileImage.png","UserName":"Tushar Amkar","Location":"Ahmedabad, Gujarat, India, 380006","BusinessName":"Kudos Think Tank","UserType":"Business","Liked":false,"LikeCount":0,"TotalRecord":0,"LoginId":2,"ReferenceText":[{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]},{"FromUserID":35,"ToUserID":17,"ReferenceID":"2","UpdatedDate":"11 days ago","UserImagePath":"/Files/User/user_20181130_052736563_ProfileImage.png","UserName":"Testerone VopOne","Location":"Ahmedabad, Gujarat, India, 380001","BusinessName":"My Business Inc.","UserType":"Business","Liked":false,"LikeCount":2,"TotalRecord":0,"LoginId":2,"ReferenceText":[{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]}]
     * Success : true
     * Message : ["Success"]
     * Count : 1/0.3
     * TotalRecord :
     */

    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * FromUserID : 2
         * ToUserID : 17
         * ReferenceID : 2
         * UpdatedDate : 13 days ago
         * UserImagePath : /Files/User/user_20181128_033846585_ProfileImage.png
         * UserName : Tushar Amkar
         * Location : Ahmedabad, Ahmadabad, Gujarat, India, 380058
         * BusinessName :
         * UserType : Individual
         * Liked : false
         * LikeCount : 1
         * TotalRecord : 0
         * LoginId : 2
         * ReferenceText : [{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]
         */

        private int FromUserID;
        private int ToUserID;
        private String ReferenceID;
        private String UpdatedDate;
        private String UserImagePath;
        private String UserName;
        private String Location;
        private String BusinessName;
        private String UserType;
        private boolean Liked;
        private int LikeCount;
        private int TotalRecord;
        private int LoginId;
        private List<ReferenceTextBean> ReferenceText;

        public int getFromUserID() {
            return FromUserID;
        }

        public void setFromUserID(int FromUserID) {
            this.FromUserID = FromUserID;
        }

        public int getToUserID() {
            return ToUserID;
        }

        public void setToUserID(int ToUserID) {
            this.ToUserID = ToUserID;
        }

        public String getReferenceID() {
            return ReferenceID;
        }

        public void setReferenceID(String ReferenceID) {
            this.ReferenceID = ReferenceID;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public boolean isLiked() {
            return Liked;
        }

        public void setLiked(boolean Liked) {
            this.Liked = Liked;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }

        public int getTotalRecord() {
            return TotalRecord;
        }

        public void setTotalRecord(int TotalRecord) {
            this.TotalRecord = TotalRecord;
        }

        public int getLoginId() {
            return LoginId;
        }

        public void setLoginId(int LoginId) {
            this.LoginId = LoginId;
        }

        public List<ReferenceTextBean> getReferenceText() {
            return ReferenceText;
        }

        public void setReferenceText(List<ReferenceTextBean> ReferenceText) {
            this.ReferenceText = ReferenceText;
        }

        public static class ReferenceTextBean {
            /**
             * ReferenceID : 1
             * ReferenceText : Reliable
             */

            private int ReferenceID;
            private String ReferenceText;
            private boolean clickable;
            private String reviewId;

            public int getReferenceID() {
                return ReferenceID;
            }

            public void setReferenceID(int ReferenceID) {
                this.ReferenceID = ReferenceID;
            }

            public String getReferenceText() {
                return ReferenceText;
            }

            public void setReferenceText(String ReferenceText) {
                this.ReferenceText = ReferenceText;
            }

            public boolean isClickable() {
                return clickable;
            }

            public void setClickable(boolean clickable) {
                this.clickable = clickable;
            }

            public String getReviewId() {
                return reviewId;
            }

            public void setReviewId(String reviewId) {
                this.reviewId = reviewId;
            }
        }
    }
}
