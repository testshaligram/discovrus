package com.danielandshayegan.discovrus.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class SavePostListData {

    /**
     * Data : [{"UserID":152,"UserName":"Testernew Shaligram","BusinessName":"","UserType":"Individual","UserImagePath":"/Files/User/user_20181102_011914917_ProfileImage.png","WorkingAt":"Satva Softech","post":[{"UserId":152,"PostId":2090,"FileId":5233,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/64362490-b82e-4b3e-a934-1cd5a31d73c0.png","Type":"Photo","LikeCount":1,"CommentCount":5,"ViewCount":32,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 13:44:53","OriginalImage":"/Files/NewsFeed/Photo/O_64362490-b82e-4b3e-a934-1cd5a31d73c0.png","ThumbFileName":""},{"UserId":152,"PostId":2083,"FileId":5227,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/5ad9de5c-7ca5-4960-93d2-da54286d4a71.png","Type":"Photo","LikeCount":0,"CommentCount":0,"ViewCount":21,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 13:47:36","OriginalImage":"/Files/NewsFeed/Photo/O_5ad9de5c-7ca5-4960-93d2-da54286d4a71.png","ThumbFileName":""},{"UserId":152,"PostId":2084,"FileId":5228,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/44d61f4f-8bf3-4576-b278-61143e9c2d73.png","Type":"Photo","LikeCount":1,"CommentCount":3,"ViewCount":31,"Commented":true,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 13:57:47","OriginalImage":"/Files/NewsFeed/Photo/O_44d61f4f-8bf3-4576-b278-61143e9c2d73.png","ThumbFileName":""},{"UserId":152,"PostId":2077,"FileId":0,"Title":"news test post in town","Description":"Testing a post is a testing and thats unit testing \n\nAhhbdhjnsjjjskkskkksk\\ud83d\\ude43\\ud83d\\ude42\\ud83d\\ude42\\ud83d\\ude42\\ud83d\\ude43\\ud83d\\ude42\\ud83d\\ude43\\ud83d\\ude42\nEmoji s testin","PostPath":"","Type":"Text","LikeCount":0,"CommentCount":0,"ViewCount":20,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 14:02:42","OriginalImage":"","ThumbFileName":""},{"UserId":152,"PostId":2021,"FileId":5187,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/42c025e4-b5a3-4d32-ab7d-cf7db01012f6.png","Type":"Photo","LikeCount":1,"CommentCount":1,"ViewCount":20,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"37 mins","OriginalImage":"/Files/NewsFeed/Photo/O_42c025e4-b5a3-4d32-ab7d-cf7db01012f6.png","ThumbFileName":""}]}]
     * Success : true
     * Message : ["Success"]
     * Count : 1/0.1
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * UserID : 152
         * UserName : Testernew Shaligram
         * BusinessName :
         * UserType : Individual
         * UserImagePath : /Files/User/user_20181102_011914917_ProfileImage.png
         * WorkingAt : Satva Softech
         * post : [{"UserId":152,"PostId":2090,"FileId":5233,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/64362490-b82e-4b3e-a934-1cd5a31d73c0.png","Type":"Photo","LikeCount":1,"CommentCount":5,"ViewCount":32,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 13:44:53","OriginalImage":"/Files/NewsFeed/Photo/O_64362490-b82e-4b3e-a934-1cd5a31d73c0.png","ThumbFileName":""},{"UserId":152,"PostId":2083,"FileId":5227,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/5ad9de5c-7ca5-4960-93d2-da54286d4a71.png","Type":"Photo","LikeCount":0,"CommentCount":0,"ViewCount":21,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 13:47:36","OriginalImage":"/Files/NewsFeed/Photo/O_5ad9de5c-7ca5-4960-93d2-da54286d4a71.png","ThumbFileName":""},{"UserId":152,"PostId":2084,"FileId":5228,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/44d61f4f-8bf3-4576-b278-61143e9c2d73.png","Type":"Photo","LikeCount":1,"CommentCount":3,"ViewCount":31,"Commented":true,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 13:57:47","OriginalImage":"/Files/NewsFeed/Photo/O_44d61f4f-8bf3-4576-b278-61143e9c2d73.png","ThumbFileName":""},{"UserId":152,"PostId":2077,"FileId":0,"Title":"news test post in town","Description":"Testing a post is a testing and thats unit testing \n\nAhhbdhjnsjjjskkskkksk\\ud83d\\ude43\\ud83d\\ude42\\ud83d\\ude42\\ud83d\\ude42\\ud83d\\ude43\\ud83d\\ude42\\ud83d\\ude43\\ud83d\\ude42\nEmoji s testin","PostPath":"","Type":"Text","LikeCount":0,"CommentCount":0,"ViewCount":20,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"11/20/2018 14:02:42","OriginalImage":"","ThumbFileName":""},{"UserId":152,"PostId":2021,"FileId":5187,"Title":"","Description":"","PostPath":"/Files/NewsFeed/Photo/42c025e4-b5a3-4d32-ab7d-cf7db01012f6.png","Type":"Photo","LikeCount":1,"CommentCount":1,"ViewCount":20,"Commented":false,"Liked":false,"Viewed":true,"PostCreatedDate":"37 mins","OriginalImage":"/Files/NewsFeed/Photo/O_42c025e4-b5a3-4d32-ab7d-cf7db01012f6.png","ThumbFileName":""}]
         */

        private int UserID;
        private String UserName;
        private String BusinessName;
        private String UserType;
        private String UserImagePath;
        private String WorkingAt;
        private List<PostBean> post;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(String WorkingAt) {
            this.WorkingAt = WorkingAt;
        }

        public List<PostBean> getPost() {
            return post;
        }

        public void setPost(List<PostBean> post) {
            this.post = post;
        }

        public static class PostBean implements Parcelable {
            /**
             * UserId : 152
             * PostId : 2090
             * FileId : 5233
             * Title :
             * Description :
             * PostPath : /Files/NewsFeed/Photo/64362490-b82e-4b3e-a934-1cd5a31d73c0.png
             * Type : Photo
             * LikeCount : 1
             * CommentCount : 5
             * ViewCount : 32
             * Commented : false
             * Liked : false
             * Viewed : true
             * PostCreatedDate : 11/20/2018 13:44:53
             * OriginalImage : /Files/NewsFeed/Photo/O_64362490-b82e-4b3e-a934-1cd5a31d73c0.png
             * ThumbFileName :
             */

            private int UserId;
            private int PostId;
            private int FileId;
            private String UserName;
            private String BusinessName;
            private String UserType;
            private String Title;
            private String Description;
            private String PostPath;
            private String Type;
            private int LikeCount;
            private int CommentCount;
            private int ViewCount;
            private boolean Commented;
            private boolean Liked;
            private boolean Viewed;
            private String PostCreatedDate;
            private String OriginalImage;
            private String ThumbFileName;

            public int getUserId() {
                return UserId;
            }

            public void setUserId(int UserId) {
                this.UserId = UserId;
            }

            public int getPostId() {
                return PostId;
            }

            public void setPostId(int PostId) {
                this.PostId = PostId;
            }

            public int getFileId() {
                return FileId;
            }

            public void setFileId(int FileId) {
                this.FileId = FileId;
            }

            public String getUserName() {
                return UserName;
            }

            public void setUserName(String userName) {
                UserName = userName;
            }

            public String getBusinessName() {
                return BusinessName;
            }

            public void setBusinessName(String businessName) {
                BusinessName = businessName;
            }

            public String getUserType() {
                return UserType;
            }

            public void setUserType(String userType) {
                UserType = userType;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String Title) {
                this.Title = Title;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String Description) {
                this.Description = Description;
            }

            public String getPostPath() {
                return PostPath;
            }

            public void setPostPath(String PostPath) {
                this.PostPath = PostPath;
            }

            public String getType() {
                return Type;
            }

            public void setType(String Type) {
                this.Type = Type;
            }

            public int getLikeCount() {
                return LikeCount;
            }

            public void setLikeCount(int LikeCount) {
                this.LikeCount = LikeCount;
            }

            public int getCommentCount() {
                return CommentCount;
            }

            public void setCommentCount(int CommentCount) {
                this.CommentCount = CommentCount;
            }

            public int getViewCount() {
                return ViewCount;
            }

            public void setViewCount(int ViewCount) {
                this.ViewCount = ViewCount;
            }

            public boolean isCommented() {
                return Commented;
            }

            public void setCommented(boolean Commented) {
                this.Commented = Commented;
            }

            public boolean isLiked() {
                return Liked;
            }

            public void setLiked(boolean Liked) {
                this.Liked = Liked;
            }

            public boolean isViewed() {
                return Viewed;
            }

            public void setViewed(boolean Viewed) {
                this.Viewed = Viewed;
            }

            public String getPostCreatedDate() {
                return PostCreatedDate;
            }

            public void setPostCreatedDate(String PostCreatedDate) {
                this.PostCreatedDate = PostCreatedDate;
            }

            public String getOriginalImage() {
                return OriginalImage;
            }

            public void setOriginalImage(String OriginalImage) {
                this.OriginalImage = OriginalImage;
            }

            public String getThumbFileName() {
                return ThumbFileName;
            }

            public void setThumbFileName(String ThumbFileName) {
                this.ThumbFileName = ThumbFileName;
            }


            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.UserId);
                dest.writeInt(this.PostId);
                dest.writeInt(this.FileId);
                dest.writeString(this.UserName);
                dest.writeString(this.BusinessName);
                dest.writeString(this.UserType);
                dest.writeString(this.Title);
                dest.writeString(this.Description);
                dest.writeString(this.PostPath);
                dest.writeString(this.Type);
                dest.writeInt(this.LikeCount);
                dest.writeInt(this.CommentCount);
                dest.writeInt(this.ViewCount);
                dest.writeByte(this.Commented ? (byte) 1 : (byte) 0);
                dest.writeByte(this.Liked ? (byte) 1 : (byte) 0);
                dest.writeByte(this.Viewed ? (byte) 1 : (byte) 0);
                dest.writeString(this.PostCreatedDate);
                dest.writeString(this.OriginalImage);
                dest.writeString(this.ThumbFileName);
            }

            public PostBean() {
            }

            protected PostBean(Parcel in) {
                this.UserId = in.readInt();
                this.PostId = in.readInt();
                this.FileId = in.readInt();
                this.UserName = in.readString();
                this.BusinessName = in.readString();
                this.UserType = in.readString();
                this.Title = in.readString();
                this.Description = in.readString();
                this.PostPath = in.readString();
                this.Type = in.readString();
                this.LikeCount = in.readInt();
                this.CommentCount = in.readInt();
                this.ViewCount = in.readInt();
                this.Commented = in.readByte() != 0;
                this.Liked = in.readByte() != 0;
                this.Viewed = in.readByte() != 0;
                this.PostCreatedDate = in.readString();
                this.OriginalImage = in.readString();
                this.ThumbFileName = in.readString();
            }

            public static final Creator<PostBean> CREATOR = new Creator<PostBean>() {
                @Override
                public PostBean createFromParcel(Parcel source) {
                    return new PostBean(source);
                }

                @Override
                public PostBean[] newArray(int size) {
                    return new PostBean[size];
                }
            };
        }
    }
}