package com.danielandshayegan.discovrus.models;

import android.os.Parcel;
import android.os.Parcelable;

public class SocialMediaLoginData implements Parcelable {

    String firstName, lastName, emailAddress, photo, id;
    boolean isLinkedIn;
    boolean isFaceBook;


    protected SocialMediaLoginData(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        emailAddress = in.readString();
        photo = in.readString();
        id = in.readString();
        isLinkedIn = in.readByte() != 0;
        isFaceBook = in.readByte() != 0;
    }

    public static final Creator<SocialMediaLoginData> CREATOR = new Creator<SocialMediaLoginData>() {
        @Override
        public SocialMediaLoginData createFromParcel(Parcel in) {
            return new SocialMediaLoginData(in);
        }

        @Override
        public SocialMediaLoginData[] newArray(int size) {
            return new SocialMediaLoginData[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLinkedIn() {
        return isLinkedIn;
    }

    public void setLinkedIn(boolean linkedIn) {
        isLinkedIn = linkedIn;
    }

    public boolean getIsFaceBook() {
        return isFaceBook;
    }

    public void setFaceBook(boolean faceBook) {
        isFaceBook = faceBook;
    }

    public SocialMediaLoginData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(emailAddress);
        dest.writeString(photo);
        dest.writeString(id);
        dest.writeByte((byte) (isLinkedIn ? 1 : 0));
        dest.writeByte((byte) (isFaceBook ? 1 : 0));

    }
}
