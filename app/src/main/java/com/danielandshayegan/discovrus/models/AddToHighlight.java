package com.danielandshayegan.discovrus.models;

import java.util.List;

public class AddToHighlight {

    /**
     * Success : true
     * Message : ["Removed post from Highlight Successfully."]
     * Count :
     * TotalRecord :
     */

    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }
}
