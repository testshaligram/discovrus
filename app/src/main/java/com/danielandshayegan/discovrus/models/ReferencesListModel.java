package com.danielandshayegan.discovrus.models;

import java.util.List;

public class ReferencesListModel {

    /**
     * Data : [{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]
     * Success : true
     * Message : ["Success"]
     * Count : 3
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * ReferenceID : 1
         * ReferenceText : Reliable
         */

        private int ReferenceID;
        private String ReferenceText;
        private boolean clickable;
        private String reviewId;

        public int getReferenceID() {
            return ReferenceID;
        }

        public void setReferenceID(int ReferenceID) {
            this.ReferenceID = ReferenceID;
        }

        public String getReferenceText() {
            return ReferenceText;
        }

        public void setReferenceText(String ReferenceText) {
            this.ReferenceText = ReferenceText;
        }

        public boolean isClickable() {
            return clickable;
        }

        public void setClickable(boolean clickable) {
            this.clickable = clickable;
        }

        public String getReviewId() {
            return reviewId;
        }

        public void setReviewId(String reviewId) {
            this.reviewId = reviewId;
        }
    }
}
