package com.danielandshayegan.discovrus.models;

import java.util.List;

public class TrendingDetailsByIdModel {


    /**
     * Data : {"TrendingID":5,"PostID":null,"UserID":14,"isActive":true,"ImagePath":"/Files/TrendingNow/images.png","ImageName":"d:\\atulm\\discovrus\\webapi\\files\\trendingnow\\images.png","UserName":"tester vop1","Type":"","BusinessName":"","ImageMappingPath":null,"Title":"classification, preparation, and applications","Description":"a) Probe sonication. The tip of a sonicator is directly engrossed into the liposome dispersion. The energy input into lipid dispersion is very high in this method. The coupling of energy at the tip results in local hotness; therefore, the vessel must be engrossed into a water/ice bath. Throughout the sonication up to 1 h, more than 5% of the lipids can be de-esterified. Also, with the probe sonicator, titanium will slough off and pollute the solution.\r\n\r\nb) Bath sonication. The liposome dispersion in a cylinder is placed into a bath sonicator. Controlling the temperature of the lipid dispersion is usually easier in this method, in contrast to sonication by dispersal directly using the tip. The material being sonicated can be protected in a sterile vessel, dissimilar the probe units, or under an inert atmosphere"}
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * TrendingID : 5
         * PostID : null
         * UserID : 14
         * isActive : true
         * ImagePath : /Files/TrendingNow/images.png
         * ImageName : d:\atulm\discovrus\webapi\files\trendingnow\images.png
         * UserName : tester vop1
         * Type :
         * BusinessName :
         * ImageMappingPath : null
         * Title : classification, preparation, and applications
         * Description : a) Probe sonication. The tip of a sonicator is directly engrossed into the liposome dispersion. The energy input into lipid dispersion is very high in this method. The coupling of energy at the tip results in local hotness; therefore, the vessel must be engrossed into a water/ice bath. Throughout the sonication up to 1 h, more than 5% of the lipids can be de-esterified. Also, with the probe sonicator, titanium will slough off and pollute the solution.

         b) Bath sonication. The liposome dispersion in a cylinder is placed into a bath sonicator. Controlling the temperature of the lipid dispersion is usually easier in this method, in contrast to sonication by dispersal directly using the tip. The material being sonicated can be protected in a sterile vessel, dissimilar the probe units, or under an inert atmosphere
         */

        private int TrendingID;
        private Object PostID;
        private int UserID;
        private boolean isActive;
        private String ImagePath;
        private String ImageName;
        private String UserName;
        private String Type;
        private String BusinessName;
        private Object ImageMappingPath;
        private String Title;
        private String Description;

        public int getTrendingID() {
            return TrendingID;
        }

        public void setTrendingID(int TrendingID) {
            this.TrendingID = TrendingID;
        }

        public Object getPostID() {
            return PostID;
        }

        public void setPostID(Object PostID) {
            this.PostID = PostID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String ImagePath) {
            this.ImagePath = ImagePath;
        }

        public String getImageName() {
            return ImageName;
        }

        public void setImageName(String ImageName) {
            this.ImageName = ImageName;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public Object getImageMappingPath() {
            return ImageMappingPath;
        }

        public void setImageMappingPath(Object ImageMappingPath) {
            this.ImageMappingPath = ImageMappingPath;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }
    }
}
