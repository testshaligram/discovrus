package com.danielandshayegan.discovrus.models;

import java.util.List;

public class SaveReferenceLikeData {

    /**
     * Data : {"UserID":7,"LoginID":76,"IsActive":true,"Message":"Like"}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     * TotalRecord :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * UserID : 7
         * LoginID : 76
         * IsActive : true
         * Message : Like
         */

        private int UserID;
        private int LoginID;
        private boolean IsActive;
        private String Message;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getLoginID() {
            return LoginID;
        }

        public void setLoginID(int LoginID) {
            this.LoginID = LoginID;
        }

        public boolean isIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }
    }
}
