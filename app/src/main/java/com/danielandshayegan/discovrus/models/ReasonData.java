package com.danielandshayegan.discovrus.models;

import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class ReasonData {

    /**
     * Data : [{"ReasonID":1,"ReportReason":"Abusive Content"},{"ReasonID":2,"ReportReason":"General Feedback"},{"ReasonID":3,"ReportReason":"Something Isn't Working"},{"ReasonID":4,"ReportReason":"Spam"},{"ReasonID":5,"ReportReason":"I think it's inappropriate for Discovrus"},{"ReasonID":6,"ReportReason":"I think it's fake, spam or a scam"}]
     * Success : true
     * Message : ["Success"]
     * Count :
     * TotalRecord :
     */

    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean implements Serializable {
        /**
         * ReasonID : 1
         * ReportReason : Abusive Content
         */

        private int ReasonID;
        private String ReportReason;

        public int getReasonID() {
            return ReasonID;
        }

        public void setReasonID(int ReasonID) {
            this.ReasonID = ReasonID;
        }

        public String getReportReason() {
            return ReportReason;
        }

        public void setReportReason(String ReportReason) {
            this.ReportReason = ReportReason;
        }
    }
}
