package com.danielandshayegan.discovrus.models;

public class Book {
    public String title;
    public String author;
    public String description;
    public int imageId;
}
