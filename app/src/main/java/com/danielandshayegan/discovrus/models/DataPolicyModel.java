package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPolicyModel {
    /**
     * Data : [{"PolicyId":1,"Policy":"Our Terms and Policies. You must use our Services according to our Terms and posted policies. If we disable your account for a violation of our Terms, you will not create another account without our permission."}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PolicyId : 1
         * Policy : Our Terms and Policies. You must use our Services according to our Terms and posted policies. If we disable your account for a violation of our Terms, you will not create another account without our permission.
         */

        @SerializedName("PolicyId")
        private int PolicyId;
        @SerializedName("Policy")
        private String Policy;

        public int getPolicyId() {
            return PolicyId;
        }

        public void setPolicyId(int PolicyId) {
            this.PolicyId = PolicyId;
        }

        public String getPolicy() {
            return Policy;
        }

        public void setPolicy(String Policy) {
            this.Policy = Policy;
        }
    }
}
