package com.danielandshayegan.discovrus.models;

import java.util.List;

public class CommonApiResponse {

    /**
     * Data : null
     * Success : true
     * Message : ["User Reference added successfully."]
     * Count :
     */

    private String Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public String getData() {
        return Data;
    }

    public void setData(String Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }
}
