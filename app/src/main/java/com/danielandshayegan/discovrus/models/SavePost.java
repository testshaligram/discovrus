package com.danielandshayegan.discovrus.models;

import java.util.List;

public class SavePost {

    /**
     * Success : true
     * Message : ["Post Saved Successfully."]
     * Count :
     */

    private boolean Success;
    private String Count;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }
}
