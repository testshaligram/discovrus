package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class postDetailData {

    /**
     * Data : [{"PostId":1777,"FileId":5019,"Description":"","Title":"","PostPath":"/Files/NewsFeed/Photo/426eef70-08e3-4b06-9a3d-64bb1a0a0a77.png","OriginalImage":"/Files/NewsFeed/Photo/426eef70-08e3-4b06-9a3d-64bb1a0a0a77.png","Type":"Photo","TagUserID":"0","IsHighLight":false,"isDetailedPost":true,"Lat":"0.0","Long":"0.0","Distance":"0.5 m","Time":"3 H","LikeCount":0,"CommentCount":0,"ViewCount":1,"Liked":false,"Commented":false,"ThumbFileName":null,"PostCreatedDate":"4 hours"}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<PostListData.Post> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<PostListData.Post> getData() {
        return Data;
    }

    public void setData(List<PostListData.Post> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

}
