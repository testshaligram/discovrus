package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowersUserListData {
    /**
     * Data : [{"UserId":12,"UserName":"hiren dixit","UserImagePath":"/Files/User/user_20180625_120336140_cropped1603633929.jpg","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Shaligram ","IsFollowing":false,"IsFollowers":true},{"UserId":22,"UserName":null,"UserImagePath":null,"UserRoleType":null,"BusinessName":null,"WorkingAt":null,"IsFollowing":false,"IsFollowers":false},{"UserId":19,"UserName":null,"UserImagePath":null,"UserRoleType":null,"BusinessName":null,"WorkingAt":null,"IsFollowing":false,"IsFollowers":false},{"UserId":3,"UserName":"Jess1 Joe1","UserImagePath":"/Files/User/user_20180622_073454486_ProfileImage.png","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Indra rights","IsFollowing":false,"IsFollowers":true},{"UserId":26,"UserName":"Shal Gram","UserImagePath":"/Files/User/user_20180625_050108643_ProfileImage.png","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Shaligram ","IsFollowing":false,"IsFollowers":true},{"UserId":14,"UserName":null,"UserImagePath":null,"UserRoleType":null,"BusinessName":null,"WorkingAt":null,"IsFollowing":false,"IsFollowers":true},{"UserId":68,"UserName":null,"UserImagePath":null,"UserRoleType":null,"BusinessName":null,"WorkingAt":null,"IsFollowing":false,"IsFollowers":true},{"UserId":1,"UserName":null,"UserImagePath":null,"UserRoleType":null,"BusinessName":null,"WorkingAt":null,"IsFollowing":false,"IsFollowers":false},{"UserId":32,"UserName":"Pooja Dixit","UserImagePath":"/Files/User/user_20180626_043617714_Desert.jpg","UserRoleType":"Individual","BusinessName":"","WorkingAt":"abc","IsFollowing":false,"IsFollowers":true},{"UserId":66,"UserName":"Tejas Padia","UserImagePath":"/Files/User/user_20180705_064531279_image.png","UserRoleType":"Business","BusinessName":"abc","WorkingAt":"","IsFollowing":false,"IsFollowers":true},{"UserId":76,"UserName":"Tushar Amkar","UserImagePath":"/Files/User/user_20180719_121437711_ProfileImage.png","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Shaligram","IsFollowing":false,"IsFollowers":true},{"UserId":87,"UserName":"Tester New","UserImagePath":"/Files/User/user_20180720_033222616_cropped1501587294.jpg","UserRoleType":"Business","BusinessName":"Test","WorkingAt":"","IsFollowing":false,"IsFollowers":true},{"UserId":77,"UserName":"Neel Purohit","UserImagePath":"/Files/User/user_20180719_122203274_ProfileImage.png","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Shaligram","IsFollowing":false,"IsFollowers":true},{"UserId":86,"UserName":"Sushi Resturant","UserImagePath":"/Files/User/user_20180720_022808953_cropped1001079931.jpg","UserRoleType":"Business","BusinessName":"Aqua Spirit","WorkingAt":"","IsFollowing":false,"IsFollowers":true},{"UserId":70,"UserName":"Pooja Dixit","UserImagePath":"/Files/User/user_20180706_032542310_image.png","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Microsoft","IsFollowing":false,"IsFollowers":true},{"UserId":150,"UserName":"Satva Tech","UserImagePath":"/Files/User/user_20181101_085543147_cropped7877240212204437045.jpg","UserRoleType":"Business","BusinessName":"Satva Softech","WorkingAt":"","IsFollowing":false,"IsFollowers":false}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * UserId : 12
         * UserName : hiren dixit
         * UserImagePath : /Files/User/user_20180625_120336140_cropped1603633929.jpg
         * UserRoleType : Individual
         * BusinessName :
         * WorkingAt : Shaligram
         * IsFollowing : false
         * IsFollowers : true
         */

        @SerializedName("UserId")
        private int UserId;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImagePath")
        private String UserImagePath;
        @SerializedName("UserRoleType")
        private String UserRoleType;
        @SerializedName("BusinessName")
        private String BusinessName;
        @SerializedName("WorkingAt")
        private String WorkingAt;
        @SerializedName("IsFollowing")
        private boolean IsFollowing;
        @SerializedName("IsFollowers")
        private boolean IsFollowers;

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getUserRoleType() {
            return UserRoleType;
        }

        public void setUserRoleType(String UserRoleType) {
            this.UserRoleType = UserRoleType;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(String WorkingAt) {
            this.WorkingAt = WorkingAt;
        }

        public boolean isIsFollowing() {
            return IsFollowing;
        }

        public void setIsFollowing(boolean IsFollowing) {
            this.IsFollowing = IsFollowing;
        }

        public boolean isIsFollowers() {
            return IsFollowers;
        }

        public void setIsFollowers(boolean IsFollowers) {
            this.IsFollowers = IsFollowers;
        }
    }
}
