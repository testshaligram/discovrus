package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class ConfirmMailData extends Observable {


    /**
     * Data : {"Email":"qu@gmail.com","Code":null,"Message":null}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * Email : qu@gmail.com
         * Code : null
         * Message : null
         */

        private String Email;
        private Object Code;
        private Object Message;

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public Object getCode() {
            return Code;
        }

        public void setCode(Object Code) {
            this.Code = Code;
        }

        public Object getMessage() {
            return Message;
        }

        public void setMessage(Object Message) {
            this.Message = Message;
        }
    }
}
