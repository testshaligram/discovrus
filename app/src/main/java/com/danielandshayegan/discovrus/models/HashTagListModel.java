package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class HashTagListModel extends Observable {


    /**
     * Data : [{"ID":1,"Tag":"#dgg vdfsdfsd","Message":null},{"ID":7,"Tag":"#fsvbvshh sdfhvbh","Message":null}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * ID : 1
         * Tag : #dgg vdfsdfsd
         * Message : null
         */

        private int ID;
        private String Tag;
        private Object Message;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getTag() {
            return Tag;
        }

        public void setTag(String Tag) {
            this.Tag = Tag;
        }

        public Object getMessage() {
            return Message;
        }

        public void setMessage(Object Message) {
            this.Message = Message;
        }
    }
}
