package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentReplyData {

    /**
     * Data : [{"CommentId":355,"PostId":1799,"UserId":76,"UserName":"Tushar Amkar","UserImagePath":"/Files/User/user_20180719_121437711_ProfileImage.png","Comment":"Very+nice+","LikeReplyCount":0,"IsCommentLike":false,"CommentReplyCount":0,"CanDelete":true,"CommentCount":null,"CommentDate":"1 hour "}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * CommentId : 355
         * PostId : 1799
         * UserId : 76
         * UserName : Tushar Amkar
         * UserImagePath : /Files/User/user_20180719_121437711_ProfileImage.png
         * Comment : Very+nice+
         * LikeReplyCount : 0
         * IsCommentLike : false
         * CommentReplyCount : 0
         * CanDelete : true
         * CommentCount : null
         * CommentDate : 1 hour
         */

        @SerializedName("CommentId")
        private int CommentId;
        @SerializedName("PostId")
        private int PostId;
        @SerializedName("UserId")
        private int UserId;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImagePath")
        private String UserImagePath;
        @SerializedName("Comment")
        private String Comment;
        @SerializedName("LikeReplyCount")
        private int LikeReplyCount;
        @SerializedName("IsCommentLike")
        private boolean IsCommentLike;
        @SerializedName("CommentReplyCount")
        private int CommentReplyCount;
        @SerializedName("CanDelete")
        private boolean CanDelete;
        @SerializedName("CommentCount")
        private Object CommentCount;
        @SerializedName("CommentDate")
        private String CommentDate;

        public int getCommentId() {
            return CommentId;
        }

        public void setCommentId(int CommentId) {
            this.CommentId = CommentId;
        }

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String Comment) {
            this.Comment = Comment;
        }

        public int getLikeReplyCount() {
            return LikeReplyCount;
        }

        public void setLikeReplyCount(int LikeReplyCount) {
            this.LikeReplyCount = LikeReplyCount;
        }

        public boolean isIsCommentLike() {
            return IsCommentLike;
        }

        public void setIsCommentLike(boolean IsCommentLike) {
            this.IsCommentLike = IsCommentLike;
        }

        public int getCommentReplyCount() {
            return CommentReplyCount;
        }

        public void setCommentReplyCount(int CommentReplyCount) {
            this.CommentReplyCount = CommentReplyCount;
        }

        public boolean isCanDelete() {
            return CanDelete;
        }

        public void setCanDelete(boolean CanDelete) {
            this.CanDelete = CanDelete;
        }

        public Object getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(Object CommentCount) {
            this.CommentCount = CommentCount;
        }

        public String getCommentDate() {
            return CommentDate;
        }

        public void setCommentDate(String CommentDate) {
            this.CommentDate = CommentDate;
        }
    }
}
