package com.danielandshayegan.discovrus.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class PostListData {

    private List<PostList> Data;
    private boolean Success;
    private List<String> Message;

    public List<PostList> getData() {
        return Data;
    }

    public void setData(List<PostList> data) {
        Data = data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> message) {
        Message = message;
    }

    public static class PostList implements Serializable {
        private int UserID, LoginID;
        private String UserName, Email, UserImagePath, Location, UserType, BusinessName;
        private boolean Follow;
        private List<Post> post;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int userID) {
            UserID = userID;
        }

        public int getLoginID() {
            return LoginID;
        }

        public void setLoginID(int loginID) {
            LoginID = loginID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String userImagePath) {
            UserImagePath = userImagePath;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String location) {
            Location = location;
        }

        public boolean isFollow() {
            return Follow;
        }

        public void setFollow(boolean follow) {
            Follow = follow;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String userType) {
            UserType = userType;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String businessName) {
            BusinessName = businessName;
        }

        public List<Post> getPost() {
            return post;
        }

        public void setPost(List<Post> post) {
            this.post = post;
        }
    }

    public static class Post implements Parcelable {
        private int UserID,PostId, FileId, LikeCount, CommentCount, ViewCount;
        private String Description, Title, PostPath, Type, PostCreatedDate, ThumbFileName, month, OriginalImage, TagUserID, Lat, Long, Distance, Time, PostTimeAgo, UserType;
        private boolean Liked = false, Commented, IsHighLight, isDetailedPost, Viewed, IsPaid, IsSave, IsFollowing;
        private double PaidAmount;

        public Post() {

        }

        protected Post(Parcel in) {
            UserID = in.readInt();
            PostId = in.readInt();
            FileId = in.readInt();
            LikeCount = in.readInt();
            CommentCount = in.readInt();
            ViewCount = in.readInt();
            Description = in.readString();
            Title = in.readString();
            PostPath = in.readString();
            Type = in.readString();
            PostCreatedDate = in.readString();
            ThumbFileName = in.readString();
            month = in.readString();
            OriginalImage = in.readString();
            TagUserID = in.readString();
            Lat = in.readString();
            Long = in.readString();
            Distance = in.readString();
            Time = in.readString();
            PostTimeAgo = in.readString();
            UserType = in.readString();
            Liked = in.readByte() != 0;
            Commented = in.readByte() != 0;
            IsHighLight = in.readByte() != 0;
            isDetailedPost = in.readByte() != 0;
            Viewed = in.readByte() != 0;
            IsPaid = in.readByte() != 0;
            IsSave = in.readByte() != 0;
            IsFollowing = in.readByte() != 0;
            PaidAmount = in.readDouble();
        }

        public static final Creator<Post> CREATOR = new Creator<Post>() {
            @Override
            public Post createFromParcel(Parcel in) {
                return new Post(in);
            }

            @Override
            public Post[] newArray(int size) {
                return new Post[size];
            }
        };

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int userID) {
            UserID = userID;
        }

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int postId) {
            PostId = postId;
        }

        public int getFileId() {
            return FileId;
        }

        public void setFileId(int fileId) {
            FileId = fileId;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int likeCount) {
            LikeCount = likeCount;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int commentCount) {
            CommentCount = commentCount;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int viewCount) {
            ViewCount = viewCount;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getPostPath() {
            return PostPath;
        }

        public void setPostPath(String postPath) {
            PostPath = postPath;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getPostCreatedDate() {
            return PostCreatedDate;
        }

        public boolean isCommented() {
            return Commented;
        }

        public void setCommented(boolean commented) {
            Commented = commented;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public void setPostCreatedDate(String postCreatedDate) {
            PostCreatedDate = postCreatedDate;
        }

        public String getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(String thumbFileName) {
            ThumbFileName = thumbFileName;
        }

        public boolean isLiked() {
            return Liked;
        }

        public void setLiked(boolean liked) {
            Liked = liked;
        }

        public String getOriginalImage() {
            return OriginalImage;
        }

        public void setOriginalImage(String originalImage) {
            OriginalImage = originalImage;
        }

        public String getTagUserID() {
            return TagUserID;
        }

        public void setTagUserID(String tagUserID) {
            TagUserID = tagUserID;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLong() {
            return Long;
        }

        public void setLong(String aLong) {
            Long = aLong;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String distance) {
            Distance = distance;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getPostTimeAgo() {
            return PostTimeAgo;
        }

        public void setPostTimeAgo(String postTimeAgo) {
            PostTimeAgo = postTimeAgo;
        }

        public boolean isHighLight() {
            return IsHighLight;
        }

        public void setHighLight(boolean highLight) {
            IsHighLight = highLight;
        }

        public boolean isDetailedPost() {
            return isDetailedPost;
        }

        public void setDetailedPost(boolean detailedPost) {
            isDetailedPost = detailedPost;
        }

        public boolean isViewed() {
            return Viewed;
        }

        public void setViewed(boolean viewed) {
            Viewed = viewed;
        }

        public boolean isPaid() {
            return IsPaid;
        }

        public void setPaid(boolean paid) {
            IsPaid = paid;
        }

        public boolean isSave() {
            return IsSave;
        }

        public void setSave(boolean save) {
            IsSave = save;
        }

        public boolean isFollowing() {
            return IsFollowing;
        }

        public void setFollowing(boolean following) {
            IsFollowing = following;
        }

        public double getPaidAmount() {
            return PaidAmount;
        }

        public void setPaidAmount(double paidAmount) {
            PaidAmount = paidAmount;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String userType) {
            UserType = userType;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {

            parcel.writeInt(UserID);
            parcel.writeInt(PostId);
            parcel.writeInt(FileId);
            parcel.writeInt(LikeCount);
            parcel.writeInt(CommentCount);
            parcel.writeInt(ViewCount);
            parcel.writeString(Description);
            parcel.writeString(Title);
            parcel.writeString(PostPath);
            parcel.writeString(Type);
            parcel.writeString(PostCreatedDate);
            parcel.writeString(ThumbFileName);
            parcel.writeString(month);
            parcel.writeString(OriginalImage);
            parcel.writeString(TagUserID);
            parcel.writeString(Lat);
            parcel.writeString(Long);
            parcel.writeString(Distance);
            parcel.writeString(Time);
            parcel.writeString(PostTimeAgo);
            parcel.writeString(UserType);
            parcel.writeByte((byte) (Liked ? 1 : 0));
            parcel.writeByte((byte) (Commented ? 1 : 0));
            parcel.writeByte((byte) (IsHighLight ? 1 : 0));
            parcel.writeByte((byte) (isDetailedPost ? 1 : 0));
            parcel.writeByte((byte) (Viewed ? 1 : 0));
            parcel.writeByte((byte) (IsPaid ? 1 : 0));
            parcel.writeByte((byte) (IsSave ? 1 : 0));
            parcel.writeByte((byte) (IsFollowing ? 1 : 0));
            parcel.writeDouble(PaidAmount);
        }


    }

}