package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavePostCommentReplyData {

    /**
     * Data : {"CommentId":341,"PostId":1626,"UserId":32,"Comment":"nice one","CreatedBy":32,"CommentCount":5}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    @SerializedName("Data")
    private DataBean Data;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Message")
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * CommentId : 341
         * PostId : 1626
         * UserId : 32
         * Comment : nice one
         * CreatedBy : 32
         * CommentCount : 5
         */

        @SerializedName("CommentId")
        private int CommentId;
        @SerializedName("PostId")
        private int PostId;
        @SerializedName("UserId")
        private int UserId;
        @SerializedName("Comment")
        private String Comment;
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @SerializedName("CommentCount")
        private int CommentCount;

        public int getCommentId() {
            return CommentId;
        }

        public void setCommentId(int CommentId) {
            this.CommentId = CommentId;
        }

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String Comment) {
            this.Comment = Comment;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }
    }
}
