package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentsData {
    @SerializedName("ImagePath")
    @Expose
    private String ImagePath;

    @SerializedName("Type")
    @Expose
    private String Type;

    @SerializedName("Title")
    @Expose
    private String Title;

    @SerializedName("Description")
    @Expose
    private String Description;

    @SerializedName("DateSponsered")
    @Expose
    private String DateSponsered;

    @SerializedName("TimeSponsered")
    @Expose
    private String TimeSponsered;

    @SerializedName("Duration")
    @Expose
    private String Duration;

    @SerializedName("Distance")
    @Expose
    private String Distance;

    @SerializedName("TotalPaid")
    @Expose
    private String TotalPaid;

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDateSponsered() {
        return DateSponsered;
    }

    public void setDateSponsered(String dateSponsered) {
        DateSponsered = dateSponsered;
    }

    public String getTimeSponsered() {
        return TimeSponsered;
    }

    public void setTimeSponsered(String timeSponsered) {
        TimeSponsered = timeSponsered;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getTotalPaid() {
        return TotalPaid;
    }

    public void setTotalPaid(String totalPaid) {
        TotalPaid = totalPaid;
    }
}