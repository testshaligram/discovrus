package com.danielandshayegan.discovrus.models;

import java.util.List;

public class MapPostDetailByLatLong {

    /**
     * Data : [{"PostId":227,"UserID":2,"Lat":51.513513,"Long":-0.1363768,"UserName":"Tushar Amkar","UserImagePath":"/Files/User/user_20181128_033846585_ProfileImage.png","PostPath":"/Files/NewsFeed/Photo/bb5b2117-e154-4772-be91-fb738f6a1708.png","ThumbFileName":"","Type":"Photo","UserType":"Individual","BusinessName":"","Location":"Ahmedabad, Ahmadabad, Gujarat, India, 380058","Title":"","Description":"","CategoryId":0,"CategoryName":"","Distance":0.135,"PostCreatedDate":null,"PostTimeAgo":null,"CommentCount":0,"ViewCount":6,"LikeCount":1,"Commented":false,"Liked":true,"PostCountInWeek":0},{"PostId":228,"UserID":15,"Lat":51.513513,"Long":-0.1363768,"UserName":"Parth Patel","UserImagePath":"/Files/User/user_20181128_065124819_ProfileImage.png","PostPath":"/Files/NewsFeed/Photo/abcb4643-4fe0-47f0-8420-012ce79f040e.png","ThumbFileName":"","Type":"Photo","UserType":"Individual","BusinessName":"","Location":"Ahmedabad Area, India","Title":"","Description":"","CategoryId":0,"CategoryName":"","Distance":0.135,"PostCreatedDate":null,"PostTimeAgo":null,"CommentCount":0,"ViewCount":6,"LikeCount":1,"Commented":false,"Liked":false,"PostCountInWeek":0},{"PostId":223,"UserID":40,"Lat":51.515884037674,"Long":-0.138383242651,"UserName":"Massimo Shayegan","UserImagePath":"/Files/User/user_20181210_080723733_ProfileImage.png","PostPath":"/Files/NewsFeed/Photo/40465dfb-b60c-4e86-bf38-733961f7d8e5.png","ThumbFileName":"","Type":"Photo","UserType":"Individual","BusinessName":"","Location":"London, England, United Kingdom, W1F 0DS","Title":"","Description":"","CategoryId":0,"CategoryName":"","Distance":0.18,"PostCreatedDate":null,"PostTimeAgo":null,"CommentCount":1,"ViewCount":13,"LikeCount":2,"Commented":false,"Liked":false,"PostCountInWeek":0}]
     * Success : true
     * Message : ["Success"]
     * Count :
     * TotalRecord :
     */

    private boolean Success;
    private String Count;
    private String TotalRecord;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public String getTotalRecord() {
        return TotalRecord;
    }

    public void setTotalRecord(String TotalRecord) {
        this.TotalRecord = TotalRecord;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostId : 227
         * UserID : 2
         * Lat : 51.513513
         * Long : -0.1363768
         * UserName : Tushar Amkar
         * UserImagePath : /Files/User/user_20181128_033846585_ProfileImage.png
         * PostPath : /Files/NewsFeed/Photo/bb5b2117-e154-4772-be91-fb738f6a1708.png
         * ThumbFileName :
         * Type : Photo
         * UserType : Individual
         * BusinessName :
         * Location : Ahmedabad, Ahmadabad, Gujarat, India, 380058
         * Title :
         * Description :
         * CategoryId : 0
         * CategoryName :
         * Distance : 0.135
         * PostCreatedDate : null
         * PostTimeAgo : null
         * CommentCount : 0
         * ViewCount : 6
         * LikeCount : 1
         * Commented : false
         * Liked : true
         * PostCountInWeek : 0
         */

        private int PostId;
        private int UserID;
        private double Lat;
        private double Long;
        private String UserName;
        private String UserImagePath;
        private String PostPath;
        private String ThumbFileName;
        private String Type;
        private String UserType;
        private String BusinessName;
        private String Location;
        private String Title;
        private String Description;
        private int CategoryId;
        private String CategoryName;
        private double Distance;
        private String PostCreatedDate;
        private String PostTimeAgo;
        private int CommentCount;
        private int ViewCount;
        private int LikeCount;
        private boolean Commented;
        private boolean Liked;
        private int PostCountInWeek;

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public double getLat() {
            return Lat;
        }

        public void setLat(double Lat) {
            this.Lat = Lat;
        }

        public double getLong() {
            return Long;
        }

        public void setLong(double Long) {
            this.Long = Long;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getPostPath() {
            return PostPath;
        }

        public void setPostPath(String PostPath) {
            this.PostPath = PostPath;
        }

        public String getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(String ThumbFileName) {
            this.ThumbFileName = ThumbFileName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String CategoryName) {
            this.CategoryName = CategoryName;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getPostCreatedDate() {
            return PostCreatedDate;
        }

        public void setPostCreatedDate(String PostCreatedDate) {
            this.PostCreatedDate = PostCreatedDate;
        }

        public String getPostTimeAgo() {
            return PostTimeAgo;
        }

        public void setPostTimeAgo(String PostTimeAgo) {
            this.PostTimeAgo = PostTimeAgo;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }

        public boolean isCommented() {
            return Commented;
        }

        public void setCommented(boolean Commented) {
            this.Commented = Commented;
        }

        public boolean isLiked() {
            return Liked;
        }

        public void setLiked(boolean Liked) {
            this.Liked = Liked;
        }

        public int getPostCountInWeek() {
            return PostCountInWeek;
        }

        public void setPostCountInWeek(int PostCountInWeek) {
            this.PostCountInWeek = PostCountInWeek;
        }
    }
}
