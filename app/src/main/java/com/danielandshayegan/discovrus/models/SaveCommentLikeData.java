package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class SaveCommentLikeData extends Observable {


    /**
     * Data : {"ID":55,"CommentID":193,"UserID":12,"PostID":1622,"PostUserID":14,"isLiked":true}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * ID : 55
         * CommentID : 193
         * UserID : 12
         * PostID : 1622
         * PostUserID : 14
         * isLiked : true
         */

        private int ID;
        private int CommentID;
        private int UserID;
        private int PostID;
        private int PostUserID;
        private boolean isLiked;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getCommentID() {
            return CommentID;
        }

        public void setCommentID(int CommentID) {
            this.CommentID = CommentID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getPostID() {
            return PostID;
        }

        public void setPostID(int PostID) {
            this.PostID = PostID;
        }

        public int getPostUserID() {
            return PostUserID;
        }

        public void setPostUserID(int PostUserID) {
            this.PostUserID = PostUserID;
        }

        public boolean isIsLiked() {
            return isLiked;
        }

        public void setIsLiked(boolean isLiked) {
            this.isLiked = isLiked;
        }
    }
}
