package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChartDataList {
    /**
     * Data : [{"DayDate":"10/01/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/02/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/03/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/04/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/05/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/06/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/07/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/08/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/09/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/10/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/11/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/12/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/13/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/14/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/15/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/16/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/17/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/18/2018","ViewCount":2,"CommentCount":0,"LikeCount":0},{"DayDate":"10/19/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/20/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/21/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/22/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/23/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/24/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/25/2018","ViewCount":1,"CommentCount":0,"LikeCount":0},{"DayDate":"10/26/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/27/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/28/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/29/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/30/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"10/31/2018","ViewCount":1,"CommentCount":0,"LikeCount":0},{"DayDate":"11/01/2018","ViewCount":5,"CommentCount":4,"LikeCount":4},{"DayDate":"11/02/2018","ViewCount":23,"CommentCount":0,"LikeCount":0},{"DayDate":"11/03/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"11/04/2018","ViewCount":3,"CommentCount":0,"LikeCount":0},{"DayDate":"11/05/2018","ViewCount":4,"CommentCount":0,"LikeCount":0},{"DayDate":"11/06/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"11/07/2018","ViewCount":12,"CommentCount":0,"LikeCount":0},{"DayDate":"11/08/2018","ViewCount":1,"CommentCount":0,"LikeCount":0},{"DayDate":"11/09/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"11/10/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"11/11/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"11/12/2018","ViewCount":1,"CommentCount":0,"LikeCount":0},{"DayDate":"11/13/2018","ViewCount":2,"CommentCount":0,"LikeCount":0},{"DayDate":"11/14/2018","ViewCount":1,"CommentCount":0,"LikeCount":0},{"DayDate":"11/15/2018","ViewCount":10,"CommentCount":1,"LikeCount":2},{"DayDate":"11/16/2018","ViewCount":35,"CommentCount":1,"LikeCount":0},{"DayDate":"11/17/2018","ViewCount":12,"CommentCount":0,"LikeCount":0},{"DayDate":"11/18/2018","ViewCount":0,"CommentCount":0,"LikeCount":0},{"DayDate":"11/19/2018","ViewCount":3,"CommentCount":0,"LikeCount":0}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * DayDate : 10/01/2018
         * ViewCount : 0
         * CommentCount : 0
         * LikeCount : 0
         */

        @SerializedName("DayDate")
        private String DayDate;
        @SerializedName("ViewCount")
        private int ViewCount;
        @SerializedName("CommentCount")
        private int CommentCount;
        @SerializedName("LikeCount")
        private int LikeCount;

        public String getDate() {
            return DayDate;
        }

        public void setDate(String DayDate) {
            this.DayDate = DayDate;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }
    }
    /*
    *//**
     * Data : [{"UserID":0,"Date":"06/01/2018","ViewCount":0},{"UserID":0,"Date":"06/02/2018","ViewCount":0},{"UserID":0,"Date":"06/03/2018","ViewCount":0},{"UserID":0,"Date":"06/04/2018","ViewCount":0},{"UserID":0,"Date":"06/05/2018","ViewCount":0},{"UserID":0,"Date":"06/06/2018","ViewCount":0},{"UserID":0,"Date":"06/07/2018","ViewCount":0},{"UserID":0,"Date":"06/08/2018","ViewCount":0},{"UserID":0,"Date":"06/09/2018","ViewCount":0},{"UserID":0,"Date":"06/10/2018","ViewCount":0},{"UserID":0,"Date":"06/11/2018","ViewCount":0},{"UserID":0,"Date":"06/12/2018","ViewCount":0},{"UserID":0,"Date":"06/13/2018","ViewCount":0},{"UserID":0,"Date":"06/14/2018","ViewCount":0},{"UserID":0,"Date":"06/15/2018","ViewCount":0},{"UserID":0,"Date":"06/16/2018","ViewCount":0},{"UserID":0,"Date":"06/17/2018","ViewCount":0},{"UserID":0,"Date":"06/18/2018","ViewCount":0},{"UserID":0,"Date":"06/19/2018","ViewCount":0},{"UserID":0,"Date":"06/20/2018","ViewCount":0},{"UserID":0,"Date":"06/21/2018","ViewCount":0},{"UserID":0,"Date":"06/22/2018","ViewCount":0},{"UserID":0,"Date":"06/23/2018","ViewCount":0},{"UserID":0,"Date":"06/24/2018","ViewCount":0},{"UserID":0,"Date":"06/25/2018","ViewCount":0},{"UserID":0,"Date":"06/26/2018","ViewCount":0},{"UserID":32,"Date":"06/27/2018","ViewCount":22},{"UserID":32,"Date":"06/28/2018","ViewCount":10},{"UserID":32,"Date":"06/29/2018","ViewCount":3},{"UserID":0,"Date":"06/30/2018","ViewCount":0},{"UserID":0,"Date":"07/01/2018","ViewCount":0},{"UserID":32,"Date":"07/02/2018","ViewCount":71},{"UserID":0,"Date":"07/03/2018","ViewCount":0},{"UserID":0,"Date":"07/04/2018","ViewCount":0},{"UserID":32,"Date":"07/05/2018","ViewCount":4},{"UserID":32,"Date":"07/06/2018","ViewCount":51},{"UserID":0,"Date":"07/07/2018","ViewCount":0},{"UserID":0,"Date":"07/08/2018","ViewCount":0},{"UserID":0,"Date":"07/09/2018","ViewCount":0},{"UserID":32,"Date":"07/10/2018","ViewCount":4},{"UserID":0,"Date":"07/11/2018","ViewCount":0},{"UserID":0,"Date":"07/12/2018","ViewCount":0},{"UserID":0,"Date":"07/13/2018","ViewCount":0},{"UserID":0,"Date":"07/14/2018","ViewCount":0},{"UserID":0,"Date":"07/15/2018","ViewCount":0},{"UserID":32,"Date":"07/16/2018","ViewCount":75},{"UserID":32,"Date":"07/17/2018","ViewCount":10},{"UserID":0,"Date":"07/18/2018","ViewCount":0},{"UserID":0,"Date":"07/19/2018","ViewCount":0},{"UserID":0,"Date":"07/20/2018","ViewCount":0},{"UserID":0,"Date":"07/21/2018","ViewCount":0},{"UserID":0,"Date":"07/22/2018","ViewCount":0},{"UserID":0,"Date":"07/23/2018","ViewCount":0},{"UserID":0,"Date":"07/24/2018","ViewCount":0},{"UserID":0,"Date":"07/25/2018","ViewCount":0},{"UserID":0,"Date":"07/26/2018","ViewCount":0},{"UserID":0,"Date":"07/27/2018","ViewCount":0},{"UserID":0,"Date":"07/28/2018","ViewCount":0},{"UserID":0,"Date":"07/29/2018","ViewCount":0},{"UserID":0,"Date":"07/30/2018","ViewCount":0},{"UserID":0,"Date":"07/31/2018","ViewCount":0}]
     * Success : true
     * Message : ["Success"]
     * Count :
     *//*

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        *//**
         * Date : 06/01/2018
         * ViewCount : 0
         *//*

        private String Date;
        private int ViewCount;

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }
    }*/
}
