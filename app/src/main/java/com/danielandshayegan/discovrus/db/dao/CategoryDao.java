package com.danielandshayegan.discovrus.db.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;

import java.util.List;

@Dao
public interface CategoryDao {

    @Query("SELECT * FROM category_master")
    List<CategoriesTableModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CategoriesTableModel Category);

    @Query("DELETE FROM category_master")
    void deleteAll();
   /* @Query("SELECT * FROM category_master where CategoryId LIKE  :categoryId")
    LiveData<CategoriesTableModel> getCategoryDetailById(int categoryId);

    @Query("SELECT * FROM category_master where CategoryName LIKE  :categoryName")
    CategoriesTableModel getCategoryDetailByName(String categoryName);

    @Query("DELETE FROM category_master")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CategoriesTableModel Category);*/
}
