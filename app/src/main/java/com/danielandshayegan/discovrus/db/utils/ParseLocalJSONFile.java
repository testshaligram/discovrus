package com.danielandshayegan.discovrus.db.utils;

import android.content.Context;

import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ParseLocalJSONFile {

    private String loadJSONFromAsset(Context mContext) {
        String json;
        try {
            InputStream is = mContext.getAssets().open("days_data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public ArrayList<CategoriesTableModel> getCategoryList(Context mContext) {

        ArrayList<CategoriesTableModel> categoryList = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(mContext));
            JSONArray categoryArray = obj.getJSONArray("category_list");

            Gson gson = new Gson();
            categoryList = gson.fromJson(categoryArray.toString(),
                    new TypeToken<ArrayList<CategoriesTableModel>>() {
                    }.getType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categoryList;
    }
}
