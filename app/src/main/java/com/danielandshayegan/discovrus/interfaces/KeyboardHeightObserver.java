package com.danielandshayegan.discovrus.interfaces;

public interface KeyboardHeightObserver {

    void onKeyboardHeightChanged(int height, int orientation);
}
