package com.danielandshayegan.discovrus.custome_veiws.blur;


// Created by Urvi Joshi
// Guide : The Mr. Hiren Dixit

public class BlurKitException extends Exception {

    public BlurKitException(String message) {
        super(message);
    }
}
