package com.danielandshayegan.discovrus.custome_veiws.videotrimming;

import android.graphics.Bitmap;

import com.zomato.photofilters.imageprocessors.Filter;

public class VideoFilterNameModel {

    public String filterName;

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }
}
