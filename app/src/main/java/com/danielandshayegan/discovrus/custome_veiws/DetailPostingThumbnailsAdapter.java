package com.danielandshayegan.discovrus.custome_veiws;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DetailPostingThumbnailListItemBinding;
import com.danielandshayegan.discovrus.databinding.ThumbnailListItemBinding;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.utils.ThumbnailItem;

import java.util.List;

public class DetailPostingThumbnailsAdapter extends RecyclerView.Adapter<DetailPostingThumbnailsAdapter.MyViewHolder> {

    private List<ThumbnailItem> thumbnailItemList;
    private ThumbnailsAdapterListener listener;
    private Context mContext;
    private int selectedIndex = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        DetailPostingThumbnailListItemBinding mBinder;

        public MyViewHolder(DetailPostingThumbnailListItemBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }


    public DetailPostingThumbnailsAdapter(Context context, List<ThumbnailItem> thumbnailItemList, ThumbnailsAdapterListener listener) {
        mContext = context;
        this.thumbnailItemList = thumbnailItemList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DetailPostingThumbnailListItemBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.detail_posting_thumbnail_list_item, parent, false);
        return new MyViewHolder(mBinder);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ThumbnailItem thumbnailItem = thumbnailItemList.get(position);

        ProgressDialog mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Applying filter...");

        holder.mBinder.thumbnail.setImageBitmap(thumbnailItem.image);

        holder.mBinder.filterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mProgressDialog.show();
                listener.onFilterSelected(thumbnailItem.filter);
                selectedIndex = position;
                notifyDataSetChanged();
//                mProgressDialog.dismiss();
            }
        });

        holder.mBinder.filterName.setText(thumbnailItem.filterName);

        if (selectedIndex == position) {
            holder.mBinder.filterName.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            holder.mBinder.filterName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_rect_round_corners_bck));
        } else {
            holder.mBinder.filterName.setTextColor(ContextCompat.getColor(mContext, R.color.post_text_color));
            holder.mBinder.filterName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_detail_posting_bg));
        }
    }

    @Override
    public int getItemCount() {
        return thumbnailItemList.size();
    }

    public interface ThumbnailsAdapterListener {
        void onFilterSelected(Filter filter);
    }
}
