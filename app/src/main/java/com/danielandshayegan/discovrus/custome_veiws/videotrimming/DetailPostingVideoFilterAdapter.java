package com.danielandshayegan.discovrus.custome_veiws.videotrimming;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DetailPostingThumbnailListItemBinding;
import com.danielandshayegan.discovrus.databinding.ThumbnailListItemBinding;

import java.util.List;

public class DetailPostingVideoFilterAdapter extends RecyclerView.Adapter<DetailPostingVideoFilterAdapter.MyViewHolder> {


    private List<VideoFilterNameModel> videoFilterNameModels;
    private VideoFilterListener listener;
    private Context mContext;
    private int selectedIndex = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        DetailPostingThumbnailListItemBinding mBinder;

        public MyViewHolder(DetailPostingThumbnailListItemBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }

    public DetailPostingVideoFilterAdapter(List<VideoFilterNameModel> videoFilterNameModels, VideoFilterListener listener, Context mContext) {
        this.videoFilterNameModels = videoFilterNameModels;
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DetailPostingThumbnailListItemBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.detail_posting_thumbnail_list_item, parent, false);
        return new MyViewHolder(mBinder);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final VideoFilterNameModel videoFilterNameModel = videoFilterNameModels.get(position);

        //     holder.mBinder.thumbnail.setImageBitmap(thumbnailItem.image);

        holder.mBinder.filterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onFilterSelected(videoFilterNameModel);
                selectedIndex = position;
                notifyDataSetChanged();
            }
        });

        holder.mBinder.filterName.setText(videoFilterNameModel.filterName);

        if (selectedIndex == position) {
            holder.mBinder.filterName.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            holder.mBinder.filterName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_rect_round_corners_bck));
        } else {
            holder.mBinder.filterName.setTextColor(ContextCompat.getColor(mContext, R.color.post_text_color));
            holder.mBinder.filterName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_detail_posting_bg));
        }

    }

    @Override
    public int getItemCount() {
        return videoFilterNameModels.size();
    }

    public interface VideoFilterListener {
        void onFilterSelected(VideoFilterNameModel videoFilterNameModel);
    }
}
