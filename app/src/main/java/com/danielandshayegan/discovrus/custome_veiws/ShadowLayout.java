package com.danielandshayegan.discovrus.custome_veiws;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.danielandshayegan.discovrus.R;


/**
 * Created by Hiren.D on 7/6/18.
 */

public class ShadowLayout extends LinearLayout {
    public ShadowLayout(Context context) {
        super(context);
        initBackground();
    }

    public ShadowLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initBackground();
    }

    public ShadowLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initBackground();
    }

    private void initBackground() {
        setBackground(ViewUtils.generateBackgroundWithShadow(this, R.color.white_with_alpha_bg,
                R.dimen.card_corner_radius, R.color.white_with_alpha_bg, R.dimen.card_elevation, Gravity.BOTTOM));
    }
}
