package com.danielandshayegan.discovrus.custome_veiws.circlelist;

public enum CircularListViewContentAlignment {
    None,
    Left,
    Right
}
