package com.danielandshayegan.discovrus.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentMainBinding;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends BaseFragment {

    FragmentMainBinding mBinding;
    View view;


    public MainFragment() {
        // Required empty public constructor
    }

    static MainFragment newInstance() {
        return new MainFragment();
    }

    public static MainFragment newInstance(Bundle extras) {
        MainFragment fragment = new MainFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_main, container, false);
        view = mBinding.getRoot();

        Intent intent = getActivity().getIntent();
        if (null != intent) {
            String email = intent.getExtras().getString("email");
            mBinding.uiMainFragment.txtEmail.setText(email);
        }

        mBinding.uiMainFragment.btnLogout.setOnClickListener(v -> {
            Intent intent1 = new Intent(getActivity(), MainActivity.class);
            intent1.putExtra("animation", "splash");
            getActivity().startActivity(intent1);
            getActivity().finish();
        });


        return view;

    }

    @Override
    public boolean onBackPressed() {

        return super.onBackPressed();

    }
}
