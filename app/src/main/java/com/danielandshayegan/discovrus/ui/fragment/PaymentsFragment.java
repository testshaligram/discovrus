package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentPaymentsBinding;
import com.danielandshayegan.discovrus.databinding.RowPaymentsBinding;
import com.danielandshayegan.discovrus.models.PaymentsData;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.BaseBinder;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class PaymentsFragment extends BaseFragment {

    FragmentPaymentsBinding binding;
    private int currentPosition = -1, from = 0;
    Context me;
    private ArrayList<PaymentsData> paymentsList = new ArrayList<>();

    public PaymentsFragment(int from) {
        this.from = from;
    }

    @SuppressLint("ValidFragment")
    public PaymentsFragment(int position, ArrayList<PaymentsData> paymentsListWeek) {
        from = position;
        paymentsList = paymentsListWeek;
    }

    public static PaymentsFragment newInstance(int position) {
        PaymentsFragment fragment = new PaymentsFragment(position);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payments, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvPayments.setLayoutManager(layoutManager);

        if(paymentsList.size() > 0)
        {
            binding.tvNoRecordFound.setVisibility(View.GONE);
            binding.rvPayments.setVisibility(View.VISIBLE);
        }
        else
        {
            binding.tvNoRecordFound.setVisibility(View.VISIBLE);
            binding.rvPayments.setVisibility(View.GONE);
        }
        binding.rvPayments.setAdapter(new PaymentsAdapter());
        return binding.getRoot();
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
        }
    }

    class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.MyViewHolder> {
        private LayoutInflater layoutInflater;
        Animation slideDown, slideUp;
        String str = "";

        public PaymentsAdapter() {
            slideDown = AnimationUtils.loadAnimation(getActivity(), R.anim.expand);
            slideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.collapse);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private final RowPaymentsBinding binding;

            public MyViewHolder(final RowPaymentsBinding itemBinding) {
                super(itemBinding.getRoot());
                this.binding = itemBinding;
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (layoutInflater == null) {
                layoutInflater = LayoutInflater.from(parent.getContext());
            }
            RowPaymentsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_payments, parent, false);
            return new MyViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            try {
                PaymentsData paymentsData = paymentsList.get(position);

                switch (paymentsData.getType()) {
                    case "Photo":
                        str = getString(R.string.photo) + " " + getString(R.string.posted);
                        BaseBinder.setImageUrl(holder.binding.ivPostPayments, paymentsData.getImagePath());
                        break;

                    case "Video":
                        str = getString(R.string.video) + " " + getString(R.string.posted);
                        BaseBinder.setImageUrl(holder.binding.ivPostPayments, paymentsData.getImagePath());
                        break;

                    case "Text":
                        str = getString(R.string.text) + " " + getString(R.string.posted);
                        holder.binding.ivPostPayments.setImageResource(R.drawable.text_post);
                        break;

                    case "PhotoAndText":
                        str = getString(R.string.photo) + " + " + getString(R.string.text) + " " + getString(R.string.posted);
                        BaseBinder.setImageUrl(holder.binding.ivPostPayments, paymentsData.getImagePath());
                        break;
                }

                holder.binding.tvPostType.setText(str);
                holder.binding.setItem(paymentsData);
                holder.binding.executePendingBindings();

                if (currentPosition != position) {
                    holder.binding.ivRightArrow.setRotation(180);
                    holder.binding.clExpand.startAnimation(slideUp);
                    holder.binding.clExpand.setVisibility(View.GONE);
                } else {
                    if (holder.binding.clExpand.getVisibility() == View.VISIBLE) {
                        holder.binding.ivRightArrow.setRotation(180);
                        holder.binding.clExpand.startAnimation(slideUp);
                        holder.binding.clExpand.setVisibility(View.GONE);
                    } else {
                        holder.binding.clExpand.setVisibility(View.VISIBLE);
                        holder.binding.ivRightArrow.setRotation(0);
                        holder.binding.clExpand.startAnimation(slideDown);
                    }
                }

                holder.binding.clRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        currentPosition = position;
                        notifyDataSetChanged();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return paymentsList.size();
        }
    }
}