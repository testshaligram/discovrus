package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BaseAdapter;
import com.danielandshayegan.discovrus.adapter.ReportReasonListAdapter;
import com.danielandshayegan.discovrus.databinding.LayoutReportAProblemStep1Binding;
import com.danielandshayegan.discovrus.databinding.RowReportProblemReasonBinding;
import com.danielandshayegan.discovrus.models.ReasonData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.ui.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getReason;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

public class ReportAProblemStep1Activity extends BaseActivity {
    LayoutReportAProblemStep1Binding binding;
    List<ReasonData.DataBean> reasonList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_report_a_problem_step_1);

        callGetReason();
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    private void callGetReason() {
        binding.loading.progressBar.setVisibility(View.VISIBLE);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(ReportAProblemStep1Activity.this).
                                create(WebserviceBuilder.class).
                                getReason()
                        , getCompositeDisposable(), getReason, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                ReasonData reasonData = (ReasonData) o;
                                if (reasonData != null && reasonData.isSuccess()) {
                                    reasonList = reasonData.getData();
                                    binding.rvReasons.setLayoutManager(new LinearLayoutManager(ReportAProblemStep1Activity.this));
                                    binding.rvReasons.setHasFixedSize(true);
                                    binding.rvReasons.setAdapter(new ReportReasonListAdapter(ReportAProblemStep1Activity.this, reasonList, listener, "settings"));
                                    binding.rvReasons.getAdapter().notifyDataSetChanged();
                                    binding.loading.progressBar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");
                                binding.loading.progressBar.setVisibility(View.GONE);
                            }
                        });
    }

    private BaseAdapter.OnItemClickListener listener = (position, view) -> {
//        startActivity(new Intent(ReportAProblemStep1Activity.this, ReportAProblemStep2Activity.class).putExtra("reasonId", reasonList.get(position).getReasonID())); //todo: pass reason id from here
        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@discovrus.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Report For:- \n" + reasonList.get(position).getReportReason());

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    };

}