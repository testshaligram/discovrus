package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityCommentSettingsBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.IndividualProfileFragment;

public class CommentSettingsActivity extends BaseActivity {
    ActivityCommentSettingsBinding binding;
    IndividualProfileFragment profileFragment;
    Animation slideDown, slideUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_comment_settings);
        slideDown = AnimationUtils.loadAnimation(this, R.anim.expand);
        slideUp = AnimationUtils.loadAnimation(this, R.anim.collapse);
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.imageView3:
                finish();
                break;

            case R.id.cvAllowComments:
                if (binding.rgAllowComments.getVisibility() == View.VISIBLE) {
                    binding.ivRightArrow.setRotation(180);
                    binding.rgAllowComments.startAnimation(slideUp);
                    binding.rgAllowComments.setVisibility(View.GONE);
                } else {
                    binding.rgAllowComments.setVisibility(View.VISIBLE);
                    binding.ivRightArrow.setRotation(0);
                    binding.rgAllowComments.startAnimation(slideDown);
                }
                break;
        }
    }
}