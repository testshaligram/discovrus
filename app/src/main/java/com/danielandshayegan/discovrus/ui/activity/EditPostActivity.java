package com.danielandshayegan.discovrus.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityEditPostBinding;
import com.danielandshayegan.discovrus.dialog.LocationDialog;
import com.danielandshayegan.discovrus.dialog.LocationSelectDialog;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.utils.BaseBinder;
import com.danielandshayegan.discovrus.utils.FusedLocationProviderClass;
import com.danielandshayegan.discovrus.utils.MyLocationChangeListner;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.JSONCallback;
import com.danielandshayegan.discovrus.webservice.Retrofit;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

//todo: Send connect people list and set as chips and check all post data from all places are similar

public class EditPostActivity extends BaseActivity implements LocationSelectDialog.SelectLocationListner, MyLocationChangeListner, PlaceSelectionListener {
    ActivityEditPostBinding binding;
    PostListData.Post postData;
    Context me;
    private HashMap<String, ConnectPeopleData.DataBean> selectedPeopleList = new HashMap<>();
    FusedLocationProviderClass fusedLocationProviderClass;
    private FusedLocationProviderClient fusedLocationProviderClient;
    String TAG = LocationDialog.class.getName();
    double lat, lng;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int REQUEST_SELECT_PLACE = 1000;
    String taggedUserIds = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_post);
        me = this;

        fusedLocationProviderClient = new FusedLocationProviderClient(me);
        fusedLocationProviderClass = new FusedLocationProviderClass(me, fusedLocationProviderClient, this);

        postData = getIntent().getParcelableExtra("PostData");
        if (postData != null)
            setData();
    }

    @SuppressLint({"LongLogTag", "LogNotTimber", "SetTextI18n"})
    private void setData() {
        Log.e("postdata usertype", postData.getUserType());

        switch (postData.getType()) {
            case "Text":
                binding.etTitle.setText(postData.getTitle());
                binding.etDesc.setText(postData.getDescription());

                Utils.setSelectionEnd(binding.etTitle);
                Utils.setSelectionEnd(binding.etDesc);

                binding.ivPost.setVisibility(View.GONE);
                binding.ivPostPhoto.setVisibility(View.GONE);
                binding.ivPlay.setVisibility(View.GONE);
                binding.grpPostText.setVisibility(View.VISIBLE);
                break;

            case "Photo":
                binding.ivPostPhoto.setVisibility(View.VISIBLE);
                binding.ivPost.setVisibility(View.GONE);
                binding.grpPostText.setVisibility(View.GONE);
                binding.ivPlay.setVisibility(View.GONE);
                BaseBinder.setImageUrl(binding.ivPostPhoto, postData.getPostPath());
                break;

            case "PhotoAndText":
                binding.etTitle.setText(postData.getTitle());
                binding.etDesc.setText(postData.getDescription());

                Utils.setSelectionEnd(binding.etTitle);
                Utils.setSelectionEnd(binding.etDesc);

                binding.ivPost.setVisibility(View.VISIBLE);
                binding.ivPostPhoto.setVisibility(View.GONE);
                binding.ivPlay.setVisibility(View.GONE);
                binding.grpPostText.setVisibility(View.VISIBLE);
                BaseBinder.setImageUrl(binding.ivPost, postData.getPostPath());
                break;

            case "Video":
                binding.ivPlay.setVisibility(View.VISIBLE);
                binding.ivPost.setVisibility(View.GONE);
                binding.ivPostPhoto.setVisibility(View.VISIBLE);
                binding.grpPostText.setVisibility(View.GONE);
                BaseBinder.setImageUrl(binding.ivPostPhoto, postData.getThumbFileName());
                break;
        }

        if (null != postData.getTagUserID() && !postData.getTagUserID().isEmpty()) {
            Log.e("postData.getTagUserID() = ", "" + postData.getTagUserID());
            binding.tvConnectPeople.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_post_connect_people_selected, 0);
            taggedUserIds = postData.getTagUserID();
        }

        if (null != postData.getLat() && null != postData.getLong()) {
            try {
                Log.e("postData.getLat()) = ", postData.getLat() +  " & postData.getLong() = " + postData.getLong());
                String str = Utils.getAddressFromLatLong(me, Double.parseDouble(postData.getLat()), Double.parseDouble(postData.getLong()));
                if(str.isEmpty())
                {
                    binding.tvEditLocation.setText(getString(R.string.edit_location));
                }
                else {
                    binding.tvEditLocation.setText(getString(R.string.edit_location) + " (" + str +")");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (postData.getUserType().equalsIgnoreCase("Individual")) {
            binding.tvEditLocation.setVisibility(View.VISIBLE);
        } else {
            binding.tvEditLocation.setVisibility(View.GONE);
        }

    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;

            case R.id.ivHat:
                startActivity(new Intent(me, LaunchpadActivity.class));
                break;

            case R.id.tvEditLocation:
                if (App_pref.getAuthorizedUser(me).getData().getAdminUserRoleName().equalsIgnoreCase("Individual")) {
                    //DialogOpen
                    LocationSelectDialog locationSelectDialog = new LocationSelectDialog();
                    locationSelectDialog.setListener(this);
                    locationSelectDialog.show(((AppCompatActivity) me).getSupportFragmentManager(), LocationSelectDialog.class.getSimpleName());
                    locationSelectDialog.setCancelable(false);
                }
                break;

            case R.id.tvConnectPeople:
                if (selectedPeopleList.size() > 0)
                    startActivityForResult(new Intent(me, DetailPostConnectPeopleActivity.class).putExtra("peopleList", selectedPeopleList), 601);
                else
                    startActivityForResult(new Intent(me, DetailPostConnectPeopleActivity.class), 601);
                break;

            case R.id.btnDone:
                if (postData.getType().equalsIgnoreCase("Text") || postData.getType().equalsIgnoreCase("PhotoAndText")) {
                    if (isValid()) {
                        callEditPostDataApi();
                    }
                } else {
                    callEditPostDataApi();
                }
                break;
        }
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("onactivityResult", "done");
        if (requestCode == 601 && resultCode == RESULT_OK) {
            selectedPeopleList = (HashMap<String, ConnectPeopleData.DataBean>) data.getSerializableExtra("peopleList");
            if (selectedPeopleList.size() > 0) {
                binding.tvConnectPeople.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_post_connect_people_selected, 0);

                if (selectedPeopleList.size() > 0) {
                    Iterator myVeryOwnIterator = selectedPeopleList.keySet().iterator();
                    while (myVeryOwnIterator.hasNext()) {
                        String key = (String) myVeryOwnIterator.next();
                        ConnectPeopleData.DataBean value = selectedPeopleList.get(key);
                        if (taggedUserIds.equalsIgnoreCase(""))
                            taggedUserIds = "" + value.getUserId();
                        else
                            taggedUserIds = taggedUserIds + "," + value.getUserId();
                    }
                }
            }
        } else if (requestCode == REQUEST_SELECT_PLACE) {
            Place place = PlaceAutocomplete.getPlace(me, data);
            this.onPlaceSelected(place);
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(me, data);
            this.onError(status);
        }
    }

    @Override
    public void onMenualSelectLocationListner() {
        if (App_pref.getAuthorizedUser(me).getData().getAdminUserRoleName().equalsIgnoreCase("Individual")) {
            if (Utils.isNetworkAvailable(me)) {
                try {
                    AutocompleteFilter filter = new AutocompleteFilter.Builder()
                            .setTypeFilter(Place.TYPE_COUNTRY)
                            .build();
                    Intent intent = new PlaceAutocomplete.IntentBuilder
                            (PlaceAutocomplete.MODE_FULLSCREEN)
//                                .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .setFilter(filter)
                            .build(this);
                    startActivityForResult(intent, REQUEST_SELECT_PLACE);
                } catch (GooglePlayServicesRepairableException |
                        GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(me, R.string.check_your_network_connection, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCurrentSelectLocationListner() {
        buildLocationSettingsRequest();
    }

    protected void buildLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(me)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(me).checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    Log.e(TAG, "All location settings are satisfied.");
                    getLocation();

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                Log.e(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        ((Activity) me),
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                                Log.e(TAG, "PendingIntent unable to execute request.");
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                                Log.e(TAG, "PendingIntent unable to execute request.");

                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            Log.e(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            }
        });
    }

    public void getLocation() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(me,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();

            } else {
                checkLocationPermission();
            }
        } else {
            getCurrentLocation();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(me, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(((Activity) me),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                } else {
                    Toast.makeText(me, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void getCurrentLocation() {
        fusedLocationProviderClass.getLocation();
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void onLocationChange(Location location) {
        Log.e("on Place Selected", "Place Selected: " + "" + location.getLatitude() + "" + location.getLongitude());
        lat = location.getLatitude();
        lng = location.getLongitude();

        binding.tvEditLocation.setText(Utils.getAddressFromLatLong(me, lat, lng));
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void onPlaceSelected(Place place) {
        if(place != null) {
            Log.e("on Place Selected", "Place Selected: " + place.getAddress() + "" + place.getLatLng().latitude + "" + place.getLatLng().longitude);
            lat = place.getLatLng().latitude;
            lng = place.getLatLng().longitude;
            binding.tvEditLocation.setText(Utils.getAddressFromLatLong(me, lat, lng));
        }
    }

    @Override
    public void onError(Status status) {

    }

    private boolean isValid() {
        try {
            if (postData.getType().equalsIgnoreCase("Text") || postData.getType().equalsIgnoreCase("PhotoAndText")) {
                if (binding.etTitle.getText().toString().isEmpty()) {
                    Toast.makeText(me, R.string.please_enter_title, Toast.LENGTH_LONG).show();
                    binding.etTitle.requestFocus();
                    return false;
                } else if (binding.etDesc.getText().toString().isEmpty()) {
                    Toast.makeText(me, R.string.please_enter_desc, Toast.LENGTH_LONG).show();
                    binding.etTitle.requestFocus();
                    return false;
                } else {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @SuppressLint("LogNotTimber")
    private void callEditPostDataApi() {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", String.valueOf(App_pref.getAuthorizedUser(me).getData().getUserId()));
            params.put("PostID", String.valueOf(postData.getPostId()));
            params.put("Type", String.valueOf(postData.getType()));

            if (postData.getType().equalsIgnoreCase("Text") || postData.getType().equalsIgnoreCase("PhotoAndText")) {
                if (!postData.getTitle().equals(binding.etTitle.getText().toString()))
                    params.put("Title", binding.etTitle.getText().toString());

                if (!postData.getDescription().equals(binding.etDesc.getText().toString()))
                    params.put("Description", binding.etDesc.getText().toString());
            }

            if (lat != 0 && lng != 0) {
                params.put("Lat", String.valueOf(lat));
                params.put("Long", String.valueOf(lng));
            }

            if (!postData.getTagUserID().equals(taggedUserIds))
                params.put("TagUserID", taggedUserIds);

            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.EDIT_POST)
                        .setParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("webservice", statusCode + message);
                                Toast.makeText(me, message, Toast.LENGTH_LONG).show();
                            }

                            @SuppressLint({"LogNotTimber", "LongLogTag"})
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                Toast.makeText(me, jsonObject.optJSONArray("Message").optString(0), Toast.LENGTH_LONG).show();
                                finish();
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}