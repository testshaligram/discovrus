package com.danielandshayegan.discovrus.ui.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.dialog.LogoutDialog;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.ui.activity.SplashActivity;
import com.facebook.login.LoginManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    private Button btnSignOut = null;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    public static ProfileFragment newInstance(Bundle extras) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_profile, container, false);
        mappingWidgets(mView);
        return mView;
    }

    private void mappingWidgets(View mView) {
        btnSignOut = (Button) mView.findViewById(R.id.btn_logout);
        btnSignOut.setOnClickListener(view -> {
            /*if(App_pref.getAuthorizedUser(getActivity()) != null)
            {
                ApplicationClass.OnlineUserListAPI(getActivity(), false);
            }

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.apply();

            App_pref.signOut(getActivity());

            LoginManager.getInstance().logOut();
            Intent intent = new Intent(getActivity(), SplashActivity.class);
            startActivity(intent);
            getActivity().finish();*/
            LogoutDialog dialogFragment = new LogoutDialog();
            dialogFragment.show(getActivity().getFragmentManager(), "Dialog Logout");
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MenuActivity.fragmentView = 4;
    }
}
