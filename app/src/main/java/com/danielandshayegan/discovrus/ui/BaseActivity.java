package com.danielandshayegan.discovrus.ui;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.ui.dialog.ProgressDialog;
import com.danielandshayegan.discovrus.utils.SessionManager;

import io.reactivex.disposables.CompositeDisposable;


public class BaseActivity extends AppCompatActivity {
    protected boolean shouldPerformDispatchTouch = true;

    public Toolbar toolbar;
    TextView title;
    protected SessionManager session;
    private ProgressDialog dialog;
    private CompositeDisposable compositeDisposable;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  ApplicationClass.context = this;
        session = new SessionManager(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
       // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
       // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void setUpToolbar(String strTitle) {
        setUpToolbarWithBackArrow(strTitle, false);
    }

    public void setUpToolbarWithBackArrow(String strTitle) {
        setUpToolbarWithBackArrow(strTitle, true);
    }

    private void setUpToolbarWithBackArrow(String strTitle, boolean isBackArrow) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            if (isBackArrow) {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_vector_arrow_back);
            }
        }

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(strTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public ProgressDialog showProgressBar() {
        return showProgressBar(null);
    }

    public ProgressDialog showProgressBar(String message) {
        if (dialog == null) dialog = new ProgressDialog(this, message);
        return dialog;
    }

    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
    public void disableScreen(boolean disable) {
        if (disable) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        boolean ret = false;
        try {
            View view = getCurrentFocus();
            ret = super.dispatchTouchEvent(event);
            if (shouldPerformDispatchTouch) {
                if (view instanceof EditText) {
                    View w = getCurrentFocus();
                    int scrCords[] = new int[2];
                    if (w != null) {
                        w.getLocationOnScreen(scrCords);
                        float x = event.getRawX() + w.getLeft() - scrCords[0];
                        float y = event.getRawY() + w.getTop() - scrCords[1];

                        if (event.getAction() == MotionEvent.ACTION_UP
                                && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }
                    }
                }
            }
            return ret;
        } catch (Exception e) {
            return ret;
        }
    }

}
