package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.FollowersAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentFollowersBinding;
import com.danielandshayegan.discovrus.models.FollowPeopleData;
import com.danielandshayegan.discovrus.models.FollowersUserListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.followPeople;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getFollowList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FollowersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FollowersFragment extends BaseFragment implements SingleCallback, FollowersAdapter.FollowButtonClickListner {

    String listType;
    FragmentFollowersBinding mBinding;
    int userId;
    int loginId;

    public FollowersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FollowersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FollowersFragment newInstance(Bundle args) {
        FollowersFragment fragment = new FollowersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("LIST_TYPE")) {
            listType = getArguments().getString("LIST_TYPE");
            userId = getArguments().getInt("USER_ID");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_followers, container, false);
        mBinding.uiFollow.toolbar.imgBack.setImageResource(R.drawable.back_button);
        mBinding.uiFollow.toolbar.imgBack.setOnClickListener(view -> mActivity.finish());

        mBinding.uiFollow.toolbar.txtTitle.setText(listType.equalsIgnoreCase("Follower") ? "Followers" : "Following");
        getFollowListData();

        return mBinding.getRoot();
    }


    private void getFollowListData() {
        if (Utils.isNetworkAvailable(mContext)) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPI() {
        Boolean isFollowing;
        isFollowing = !listType.equals("Follower");
        loginId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getFollowList(userId, loginId, isFollowing)
                        , getCompositeDisposable(), getFollowList, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getFollowList:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                FollowersUserListData followersUserListData = (FollowersUserListData) o;
                if (followersUserListData.isSuccess()) {
                    if (followersUserListData.getData() != null && followersUserListData.getData().size() > 0) {
                        mBinding.uiFollow.recylerView.setVisibility(View.VISIBLE);
                        mBinding.uiFollow.txtNoData.setVisibility(View.GONE);
                        FollowersAdapter mAdapter;
                        if (listType.equalsIgnoreCase("Follower")) {
                            mAdapter = new FollowersAdapter(followersUserListData.getData(), getContext(), true, this);
                        } else {
                            mAdapter = new FollowersAdapter(followersUserListData.getData(), getContext(), false, this);
                        }
                        mBinding.uiFollow.recylerView.setAdapter(mAdapter);

                    } else {
                        mBinding.uiFollow.recylerView.setVisibility(View.GONE);
                        mBinding.uiFollow.txtNoData.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case followPeople:
                FollowPeopleData followPeopleData = (FollowPeopleData) o;
                if (followPeopleData.isSuccess()) {
                    mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    getFollowListData();
                } else {
                    Toast.makeText(getActivity(), followPeopleData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFollowButtonClick(FollowersUserListData.DataBean dataBean) {
        if (Utils.isNetworkAvailable(mContext)) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            followPeople(dataBean.getUserId());
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }

    public void followPeople(int followUserId) {
        if (listType.equals("Follower"))
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    followPeople(followUserId, loginId)
                            , getCompositeDisposable(), followPeople, this);
        else
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    followPeople(loginId, followUserId)
                            , getCompositeDisposable(), followPeople, this);

    }
}
