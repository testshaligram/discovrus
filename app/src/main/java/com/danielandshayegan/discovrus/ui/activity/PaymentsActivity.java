package com.danielandshayegan.discovrus.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityPaymentsBinding;
import com.danielandshayegan.discovrus.models.PaymentsData;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.PaymentsFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.JSONCallback;
import com.danielandshayegan.discovrus.webservice.Retrofit;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class PaymentsActivity extends BaseActivity {

    ActivityPaymentsBinding binding;
    Context me;
    PaymentsActivity.PaymentsListAdapter paymentsAdapter;
    private ArrayList<PaymentsData> paymentsListWeek = new ArrayList<>();
    private ArrayList<PaymentsData> paymentsListMonth = new ArrayList<>();
    private ArrayList<PaymentsData> paymentsListYear = new ArrayList<>();
    private int pageNumber = 0, pageSize = 100;

    //TODO: Put load more properly for three tabs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payments);
        me = this;

        callGetPaymentsList(0);

        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changeTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class PaymentsListAdapter extends FragmentStatePagerAdapter {
        PaymentsListAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new PaymentsFragment(position, paymentsListWeek);

                case 1:
                    return new PaymentsFragment(position, paymentsListMonth);

                case 2:
                    return new PaymentsFragment(position, paymentsListYear);

                default:
                    return new PaymentsFragment(position, paymentsListWeek);
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.imageView3:
                finish();
                break;

            case R.id.rlTabZero:
                changeTab(0);
                break;

            case R.id.rlTabOne:
                changeTab(1);
                break;

            case R.id.rlTabTwo:
                changeTab(2);
                break;
        }
    }

    private void changeTab(int position) {
        switch (position) {
            case 0:
                binding.indicatorZero.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
                binding.indicatorOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                binding.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                binding.tabZero.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.tabOne.setTextColor(getResources().getColor(R.color.post_title));
                binding.tabTwo.setTextColor(getResources().getColor(R.color.post_title));

                binding.viewpager.setCurrentItem(0);
                paymentsAdapter.notifyDataSetChanged();
                break;

            case 1:
                binding.indicatorZero.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                binding.indicatorOne.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
                binding.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                binding.tabZero.setTextColor(getResources().getColor(R.color.post_title));
                binding.tabOne.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.tabTwo.setTextColor(getResources().getColor(R.color.post_title));

                binding.viewpager.setCurrentItem(1);
                paymentsAdapter.notifyDataSetChanged();
                break;

            case 2:
                binding.indicatorZero.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                binding.indicatorOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                binding.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));

                binding.tabZero.setTextColor(getResources().getColor(R.color.post_title));
                binding.tabOne.setTextColor(getResources().getColor(R.color.post_title));
                binding.tabTwo.setTextColor(getResources().getColor(R.color.colorAccent));

                binding.viewpager.setCurrentItem(2);
                paymentsAdapter.notifyDataSetChanged();
                break;
        }
        callGetPaymentsList(position);
    }

    @SuppressLint("LogNotTimber")
    private void callGetPaymentsList(int from) {
        try {

            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", String.valueOf(App_pref.getAuthorizedUser(me).getData().getUserId()));
            params.put("PageNumber", String.valueOf(pageNumber));
            params.put("PageSize", String.valueOf(pageSize));
            params.put("IsWeek", from == 0 ? "1" : "0");
            params.put("IsMonth", from == 1 ? "1" : "0");
            params.put("IsYear", from == 2 ? "1" : "0");

            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.GET_PAYMENTS)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("webservice", statusCode + message);
                                Toast.makeText(me, message, Toast.LENGTH_LONG).show();

                                binding.progressBar.setVisibility(View.GONE);
                                paymentsAdapter = new PaymentsActivity.PaymentsListAdapter(getSupportFragmentManager());
                                binding.viewpager.setAdapter(paymentsAdapter);
                                binding.tvTotalTitle.setVisibility(View.GONE);
                                binding.viewpager.setCurrentItem(0);
                            }

                            @SuppressLint({"LogNotTimber", "LongLogTag"})
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                Gson gson = new Gson();
                                Type type = new TypeToken<ArrayList<PaymentsData>>() {
                                }.getType();
                                switch (from) {
                                    case 0:
                                        paymentsListWeek.clear();
                                        paymentsListWeek = gson.fromJson(jsonObject.getJSONArray("Data").toString(), type);
                                        break;

                                    case 1:
                                        paymentsListMonth.clear();
                                        paymentsListMonth = gson.fromJson(jsonObject.getJSONArray("Data").toString(), type);
                                        break;

                                    case 2:
                                        paymentsListYear.clear();
                                        paymentsListYear = gson.fromJson(jsonObject.getJSONArray("Data").toString(), type);
                                        break;
                                }
                                binding.tvTotalTitle.setVisibility(View.VISIBLE);
                                binding.tvTotalAmount.setText(Utils.getFormattedPrice(jsonObject.optString("TotalAmount"), true));
                                binding.progressBar.setVisibility(View.GONE);

                                if (paymentsAdapter == null) {
                                    paymentsAdapter = new PaymentsActivity.PaymentsListAdapter(getSupportFragmentManager());
                                    binding.viewpager.setAdapter(paymentsAdapter);
                                    binding.viewpager.setCurrentItem(0);
                                } else {
                                    paymentsAdapter.notifyDataSetChanged();
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}