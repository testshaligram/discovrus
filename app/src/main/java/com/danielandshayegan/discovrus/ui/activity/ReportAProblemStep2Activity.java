package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.LayoutReportAProblemStep2Binding;
import com.danielandshayegan.discovrus.models.ReasonData;
import com.danielandshayegan.discovrus.models.ReportProblemData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.utils.Validators;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.reportProblem;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

public class ReportAProblemStep2Activity extends BaseActivity {
    LayoutReportAProblemStep2Binding binding;
    int reasonId;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_report_a_problem_step_2);
        if (getIntent().getExtras() != null) {
            reasonId = getIntent().getIntExtra("reasonId", 0);
        }
        userId = App_pref.getAuthorizedUser(ReportAProblemStep2Activity.this).getData().getUserId();
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;

            case R.id.btnSend:
                if (isValid()) {
                    callReportProblem(reasonId);
                }
                break;
        }
    }

    private void callReportProblem(int reasonId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(ReportAProblemStep2Activity.this).
                                create(WebserviceBuilder.class).
                                reportProblem("", userId, reasonId, binding.etEmail.getText().toString(), "", binding.etFeedback.getText().toString())
                        , getCompositeDisposable(), reportProblem, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                ReportProblemData reportProblemData = (ReportProblemData) o;
                                if (reportProblemData != null) {
                                    Toast.makeText(ReportAProblemStep2Activity.this, R.string.report_submitted_successfully, Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }


    private boolean isValid() {
        try {
            if (binding.etEmail.getText().toString().isEmpty()) {
                binding.tvErrorName.setText(getString(R.string.please_enter_email_address));
                binding.tvErrorName.setVisibility(View.VISIBLE);
                binding.etEmail.requestFocus();
                return false;
            } else if (!Validators.isValidEmail(binding.etEmail.getText().toString())) {
                binding.tvErrorName.setText(getString(R.string.please_enter_valid_email_address));
                binding.tvErrorName.setVisibility(View.VISIBLE);
                binding.etEmail.requestFocus();
                return false;
            } else if (binding.etFeedback.getText().toString().isEmpty()) {
                binding.tvErrorName.setVisibility(View.GONE);
                binding.tvErrorFeedback.setVisibility(View.VISIBLE);
                binding.etEmail.requestFocus();
                return false;
            } else {
                binding.tvErrorName.setVisibility(View.GONE);
                binding.tvErrorFeedback.setVisibility(View.GONE);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}