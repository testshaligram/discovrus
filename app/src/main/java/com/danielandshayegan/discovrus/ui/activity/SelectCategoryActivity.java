package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySelectCategoryBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.CategorySelectFragment;

public class SelectCategoryActivity extends BaseActivity {

    ActivitySelectCategoryBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_category);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CategorySelectFragment.newInstance(), "")
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.enter, R.anim.no_anim);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_anim, R.anim.exit);
    }
}


