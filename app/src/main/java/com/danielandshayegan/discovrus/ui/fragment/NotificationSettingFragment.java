package com.danielandshayegan.discovrus.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentNotificationSettingsBinding;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveGeneralSettings;

public class NotificationSettingFragment extends BaseFragment {

    FragmentNotificationSettingsBinding binding;
    Map<String, String> settingsList = new HashMap<>();

    public NotificationSettingFragment() {

    }

    public static NotificationSettingFragment newInstance() {
        NotificationSettingFragment fragment = new NotificationSettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_settings, container, false);
        binding.txtTitle.setText(getActivity().getString(R.string.notifications));

        settingsList = Utils.getGeneralSettingsDataKeys(getActivity());

        setData();

        binding.swComments.setOnCheckedChangeListener(onCheckedChangedListener);
        binding.swLikes.setOnCheckedChangeListener(onCheckedChangedListener);
        binding.swMentions.setOnCheckedChangeListener(onCheckedChangedListener);
        binding.swNewFollowers.setOnCheckedChangeListener(onCheckedChangedListener);
        binding.swReferences.setOnCheckedChangeListener(onCheckedChangedListener);
        binding.swStories.setOnCheckedChangeListener(onCheckedChangedListener);
        binding.swTrendingNow.setOnCheckedChangeListener(onCheckedChangedListener);

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return binding.getRoot();
    }

    private void setData() {
        try {
            String gsData = Utils.getGeneralSettingsData(mContext);

            Gson gson = new Gson();
            Type type = new TypeToken<GeneralSettingsData>() {
            }.getType();
            GeneralSettingsData nsData = gson.fromJson(gsData, type);

            if (nsData != null) {
                binding.swComments.setChecked(Boolean.parseBoolean(nsData.getComments()));
                binding.swLikes.setChecked(Boolean.parseBoolean(nsData.getLikes()));
                binding.swMentions.setChecked(Boolean.parseBoolean(nsData.getMentions()));
                binding.swNewFollowers.setChecked(Boolean.parseBoolean(nsData.getNewFollowers()));
                binding.swReferences.setChecked(Boolean.parseBoolean(nsData.getReferences()));
                binding.swStories.setChecked(Boolean.parseBoolean(nsData.getStories()));
                binding.swTrendingNow.setChecked(Boolean.parseBoolean(nsData.getTrendingNow()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    CompoundButton.OnCheckedChangeListener onCheckedChangedListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String title = "";
            switch (buttonView.getId()) {
                case R.id.swComments:
                    title = "Comments";
                 //   settingsList.put("Comments", isChecked ? 1 : 0);
                    break;

                case R.id.swLikes:
                    title = "Likes";
                 //   settingsList.put("Likes", isChecked ? 1 : 0);
                    break;

                case R.id.swMentions:
                    title = "Mentions";
                   // settingsList.put("Mentions", isChecked ? 1 : 0);
                    break;

                case R.id.swNewFollowers:
                    title = "NewFollowers";
                  //  settingsList.put("NewFollowers", isChecked ? 1 : 0);
                    break;

                case R.id.swReferences:
                    title = "References";
                  //  settingsList.put("References", isChecked ? 1 : 0);
                    break;

                case R.id.swStories:
                    title = "Stories";
                  //  settingsList.put("Stories", isChecked ? 1 : 0);
                    break;

                case R.id.swTrendingNow:
                    title = "TrendingNow";
                   // settingsList.put("TrendingNow", isChecked ? 1 : 0);
                    break;
            }
            settingsList.put(title, isChecked ? "1" : "0");
            //callSetNotificationSettings();
            settingsList.put("LoginId", String.valueOf(App_pref.getAuthorizedUser(getActivity()).getData().getUserId()));
            SavePhotosAndVideosFragment.callSetGeneralSettings(getActivity(), settingsList);
        }
    };

/*
    private void callSetNotificationSettings() {
        settingsList.put("LoginId", App_pref.getAuthorizedUser(getActivity()).getData().getUserId());

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), new JSONObject(settingsList).toString());

        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity()).
                        create(WebserviceBuilder.class).
                        saveGeneralSettings(requestBody)
                , getCompositeDisposable(), saveGeneralSettings, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object object, WebserviceBuilder.ApiNames apiNames) {
                        try {
                            if (object != null)
                                settingsList.clear();
                                Utils.setGeneralSettingsData(getActivity(), (GeneralSettingsData) object);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                          throwable.printStackTrace();
                    }
                });
    }
*/
}