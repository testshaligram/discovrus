package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Selection;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.CommentsAdapter;
import com.danielandshayegan.discovrus.adapter.MapCategoryAdapter;
import com.danielandshayegan.discovrus.custome_veiws.OnSwipeTouchListener;
import com.danielandshayegan.discovrus.custome_veiws.circlelist.CircularListViewContentAlignment;
import com.danielandshayegan.discovrus.databinding.FragmentHeatMapBinding;
import com.danielandshayegan.discovrus.databinding.FragmentNavigationBinding;
import com.danielandshayegan.discovrus.db.AppDatabase;
import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.models.DeleteCommentData;
import com.danielandshayegan.discovrus.models.MapListData;
import com.danielandshayegan.discovrus.models.MapPostDetail;
import com.danielandshayegan.discovrus.models.PostCommentData;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePostCommentReplyData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.GPSTracker;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.gson.Gson;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.CircleLayer;
import com.mapbox.mapboxsdk.style.layers.HeatmapLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deleteComment;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCommentList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getMapByPostId;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveComments;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostCommentReply;
import static com.mapbox.mapboxsdk.style.expressions.Expression.all;
import static com.mapbox.mapboxsdk.style.expressions.Expression.division;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.gt;
import static com.mapbox.mapboxsdk.style.expressions.Expression.gte;
import static com.mapbox.mapboxsdk.style.expressions.Expression.has;
import static com.mapbox.mapboxsdk.style.expressions.Expression.heatmapDensity;
import static com.mapbox.mapboxsdk.style.expressions.Expression.interpolate;
import static com.mapbox.mapboxsdk.style.expressions.Expression.linear;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.lt;
import static com.mapbox.mapboxsdk.style.expressions.Expression.rgb;
import static com.mapbox.mapboxsdk.style.expressions.Expression.rgba;
import static com.mapbox.mapboxsdk.style.expressions.Expression.stop;
import static com.mapbox.mapboxsdk.style.expressions.Expression.toNumber;
import static com.mapbox.mapboxsdk.style.expressions.Expression.zoom;
import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleStrokeColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleStrokeWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapIntensity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapWeight;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textField;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHeatMap extends BaseFragment {

    FragmentHeatMapBinding mBinding;
    View view;
    private MapView mapView;
    private MapboxMap mapboxMap;
    private WebserviceBuilder apiService;
    int userId;
    private CompositeDisposable disposable = new CompositeDisposable();
    LaunchpadFragment fragment;
    MapListData SearchResult;
    GPSTracker gpsTracker;
    Location gpsLocation;

    private static final String EARTHQUAKE_SOURCE_URL = "https://www.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson";
    private static final String POST_SOURCE_ID = "post";
    private static final String HEATMAP_LAYER_ID = "post-heat";
    private static final String HEATMAP_LAYER_SOURCE = "post";
    private static final String CIRCLE_LAYER_ID = "post-circle";
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;

    public FragmentHeatMap() {
        // Required empty public constructor
    }

    public static FragmentHeatMap newInstance() {
        return new FragmentHeatMap();
    }

    public static FragmentHeatMap newInstance(Bundle extras) {
        FragmentHeatMap fragment = new FragmentHeatMap();
        fragment.setArguments(extras);
        return fragment;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_heat_map, container, false);
        view = mBinding.getRoot();

        Mapbox.getInstance(mActivity, getString(R.string.mapbox_access_token));

        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);



        mapView = view.findViewById(R.id.mapView);

        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                FragmentHeatMap.this.mapboxMap = mapboxMap;
                mapboxMap.getUiSettings().setAttributionEnabled(false);
                mapboxMap.getUiSettings().setLogoEnabled(false);
                mBinding.uiHeatMap.txtSearch.setVisibility(View.VISIBLE);
                initSearch();
                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        23.008698, 72.503071), 4));
                if (Utils.latitude == 0 && Utils.longitude == 0) {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermission();
                        Log.e("ask for permission", "true");
                    } else {
                        getLocationCall();
                        Log.e("Permission given", "success");
                    }
                } else
                    getMapData(Utils.latitude, Utils.longitude);
            }
        });
        mBinding.uiHeatMap.imageView.setOnClickListener(v -> {
            fragment = LaunchpadFragment.newInstance();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, fragment, "")
                    .commit();
        });
        return view;
    }

    private void initSearch() {
        mBinding.uiHeatMap.txtSearch.setOnClickListener(view -> {
            Intent intent = new PlaceAutocomplete.IntentBuilder()
                    .accessToken(Mapbox.getAccessToken())
                    .placeOptions(PlaceOptions.builder()
                            .backgroundColor(Color.parseColor("#EEEEEE"))
                            .limit(10)
//                                .addInjectedFeature(home)
//                                .addInjectedFeature(work)
                            .build(PlaceOptions.MODE_CARDS))
                    .build(mActivity);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {

            // Retrieve selected location's CarmenFeature
            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);

            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above
            FeatureCollection featureCollection = FeatureCollection.fromFeatures(
                    new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())});

            // Retrieve and update the source designated for showing a selected location's symbol layer icon
            /*GeoJsonSource source = mapboxMap.getSourceAs(geojsonSourceLayerId);
            if (source != null) {
                source.setGeoJson(featureCollection);
            }*/

            // Move map camera to the selected location
            CameraPosition newCameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                            ((Point) selectedCarmenFeature.geometry()).longitude()))
                    .zoom(10)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition), 4000);
//            getMapData(((Point) selectedCarmenFeature.geometry()).latitude(), ((Point) selectedCarmenFeature.geometry()).longitude());
            getMapData(23.03743, 72.50272);
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLocationCall();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    private void getLocationCall() {
        Log.e("getLocationCall", "success");
        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        gpsTracker = new GPSTracker(getActivity());
        gpsLocation = gpsTracker.getLocation();

        try {
            Log.e("try", "success");

            Thread.sleep(1000);
            if (gpsLocation != null) {
                Log.e("gpsLocation!= null", "success");
                mBinding.loading.progressBar.setVisibility(View.GONE);
                Utils.latitude = gpsLocation.getLatitude();
                Utils.longitude = gpsLocation.getLongitude();
                getMapData(gpsLocation.getLatitude(), gpsLocation.getLongitude());
            } else {
                Log.e("gpsLocation null", "success");
                Toast.makeText(getActivity(), "we are unable to detect your location.please search manually", Toast.LENGTH_LONG).show();
                mBinding.loading.progressBar.setVisibility(View.GONE);
            }
        } catch (InterruptedException e) {
            Log.e("catch", "success");
            Thread.interrupted();
            mBinding.loading.progressBar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "we are unable to detect your location.please search manually", Toast.LENGTH_LONG).show();
        }
    }

    public void getMapData(double latitude, double longitude) {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getMapList(latitude, longitude, 0.4, userId).
//        disposable.add(apiService.getMapList(22.994555, 72.616315, 1000).
        subscribeOn(Schedulers.io()).
                        observeOn(AndroidSchedulers.mainThread()).
                        subscribeWith(new DisposableSingleObserver<MapListData>() {
                            @Override
                            public void onSuccess(MapListData value) {
                                if (value != null) {
                                    SearchResult = value;
                                    if (SearchResult.getFeatures() != null && SearchResult.getFeatures().size() != 0) {

                                        List<MapListData.FeaturesBean> featuresList = new ArrayList<>();

                                        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                                latitude, longitude), 4));

                                        String json = new Gson().toJson(SearchResult);
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[23.0497,72.5117]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[23.022505,72.5713621]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[100.507665295,0.0035208782436]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[11188462.0943,389.378064197]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,23.0497]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5713621,23.022505]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"poi\":\"restaurant\",\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,23.0497]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"poi\":\"lodging\",\"CategoryName\":\"Education\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5713621,23.022505]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"poi\":\"cafe\",\"CategoryName\":\"Food\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"CategoryId\":11,\"CategoryName\":\"IT\",\"Icon\":\"/files/category/Icon/IT.png\",\"Lat\":\"23.500497\",\"Long\":\"72.5117\",\"PostId\":1688,\"PostNo\":0,\"RadiansDistance\":1.371268312512451},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117003,23.0497]}},{\"type\":\"Feature\",\"properties\":{\"CategoryId\":0,\"CategoryName\":\"\",\"Icon\":\"/files/category/Icon/location-pin.png\",\"Lat\":\"24.03500230\",\"Long\":\"72.543910\",\"PostId\":1256,\"PostNo\":0,\"RadiansDistance\":3.7273995709421888},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,24.0497]}},{\"type\":\"Feature\",\"properties\":{\"CategoryId\":0,\"CategoryName\":\"\",\"Icon\":\"/files/category/Icon/location-pin.png\",\"Lat\":\"23.022505\",\"Long\":\"72.5713621\",\"PostId\":1711,\"PostNo\":0,\"RadiansDistance\":6.746924368285192},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,25.0497]}}]}";
//                                        new FragmentNavigation.loadIconTask().execute(json);
                                /*mapboxMap.addImage("Education-64", BitmapUtils.getBitmapFromDrawable(
                                        getResources().getDrawable(R.drawable.education)));
                                mapboxMap.addImage("IT-64", BitmapUtils.getBitmapFromDrawable(
                                        getResources().getDrawable(R.drawable.it)));
                                mapboxMap.addImage("Food-64", BitmapUtils.getBitmapFromDrawable(
                                        getResources().getDrawable(R.drawable.food)));*/

//                                addClusteredGeoJsonSource(json);
                                        addEarthquakeSource(json);
                                        addHeatmapLayer();
                                        addCircleLayer();
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    } else {
                                        Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    }
                                } else {
                                    if (value.getFeatures() == null && value.getFeatures().size() == 0) {
                                        Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    } else {
                                        Toast.makeText(getActivity(), "Something want wrong; Please try after sometime.", Toast.LENGTH_LONG).show();
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    }
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d("fail", e.toString());
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                            }

                        }));

    }

    private void addEarthquakeSource(String json) {
       /* try {
            mapboxMap.addSource(new GeoJsonSource(POST_SOURCE_ID, new URL(EARTHQUAKE_SOURCE_URL)));
        } catch (MalformedURLException malformedUrlException) {
            Timber.e(malformedUrlException, "That's not an url... ");
        }*/
        try {
            mapboxMap.addSource(new GeoJsonSource(POST_SOURCE_ID, json)
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addHeatmapLayer() {
        HeatmapLayer layer = new HeatmapLayer(HEATMAP_LAYER_ID, POST_SOURCE_ID);
        layer.setMaxZoom(9);
        layer.setSourceLayer(HEATMAP_LAYER_SOURCE);
        layer.setProperties(

                // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
                // Begin color ramp at 0-stop with a 0-transparancy color
                // to create a blur-like effect.
                heatmapColor(
                        interpolate(
                                linear(), heatmapDensity(),
                                literal(0), rgba(33, 102, 172, 0),
                                literal(0.2), rgb(103, 169, 207),
                                literal(0.4), rgb(209, 229, 240),
                                literal(0.6), rgb(253, 219, 199),
                                literal(0.8), rgb(239, 138, 98),
                                literal(1), rgb(178, 24, 43)
                        )
                ),

                // Increase the heatmap weight based on frequency and property magnitude
                heatmapWeight(
                        interpolate(
                                linear(), get("mag"),
                                stop(0, 0),
                                stop(6, 1)
                        )
                ),

                // Increase the heatmap color weight weight by zoom level
                // heatmap-intensity is a multiplier on top of heatmap-weight
                heatmapIntensity(
                        interpolate(
                                linear(), zoom(),
                                stop(0, 1),
                                stop(9, 3)
                        )
                ),

                // Adjust the heatmap radius by zoom level
                heatmapRadius(
                        interpolate(
                                linear(), zoom(),
                                stop(0, 2),
                                stop(9, 20)
                        )
                ),

                // Transition from heatmap to circle layer by zoom level
                heatmapOpacity(
                        interpolate(
                                linear(), zoom(),
                                stop(7, 1),
                                stop(9, 0)
                        )
                )
        );

        mapboxMap.addLayerAbove(layer, "waterway-label");
    }

    private void addCircleLayer() {
        CircleLayer circleLayer = new CircleLayer(CIRCLE_LAYER_ID, POST_SOURCE_ID);
        circleLayer.setProperties(

                // Size circle radius by earthquake magnitude and zoom level
                circleRadius(
                        interpolate(
                                linear(), zoom(),
                                literal(7), interpolate(
                                        linear(), get("mag"),
                                        stop(1, 1),
                                        stop(6, 4)
                                ),
                                literal(16), interpolate(
                                        linear(), get("mag"),
                                        stop(1, 5),
                                        stop(6, 50)
                                )
                        )
                ),

                // Color circle by earthquake magnitude
                circleColor(
                        interpolate(
                                linear(), get("mag"),
                                literal(1), rgba(33, 102, 172, 0),
                                literal(2), rgb(103, 169, 207),
                                literal(3), rgb(209, 229, 240),
                                literal(4), rgb(253, 219, 199),
                                literal(5), rgb(239, 138, 98),
                                literal(6), rgb(178, 24, 43)
                        )
                ),

                // Transition from heatmap to circle layer by zoom level
                circleOpacity(
                        interpolate(
                                linear(), zoom(),
                                stop(7, 0),
                                stop(8, 1)
                        )
                ),
                circleStrokeColor("white"),
                circleStrokeWidth(1.0f)
        );

        mapboxMap.addLayerBelow(circleLayer, HEATMAP_LAYER_ID);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public void handleClicks(View v)
    {
        fragment.handleClicks(v);
    }
}
