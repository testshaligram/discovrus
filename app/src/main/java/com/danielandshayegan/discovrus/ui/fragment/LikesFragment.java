package com.danielandshayegan.discovrus.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.LikeListAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentLikesBinding;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getLikeListData;

public class LikesFragment extends BaseFragment implements SingleCallback {
    FragmentLikesBinding mBinding;
    int userId;
    PostListData.Post postData;

    public LikesFragment() {
    }

    public static LikesFragment newInstance() {
        LikesFragment fragment = new LikesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Bundle args = getArguments();
            if (args != null) {
                postData = args.getParcelable("postDetails");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_likes, container, false);
        mBinding.uiFollow.toolbar.imgBack.setImageResource(R.drawable.back_button);
        mBinding.uiFollow.toolbar.txtTitle.setText("Likes");
        mBinding.uiFollow.toolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        mBinding.uiFollow.recylerView.setVisibility(View.VISIBLE);
        mBinding.uiFollow.txtNoData.setVisibility(View.GONE);

        getLikeListData();

        return mBinding.getRoot();
    }


    private void getLikeListData() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void callAPI() {
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getLikeListData(App_pref.getAuthorizedUser(mContext).getData().getUserId(), postData.getPostId())
                        , getCompositeDisposable(), getLikeListData, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getLikeListData:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LikeListData followersUserListData = (LikeListData) o;
                if (followersUserListData.isSuccess()) {
                    if (followersUserListData.getData() != null && followersUserListData.getData().size() > 0) {
                        mBinding.uiFollow.recylerView.setVisibility(View.VISIBLE);
                        mBinding.uiFollow.txtNoData.setVisibility(View.GONE);
                        LikeListAdapter mAdapter = new LikeListAdapter(followersUserListData.getData(), getContext(), true);
                        mBinding.uiFollow.recylerView.setAdapter(mAdapter);
                    } else {
                        mBinding.uiFollow.recylerView.setVisibility(View.GONE);
                        mBinding.uiFollow.txtNoData.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}