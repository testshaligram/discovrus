package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentLinkSocialMediaBinding;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LinkSocialMediaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LinkSocialMediaFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentLinkSocialMediaBinding mBinding;


    public LinkSocialMediaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LinkSocialMediaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LinkSocialMediaFragment newInstance() {
        LinkSocialMediaFragment fragment = new LinkSocialMediaFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding= DataBindingUtil.inflate(inflater,R.layout.fragment_link_social_media, container, false);
        mBinding.toolbar.txtTitle.setText(R.string.str_title_social_media);
        mBinding.toolbar.imgBack.setOnClickListener(view->{
            getActivity().finish();
        });
        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }

}
