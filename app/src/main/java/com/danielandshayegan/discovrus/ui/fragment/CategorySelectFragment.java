package com.danielandshayegan.discovrus.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.CategoryItemAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentCategorySelectBinding;
import com.danielandshayegan.discovrus.db.AppDatabase;
import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;
import com.danielandshayegan.discovrus.ui.BaseFragment;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategorySelectFragment extends BaseFragment {

    FragmentCategorySelectBinding mBinding;
    View view;
    public static String CATEGORY_NAME = "category_name";
    public static String CATEGORY_ID = "category_id";
    public static String CATEGORY_IMAGE = "category_image";

    public CategorySelectFragment() {
        // Required empty public constructor
    }

    public static CategorySelectFragment newInstance() {
        return new CategorySelectFragment();
    }

    public static CategorySelectFragment newInstance(Bundle extras) {
        CategorySelectFragment fragment = new CategorySelectFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_category_select, container, false);
        view = mBinding.getRoot();
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Select Category");
        List<CategoriesTableModel> categoriesTableModels = AppDatabase.getAppDatabase(mContext).categoryDao().getAll();
        if (categoriesTableModels != null && categoriesTableModels.size() > 0) {
            CategoryItemAdapter categoryItemAdapter = new CategoryItemAdapter(mContext, categoriesTableModels);
            mBinding.uiMainFragment.listView.setAdapter(categoryItemAdapter);
            mBinding.uiMainFragment.listView.setSingleScroll(true);
            mBinding.uiMainFragment.sideSelector.setListView(mBinding.uiMainFragment.listView);
        }
        mBinding.uiMainFragment.listView.setOnItemClickListener((AdapterView.OnItemClickListener) (adapterView, view, i, l) -> {
            Intent intent = new Intent();
            intent.putExtra(CATEGORY_NAME, categoriesTableModels.get(i).getCategoryName());
            intent.putExtra(CATEGORY_ID, categoriesTableModels.get(i).getCategoryId());
            intent.putExtra(CATEGORY_IMAGE, categoriesTableModels.get(i).getImagePath());
            mActivity.setResult(Activity.RESULT_OK, intent);
            mActivity.onBackPressed();
        });

        return view;

    }

    @Override
    public boolean onBackPressed() {

        return super.onBackPressed();

    }
}
