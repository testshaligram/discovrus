package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.ApplozicClient;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.applozic.mobicomkit.api.account.user.PushNotificationTask;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;
import com.applozic.mobicomkit.uiwidgets.ApplozicSetting;
import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentLoginBinding;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.models.LoginData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.services.NotificationToken;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity;
import com.danielandshayegan.discovrus.ui.activity.MainActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.utils.Validators;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.JSONCallback;
import com.danielandshayegan.discovrus.webservice.Retrofit;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static android.support.constraint.Constraints.TAG;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.single;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_NAVIGATION;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.FRAGMENT_CONFEMAIL;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.ANIMATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements SingleCallback {


    //    FragmentRegisterBinding mBinding;
    FragmentLoginBinding mBinding;
    View view;
    private UserLoginTask mAuthTask = null;


    public LoginFragment() {
        // Required empty public constructor
    }

    static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public static LoginFragment newInstance(Bundle extras) {
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_login, container, false);
        view = mBinding.getRoot();
        mBinding.uiLogin.imgArrowDown.setOnClickListener(v -> {

            Intent intentLogin = new Intent(getActivity(), MainActivity.class).putExtra(ANIMATION, "loginAnimation");
            startActivity(intentLogin);
            mActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            mActivity.finish();
           /* getActivity().overridePendingTransition(R.anim.slide_in_down, R.anim.stay);
            getActivity().finish();*/
        });

        mBinding.uiLogin.btnLogin.setOnClickListener(v -> {
            if (validation()) {
                if (Utils.isNetworkAvailable(mActivity)) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        mBinding.uiLogin.txtForgotPassword.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, MainActivity.FRAGMENT_FORGOTPASS);
            startActivity(intent);
            mActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            mActivity.finish();
        });

        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            //r will be populated with the coordinates of your view that area still visible.
            view.getWindowVisibleDisplayFrame(r);

            int heightDiff = view.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...

                mBinding.uiLogin.imgCap.setVisibility(View.GONE);
            } else {
                mBinding.uiLogin.imgCap.setVisibility(View.VISIBLE);
            }
        });

        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            //r will be populated with the coordinates of your view that area still visible.
            view.getWindowVisibleDisplayFrame(r);

            int heightDiff = view.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...

                mBinding.uiLogin.imgCap.setVisibility(View.GONE);
            } else {
                mBinding.uiLogin.imgCap.setVisibility(View.VISIBLE);
            }
        });


        return view;
    }


    private void callAPI() {
        NotificationToken notificationToken = App_pref.getNotificationToken(mActivity);
        String AndroidDeviceID = notificationToken.getAccessToken();
        Log.e(TAG, "storedRegId: " + AndroidDeviceID);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                userLogin(mBinding.uiLogin.edtEmail.getText().toString(), null, mBinding.uiLogin.edtPassword.getText().toString(), true, false, false, false, AndroidDeviceID, "")
                        , getCompositeDisposable(), single, this);
    }


    public boolean validation() {

        boolean value = true;
        if (mBinding.uiLogin.edtEmail.length() == 0) {
            mBinding.uiLogin.txtErrorEmail.setText(R.string.null_email);
            mBinding.uiLogin.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        } else if (!Validators.isValidEmail(String.valueOf(mBinding.uiLogin.edtEmail.getText().toString()))) {
            mBinding.uiLogin.txtErrorEmail.setText(R.string.valid_email);
            mBinding.uiLogin.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        }
        if (mBinding.uiLogin.edtPassword.length() == 0) {
            mBinding.uiLogin.txtErrorPass.setText(R.string.null_password);
            mBinding.uiLogin.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (mBinding.uiLogin.edtPassword.length() < 6) {
            mBinding.uiLogin.txtErrorPass.setText(R.string.pass_six_digit);
            mBinding.uiLogin.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        }
        return value;

    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LoginData loginModel = (LoginData) o;
                if (loginModel.isSuccess()) {
                    App_pref.saveAuthorizedUser(getActivity(), loginModel);
                    Toast.makeText(getActivity(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                    setUpAppLogicUser(User.AuthenticationType.APPLOZIC);
                    if (App_pref.getAuthorizedUser(getActivity()) != null) {
                        ApplicationClass.OnlineUserListAPI(getActivity(), true);
                    }

                    callGetGeneralSettings(getActivity());

/*
                    Utils.callGetGeneralSettings(getActivity(), App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), new CallbackTask() {
                        @Override
                        public void onFail(Object object) {
                            Log.e("general settings error", object.toString());
//                            Toast.makeText(getActivity(), "callGetGeneralSettings onFail", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onSuccess(Object object) {
                            Utils.setGeneralSettingsData(mActivity, (GeneralSettingsData) object);

                            */
/*{"Data":{"LoginId":6,"Comments":false,"Likes":true,"Mentions":false,"NewFollowers":false,"References":false,
                            "Stories":false,"TrendingNow":false,"SavePhotos":false,"SaveVideos":false,"AllowStorySharing":false,
                            "SaveStories":false},"Success":true,"Message":["Save Successfully"],"Count":"","TotalRecord":""}*//*

                        //    try {
*/
/*                                ArrayList<GeneralSettingsData.DataBean> dataBeans = ((GeneralSettingsData) object).getData();
                                Log.e("dataBeans sixe", dataBeans.size() + "");
                               // Iterator iterator = dataBeans.iterator();
                                HashMap<String, Object> keyList = new HashMap<>();
                               //
                                Gson gson = new GsonBuilder().create();
                                JsonArray myCustomArray = gson.toJsonTree(dataBeans).getAsJsonArray();
                                JSONArray jsArray = new JSONArray(myCustomArray.toString());
                                JSONObject jObject = jsArray.getJSONObject(0);
                                Iterator iterator = jObject.keys();
                                while (iterator.hasNext()) {
                                    String key = (String) iterator.next();
                                    JSONObject issue = jObject.getJSONObject(key);

                                    //  get id from  issue
                                    String _pubKey = issue.optString("id");
                                    Log.e("key", _pubKey);
                                    keyList.put(_pubKey, "");
                                }
                                Utils.setGeneralSettingsDataKeys(mActivity, keyList);

                                Gson gson = new Gson();
                                Type type = new TypeToken<ArrayList<GeneralSettingsData.DataBean>>() {
                                }.getType();
                                String strObject = (gson.fromJson(((GeneralSettingsData) object).toString(), type)).toString();
                                //  JSONObject issueObj = new JSONObject(strObject);
                                //if (issueObj.has("Success") && issueObj.optString("Success").equalsIgnoreCase("true")) {

                                JSONArray jObj = new JSONArray(strObject);*//*




                                // }


                            */
/*} catch (JSONException e) {
                                e.printStackTrace();
                            }*//*

                        }

                        @Override
                        public void onFailure(Throwable t) {
//                            Toast.makeText(getActivity(), "callGetGeneralSettings onFailure", Toast.LENGTH_LONG).show();
                        }
                    });
*/

                    Intent intent = new Intent(getActivity(), MenuActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_NAVIGATION);
                    // intent.putExtra("email", loginModel.getData().getEmail());
                    startActivity(intent);
                    mActivity.overridePendingTransition(R.anim.enter, R.anim.stay);
                    mActivity.finish();

                } else {
                    Toast.makeText(getContext(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                    if (loginModel.getMessage().get(0).equalsIgnoreCase("User is not Verified.")) {
                        Intent intent = new Intent(getActivity(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_CONFEMAIL);
                        intent.putExtra("email", mBinding.uiLogin.edtEmail.getText().toString());
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.stay);
                    }
                }
                break;

        }
    }

    public static void callGetGeneralSettings(Context me) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("LoginId", String.valueOf(App_pref.getAuthorizedUser(me).getData().getUserId()));
            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.GET_GENERAL_SETTINGS)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("webservice", statusCode + message);
                            }

                            @SuppressLint({"LogNotTimber", "LongLogTag"})
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                Gson gson = new Gson();
                                Log.e("general settings response", jsonObject.toString());
                                JSONObject jObj = (JSONObject) jsonObject.getJSONArray("Data").get(0);

                                Utils.setGeneralSettingsData(me, jObj);

                                HashMap<String, String> keyList = new HashMap<>();

                                Iterator<String> keys = jObj.keys();

                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    keyList.put(key, "");
                                }

                                Utils.setGeneralSettingsDataKeys(me, keyList);
                                Log.e("general settings response key size", keyList.size() + "");
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpAppLogicUser(User.AuthenticationType authenticationType) {


        UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {

            @Override
            public void onSuccess(RegistrationResponse registrationResponse, final Context context) {
                mAuthTask = null;


                ApplozicClient.getInstance(context).setContextBasedChat(true).setHandleDial(true);

                Map<ApplozicSetting.RequestCode, String> activityCallbacks = new HashMap<ApplozicSetting.RequestCode, String>();
                activityCallbacks.put(ApplozicSetting.RequestCode.USER_LOOUT, MenuActivity.class.getName());
                ApplozicSetting.getInstance(context).setActivityCallbacks(activityCallbacks);
                MobiComUserPreference.getInstance(context).setUserRoleType(registrationResponse.getRoleType());

                PushNotificationTask.TaskListener pushNotificationTaskListener = new PushNotificationTask.TaskListener() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {

                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
//
                    }
                };
                PushNotificationTask pushNotificationTask = new PushNotificationTask(Applozic.getInstance(context).getDeviceRegistrationId(), pushNotificationTaskListener, context);
                pushNotificationTask.execute((Void) null);

            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                mAuthTask = null;

                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle(exception.getMessage().toString());
                alertDialog.setMessage(exception.toString());
                alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok_alert),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

            }
        };

        if (App_pref.getAuthorizedUser(getContext()) != null) {
            User user = new User();
            user.setUserId(String.valueOf(App_pref.getAuthorizedUser(getContext()).getData().getUserId()));
            user.setEmail(App_pref.getAuthorizedUser(getContext()).getData().getEmail());
            user.setPassword("123456");
            user.setDisplayName(App_pref.getAuthorizedUser(getContext()).getData().getFirstName());
            //user.setContactNumber(App_pref.getAuthorizedUser(getApplicationContext()).getData().getN());
            user.setImageLink(ApiClient.WebService.imageUrl + App_pref.getAuthorizedUser(getContext()).getData().getUserImagePath());
            user.setAuthenticationTypeId(authenticationType.getValue());

            mAuthTask = new UserLoginTask(user, listener, getContext());
            mAuthTask.execute((Void) null);
           /* Applozic.connectUser(getActivity(), user, new AlLoginHandler() {
                @Override
                public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                    ApplozicClient.getInstance(context).setContextBasedChat(true).setHandleDial(true);

                    Map<ApplozicSetting.RequestCode, String> activityCallbacks = new HashMap<ApplozicSetting.RequestCode, String>();
                    activityCallbacks.put(ApplozicSetting.RequestCode.USER_LOOUT, MenuActivity.class.getName());
                    ApplozicSetting.getInstance(context).setActivityCallbacks(activityCallbacks);
                    MobiComUserPreference.getInstance(context).setUserRoleType(registrationResponse.getRoleType());
                    Applozic.registerForPushNotification(context, Applozic.getInstance(context).getDeviceRegistrationId(), new   AlPushNotificationHandler() {
                        @Override
                        public void onSuccess(RegistrationResponse registrationResponse) {

                        }

                        @Override
                        public void onFailure(RegistrationResponse registrationResponse, Exception exception) {

                        }
                    });

                    PushNotificationTask.TaskListener pushNotificationTaskListener = new PushNotificationTask.TaskListener() {
                        @Override
                        public void onSuccess(RegistrationResponse registrationResponse) {

                        }

                        @Override
                        public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
//
                        }
                    };
                    PushNotificationTask pushNotificationTask = new PushNotificationTask(Applozic.getInstance(context).getDeviceRegistrationId(), pushNotificationTaskListener, context);
                    pushNotificationTask.execute((Void) null);
                }

                @Override
                public void onFailure(RegistrationResponse registrationResponse, Exception exception) {

                    android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle(exception.getMessage().toString());
                    alertDialog.setMessage(exception.toString());
                    alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok_alert),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            });*/
        }
    }
}
