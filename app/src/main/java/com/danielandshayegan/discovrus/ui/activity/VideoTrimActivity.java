package com.danielandshayegan.discovrus.ui.activity;

import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.VideoTrimmer;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.interfaces.OnHgLVideoListener;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.interfaces.OnTrimVideoListener;
import com.danielandshayegan.discovrus.ui.BaseActivity;

import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

public class VideoTrimActivity extends BaseActivity implements OnTrimVideoListener, OnHgLVideoListener {
    private VideoTrimmer mVideoTrimmer;
    private ProgressDialog mProgressDialog;
    public static String VIDEO_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trim);
        Intent extraIntent = getIntent();
        String path = "";
        int maxDuration = 10;

        if (extraIntent != null) {
            path = extraIntent.getStringExtra(MenuActivity.EXTRA_VIDEO_PATH);
            maxDuration = extraIntent.getIntExtra(MenuActivity.VIDEO_TOTAL_DURATION, 10);
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.trimming_progress));

        mVideoTrimmer = ((VideoTrimmer) findViewById(R.id.timeLine));
        if (mVideoTrimmer != null) {

            Log.e("tg", "maxDuration = " + maxDuration);
            mVideoTrimmer.setMaxDuration(maxDuration);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnHgLVideoListener(this);
            //mVideoTrimmer.setDestinationPath("/storage/emulated/0/DCIM/CameraCustom/");
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            mVideoTrimmer.setVideoInformationVisibility(true);
        }

    }

    @Override
    public void onVideoPrepared() {

    }

    @Override
    public void onTrimStarted() {
        mProgressDialog.show();
    }

    @Override
    public void getResult(final Uri contentUri) {
        mProgressDialog.dismiss();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast.makeText(TrimmerActivity.this, getString(R.string.video_saved_at, contentUri.getPath()), Toast.LENGTH_SHORT).show();

            }
        });

        try {
            String path = contentUri.getPath();
            // File file = new File(path);
            //  Log.e("tg", " path1 = " + path + " uri1 = " + Uri.fromFile(file));


            VIDEO_URL = String.valueOf(path);
            /*Intent intent = new Intent(VideoTrimActivity.this,
                    MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "path");
            startActivity(intent);
            finish();*/

            Intent intent = new Intent();
            intent.putExtra(ACTIVITY_INTENT, "path");
            setResult(RESULT_OK, intent);
            finish();

            //  String selectedPath = getPath(Uri.fromFile(file));

            /* if (selectedPath != null)*/
           /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromFile(file));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(Uri.fromFile(file), "video/*");
            startActivity(intent);
            finish();*/

        } catch (Exception ex) {
            Log.e("tg", " " + ex.getMessage());
            ex.printStackTrace();

        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else {
            return null;
        }
    }


    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(VideoTrimActivity.this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    private void playUriOnVLC(Uri uri) {

        int vlcRequestCode = 42;
        Intent vlcIntent = new Intent(Intent.ACTION_VIEW);
        vlcIntent.setPackage("org.videolan.vlc");
        vlcIntent.setDataAndTypeAndNormalize(uri, "video/*");
        vlcIntent.putExtra("title", "Kung Fury");
        vlcIntent.putExtra("from_start", false);
        vlcIntent.putExtra("position", 90000l);
        startActivityForResult(vlcIntent, vlcRequestCode);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("tg", "resultCode = " + resultCode + " data " + data);


    }

    @Override
    public void cancelAction() {
        mProgressDialog.dismiss();
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(final String message) {
        mProgressDialog.dismiss();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast.makeText(TrimmerActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
