package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentSelectPostReachBinding;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.LaunchpadActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.GPSTracker;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.text.DecimalFormat;

import io.reactivex.disposables.CompositeDisposable;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectPostReachFragment extends BaseFragment {

    FragmentSelectPostReachBinding mBinding;
    View view;
    private MapView mapView;
    private MapboxMap mapboxMap;
    int userId;
    String distance = "400";
    double distanceAmount = 0;
    View thumbViewDistance;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static SelectPostReachFragment fragmentNavigation;
    //LaunchpadFragment fragment;

    public SelectPostReachFragment() {
        // Required empty public constructor
    }

    public static SelectPostReachFragment newInstance() {
        return new SelectPostReachFragment();
    }

    public static SelectPostReachFragment newInstance(Bundle extras) {
        SelectPostReachFragment fragment = new SelectPostReachFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_select_post_reach, container, false);
        view = mBinding.getRoot();
        fragmentNavigation = this;
        Mapbox.getInstance(mActivity, getString(R.string.mapbox_access_token));

        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        thumbViewDistance = LayoutInflater.from(getActivity()).inflate(R.layout.layout_seekbar_thumb, null, false);
        mBinding.uiSelectPostReach.seekBarDistance.setThumb(getThumb("400 M", thumbViewDistance));

        mapView = view.findViewById(R.id.mapView);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap map) {
                mapboxMap = map;
                mapboxMap.getUiSettings().setAttributionEnabled(false);
                mapboxMap.getUiSettings().setLogoEnabled(false);



                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        51.5136143, -0.13654860000000002), 10));

            }
        });

        mBinding.uiSelectPostReach.imageCap.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(getActivity(), LaunchpadActivity.class));
        });

        mBinding.uiSelectPostReach.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.finish();
            }
        });

        mBinding.uiSelectPostReach.btnDone.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.putExtra("distance", distance);
            intent.putExtra("distanceAmount", distanceAmount);
            mActivity.setResult(Activity.RESULT_OK, intent);
            mActivity.finish();
        });

        mBinding.uiSelectPostReach.seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String progressString = "";
                String distanceToShow = "";
                if (progress <= 400) {
                    distance = "400";
                    distanceToShow = "400 M";
                    distanceAmount = 0;
                    mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one_red));
                    mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two));
                    mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three));
                    mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four));
                    mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five));
                } else if (progress < 1000) {
                    distance = ""+progress;
                    distance = progress + " M";
                    if (progress == 500) {
                        distanceAmount = 0.50;
                    } else if (progress - 400 != 0) {
                        distanceAmount = ((progress) / 100) * 0.10;
                    } else {
                        distanceAmount = 0;
                    }
                    mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one_red));
                    mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two));
                    mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three));
                    mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four));
                    mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five));
                } else {
                    int progressInt = (progress / 1000);
                    distance = progressInt+"";
                    distanceToShow = progressInt+" KM";
                    distanceAmount = (((progressInt * 1000)) / 100) * 0.10;
                    double miles = convertKmsToMiles(Float.parseFloat(distance));
                    if (miles < 1) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one_red));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five));
                    } else if (miles >= 1 && miles < 2) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two_red));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five));
                    } else if (miles >= 2 && miles < 5) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three_red));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five));
                    } else if (miles >= 5 && miles < 9) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four_red));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five));
                    }else if (miles >= 9) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.map_layer_circle_five_red));
                    }
                }
                // You can have your own calculation for progress
                seekBar.setThumb(getThumb(distanceToShow, thumbViewDistance));
                Log.e("distanceAmount", "Amount Payable due to Distance " + decimalFormat.format(distanceAmount));
                mBinding.uiSelectPostReach.txtPrice.setText(getString(R.string.pound_sign) + " " + decimalFormat.format(distanceAmount));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }

    public double convertKmsToMiles(float kms){
        double miles = 0.621371 * kms;
        return miles;
    }

    public Drawable getThumb(String progress, View thumbView) {
        ((TextView) thumbView.findViewById(R.id.tvProgress)).setText(progress);

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(getResources(), bitmap);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

   /* @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }*/

}
