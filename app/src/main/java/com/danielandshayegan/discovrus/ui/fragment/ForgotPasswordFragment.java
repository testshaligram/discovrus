package com.danielandshayegan.discovrus.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentForgotPasswordBinding;
import com.danielandshayegan.discovrus.models.ForgotPasswordData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity;
import com.danielandshayegan.discovrus.ui.activity.MainActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.utils.Validators;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.single;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends BaseFragment implements SingleCallback {

    FragmentForgotPasswordBinding mBinding;
    View view;


    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    public static ForgotPasswordFragment newInstance(Bundle extras) {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_forgot_password, container, false);
        view = mBinding.getRoot();
        mBinding.uiForgotPass.imgArrowDown.setOnClickListener(v -> {

            Intent intentLogin = new Intent(getActivity(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, MainActivity.FRAGMENT_LOGIN);
            startActivity(intentLogin);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            getActivity().finish();
           /* getActivity().overridePendingTransition(R.anim.slide_in_down, R.anim.stay);
            getActivity().finish();*/
        });
        mBinding.uiForgotPass.btnSubmit.setOnClickListener(v -> {
            if (validation()) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                view.getWindowVisibleDisplayFrame(r);

                int heightDiff = view.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...

                    mBinding.uiForgotPass.imgCap.setVisibility(View.GONE);
                } else {
                    mBinding.uiForgotPass.imgCap.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }


    public boolean validation() {

        boolean value = true;
        if (mBinding.uiForgotPass.edtEmail.length() == 0) {
            mBinding.uiForgotPass.txtErrorEmail.setText(R.string.null_email);
            mBinding.uiForgotPass.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        } else if (!Validators.isValidEmail(String.valueOf(mBinding.uiForgotPass.edtEmail.getText().toString()))) {
            mBinding.uiForgotPass.txtErrorEmail.setText(R.string.valid_email);
            mBinding.uiForgotPass.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        }
        return value;

    }


    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                userForgotPassword(mBinding.uiForgotPass.edtEmail.getText().toString())
                        , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case single:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ForgotPasswordData forgotPasswordData = (ForgotPasswordData) o;
                if (forgotPasswordData.isSuccess()) {
                    Toast.makeText(getActivity(), forgotPasswordData.getMessage().get(0), Toast.LENGTH_LONG).show();
                    break;
                } else {

                    Toast.makeText(getContext(), forgotPasswordData.getMessage().get(0), Toast.LENGTH_LONG).show();
                }


        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}
