package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.NotificationsAdapter;
import com.danielandshayegan.discovrus.databinding.ActivityNotificationsBinding;
import com.danielandshayegan.discovrus.models.Book;
import com.danielandshayegan.discovrus.ui.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class NotificationsActivity extends BaseActivity {
    ActivityNotificationsBinding mBinding;
    private NotificationsAdapter notificationsAdapter;
    private LinearLayoutManager layoutManager;
    private List<Book> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_notifications);
        setRecyclerView();
    }

    public void setRecyclerView() {
        mBinding.recyclerNotifications.setVisibility(View.GONE);
        mBinding.txtNoData.setVisibility(View.VISIBLE);
        getNextItems();
        layoutManager = new LinearLayoutManager(this);
        mBinding.recyclerNotifications.setHasFixedSize(false);
        mBinding.recyclerNotifications.setLayoutManager(layoutManager);
        notificationsAdapter = new NotificationsAdapter(data, this, mBinding.recyclerNotifications);
        mBinding.recyclerNotifications.setAdapter(notificationsAdapter);
        mBinding.recyclerNotifications.setItemAnimator(new DefaultItemAnimator());

        notificationsAdapter.setOnLoadMoreListener(new NotificationsAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.i("LoadMore", "onLoadMore called");
                //add progress item
                data.add(null);
                notificationsAdapter.notifyDataSetChanged();

                Log.i("LoadMore", "Loading new data... (" + 5 + ") posts");
                //remove progress item
                data.remove(data.size() - 1);

                getNextItems();

                notificationsAdapter.notifyDataSetChanged();
                //add items one by one

                notificationsAdapter.setLoaded();
            }
        });

    }

    private void getNextItems() {
        int itemCount = data.size();
        for (int i = itemCount; i <= itemCount + 15; i++) {
            Book book = new Book();
            book.title = "Title " + i;
            book.author = "Author " + i;
            book.description = "Description " + i;
            book.imageId = R.drawable.placeholder_image;
            data.add(book);
        }
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}