package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.SavePostFragment;
import com.danielandshayegan.discovrus.ui.fragment.SettingFragment;

public class SavePostActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_post);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, SavePostFragment.newInstance(), "settingsFragment")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
