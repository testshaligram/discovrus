package com.danielandshayegan.discovrus.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.SavePostAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentSavePostBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.models.SavePostListData;
import com.danielandshayegan.discovrus.models.SavePostViewData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailActivity;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailVideoActivity;
import com.danielandshayegan.discovrus.utils.Utils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addToFavourite;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getSavedPosts;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePost;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostView;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SavePostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SavePostFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    GridLayoutManager layoutManagerPost;
    public int userId, logInUserId;
    int currentPagePost = 1, pageSize = 10;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentSavePostBinding mBinding;
    SavePostAdapter savePostAdapter;
    public static SavePostFragment savePostFragment;

    public SavePostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SavePostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SavePostFragment newInstance() {
        SavePostFragment fragment = new SavePostFragment();
      /*  Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_save_post, container, false);
        mBinding.uiSavePost.toolbar.txtTitle.setText("Saved Post");
        savePostFragment = this;
        mBinding.uiSavePost.toolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        //   userId = getArguments().getInt("userId");
        logInUserId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();

        layoutManagerPost = new GridLayoutManager(getActivity(), 2);
        layoutManagerPost.setOrientation(LinearLayoutManager.VERTICAL);
      /*  linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mBinding.recyclerBusinessProfile.setLayoutManager(linearLayoutManager);
*/
        mBinding.uiSavePost.recylerView.setLayoutManager(layoutManagerPost);

        getSaveDataList();
        return mBinding.getRoot();
    }

    private void getSaveDataList() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void callAPI() {

        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getSavedPosts(logInUserId, currentPagePost, pageSize)
                        , getCompositeDisposable(), getSavedPosts, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                SavePostListData postData = (SavePostListData) o;
                                if (postData.isSuccess()) {
                                    savePostAdapter = new SavePostAdapter(postData, SavePostFragment.this);

                                }

                                mBinding.uiSavePost.recylerView.setItemAnimator(new SlideInUpAnimator());
                                mBinding.uiSavePost.recylerView.setAdapter(savePostAdapter);
                                Log.e("onSingleSuccess: ", "Success");
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");

                            }
                        });
    }

    public void savePostView(int postId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                savePostView(postId, userId)
                        , getCompositeDisposable(), savePostView, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePostViewData savePostViewData = (SavePostViewData) o;
                                //                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                                if (savePostViewData.isSuccess())
                                    Log.e("savePostViewData", "savePostViewData");
                                else
                                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed" + throwable.getMessage());

                            }
                        });

    }

    public void getPostImageDetails(int postId) {
        Intent intent1 = new Intent(getActivity(), ClickPostDetailActivity.class);
        intent1.putExtra("clickFragmentName", "postImage");
        intent1.putExtra("postId", String.valueOf(postId));
        startActivity(intent1);
        getActivity().overridePendingTransition(0, 0);
    }

    public void getPostTextDetails(int postId) {
        Intent intent = new Intent(getActivity(), ClickPostDetailActivity.class);
        intent.putExtra("clickFragmentName", "postText");
        intent.putExtra("postId", String.valueOf(postId));
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    public void getPostVideoDetails(int postId) {
        Bundle bundle = new Bundle();
        bundle.putString("postId", String.valueOf(postId));
        Intent intent = new Intent(getActivity(), ClickPostDetailVideoActivity.class);
        intent.putExtra("clickFragmentName", "postVideo");
        intent.putExtras(bundle);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    public void unSavePost(int position, int postId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                savePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postId, false)
                        , getCompositeDisposable(), savePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                if (savePostData.isSuccess()) {
                                    callAPI();
                                } else {
                                    Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }

}
