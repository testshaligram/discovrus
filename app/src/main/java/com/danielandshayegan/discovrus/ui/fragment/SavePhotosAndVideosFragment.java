package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentSavePhotosVideosBinding;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.JSONCallback;
import com.danielandshayegan.discovrus.webservice.Retrofit;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SavePhotosAndVideosFragment extends BaseFragment {

    FragmentSavePhotosVideosBinding binding;
    Map<String, String> settingsList = new HashMap<>();

    public SavePhotosAndVideosFragment() {
    }

    public static SavePhotosAndVideosFragment newInstance() {
        SavePhotosAndVideosFragment fragment = new SavePhotosAndVideosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_save_photos_videos, container, false);
        binding.txtTitle.setText(getActivity().getString(R.string.save_photos_videos));

        settingsList = Utils.getGeneralSettingsDataKeys(getActivity());
        settingsList.put("LoginId", String.valueOf(App_pref.getAuthorizedUser(getActivity()).getData().getUserId()));

        setData();

        binding.swSavePhotos.setOnCheckedChangeListener((buttonView, isChecked) -> {
            settingsList.put("SavePhotos", isChecked ? "1" : "0");
            callSetGeneralSettings(getActivity(), settingsList);
        });

        binding.swSaveVideos.setOnCheckedChangeListener((buttonView, isChecked) -> {
            settingsList.put("SaveVideos", isChecked ? "1" : "0");
            callSetGeneralSettings(getActivity(), settingsList);
        });

        binding.imgBack.setOnClickListener(v -> getActivity().finish());
        return binding.getRoot();
    }

    private void setData() {
        try {
            String gsData = Utils.getGeneralSettingsData(mContext);

            Gson gson = new Gson();
            Type type = new TypeToken<GeneralSettingsData>() {
            }.getType();
            GeneralSettingsData nsData = gson.fromJson(gsData, type);

            if (nsData != null) {
                binding.swSavePhotos.setChecked(Boolean.parseBoolean(nsData.getSavePhotos()));
                binding.swSaveVideos.setChecked(Boolean.parseBoolean(nsData.getSaveVideos()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callSetGeneralSettings(Context me, Map<String, String> settingsList) {
        /*RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), new JSONObject(settingsList).toString());

        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity()).
                        create(WebserviceBuilder.class).
                        saveGeneralSettings(requestBody)
                , getCompositeDisposable(), saveGeneralSettings, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object object, WebserviceBuilder.ApiNames apiNames) {
                        try {
                            if (object != null) {
                                Utils.setGeneralSettingsData(getActivity(), (GeneralSettingsData) object);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        throwable.printStackTrace();
                    }
                });*/

        try {
            JSONObject jObj = new JSONObject(settingsList);
            Log.e("HashMap Params", jObj.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.SET_GENERAL_SETTINGS)
                        .setParameters(jObj)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("webservice", statusCode + message);
                            }

                            @SuppressLint({"LogNotTimber", "LongLogTag"})
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                Gson gson = new Gson();
                                Log.e("save general settings response", jsonObject.toString());
                                JSONObject jObj;

                                if(jsonObject.get("Data") instanceof JSONObject) {
                                    jObj = jsonObject.getJSONObject("Data");
                                }
                                else
                                {
                                    jObj = (JSONObject) jsonObject.getJSONArray("Data").get(0);
                                }

                                Utils.setGeneralSettingsData(me, jObj);

                                HashMap<String, String> keyList = new HashMap<>();

                                Iterator<String> keys = jObj.keys();

                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    keyList.put(key, "");
                                }

                                Utils.setGeneralSettingsDataKeys(me, keyList);
                                Log.e("general settings response key size", keyList.size() + "");
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}