package com.danielandshayegan.discovrus.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.utils.FileUtils;
import com.danielandshayegan.discovrus.databinding.ActivitySelectPostReachBinding;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

public class SelectPostReachActivity extends BaseActivity {

    ActivitySelectPostReachBinding mBinding;
    Animator translationAnimator;
    private static final int REQUEST_VIDEO_TRIMMER = 0x01;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    Bundle bundle;
    public Uri mImageUri;
    public static SelectPostReachActivity selectPostReachActivity;
    public static String videoUri;

    private MapView mapView;
    private MapboxMap mapboxMap;
    int userId;
    String distance = "400";
    double distanceAmount = 0;
    View thumbViewDistance;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_post_reach);
        mBinding.discovrusNews.setImageDrawable(getResources().getDrawable(R.drawable.newsfeed_selected));
        mBinding.btnAddPost.setRotation(45.0f);
        selectPostReachActivity = SelectPostReachActivity.this;

        bottomSheetClick();
        bottomMenuClick();

        setUi(savedInstanceState);

    }

    public void setUi(Bundle savedInstanceState) {
        Mapbox.getInstance(SelectPostReachActivity.this, getString(R.string.mapbox_access_token));

        userId = App_pref.getAuthorizedUser(SelectPostReachActivity.this).getData().getUserId();
        thumbViewDistance = LayoutInflater.from(SelectPostReachActivity.this).inflate(R.layout.layout_seekbar_thumb, null, false);
        mBinding.uiSelectPostReach.seekBarDistance.setThumb(getThumb("400 M", thumbViewDistance));

        mapView = findViewById(R.id.mapView);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap map) {
                mapboxMap = map;
                mapboxMap.getUiSettings().setAttributionEnabled(false);
                mapboxMap.getUiSettings().setLogoEnabled(false);



                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        51.5136143, -0.13654860000000002), 10));

            }
        });

        mBinding.uiSelectPostReach.imageCap.setOnClickListener(v -> {
            startActivity(new Intent(SelectPostReachActivity.this, LaunchpadActivity.class));
        });

        mBinding.uiSelectPostReach.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.uiSelectPostReach.btnDone.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.putExtra("distance", distance);
            intent.putExtra("distanceAmount", distanceAmount);
            setResult(Activity.RESULT_OK, intent);
            finish();
        });

        mBinding.uiSelectPostReach.seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String progressString = "";
                String distanceToShow = "";
                if (progress <= 400) {
                    distance = "400";
                    distanceToShow = "400 M";
                    distanceAmount = 0;
                    mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one_red));
                    mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two));
                    mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three));
                    mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four));
                    mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five));
                } else if (progress < 1000) {
                    distance = ""+progress;
                    distance = progress + " M";
                    if (progress == 500) {
                        distanceAmount = 0.50;
                    } else if (progress - 400 != 0) {
                        distanceAmount = ((progress) / 100) * 0.10;
                    } else {
                        distanceAmount = 0;
                    }
                    mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one_red));
                    mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two));
                    mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three));
                    mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four));
                    mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five));
                } else {
                    int progressInt = (progress / 1000);
                    distance = progressInt+"";
                    distanceToShow = progressInt+" KM";
                    distanceAmount = (((progressInt * 1000)) / 100) * 0.10;
                    double miles = convertKmsToMiles(Float.parseFloat(distance));
                    if (miles < 1) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one_red));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five));
                    } else if (miles >= 1 && miles < 2) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two_red));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five));
                    } else if (miles >= 2 && miles < 5) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three_red));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five));
                    } else if (miles >= 5 && miles < 9) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four_red));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five));
                    }else if (miles >= 9) {
                        mBinding.uiSelectPostReach.circleOne.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_one));
                        mBinding.uiSelectPostReach.circleTwo.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_two));
                        mBinding.uiSelectPostReach.circleThree.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_three));
                        mBinding.uiSelectPostReach.circleFour.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_four));
                        mBinding.uiSelectPostReach.circleFive.setImageDrawable(SelectPostReachActivity.this.getResources().getDrawable(R.drawable.map_layer_circle_five_red));
                    }
                }
                // You can have your own calculation for progress
                seekBar.setThumb(getThumb(distanceToShow, thumbViewDistance));
                Log.e("distanceAmount", "Amount Payable due to Distance " + decimalFormat.format(distanceAmount));
                mBinding.uiSelectPostReach.txtPrice.setText(getString(R.string.pound_sign) + " " + decimalFormat.format(distanceAmount));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public double convertKmsToMiles(float kms){
        double miles = 0.621371 * kms;
        return miles;
    }

    public Drawable getThumb(String progress, View thumbView) {
        ((TextView) thumbView.findViewById(R.id.tvProgress)).setText(progress);

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(getResources(), bitmap);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        Log.e("ondestroy", "called");
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void bottomMenuClick() {
        mBinding.discovrusChat.setOnClickListener(v -> {
            /*getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.container, ChatFragment.newInstance(), "")
                    .commit();*/

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "chatFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);


        });

        mBinding.discovrusNavigation.setOnClickListener(v -> {
         /*   getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.container, FragmentNavigation.newInstance(), "")
                    .commit();*/

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "navigationFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });


        mBinding.discovrusProfile.setOnClickListener(v -> {
           /* getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.container, ProfileFragment.newInstance(), "")
                    .commit();*/

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "profileFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

        /* mBinding.discovrusNews.setOnClickListener(v -> {
         *//*  getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.container, PostManagementFragment.newInstance(), "")
                    .commit();*//*

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra("menuActivityFragment", "chatFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0,0);

        });*/

        mBinding.btnAddPost.setOnClickListener(v -> {
            onBackPressed();
        });


    }

    @Override
    public void onBackPressed() {
        mBinding.btnAddPost.setRotation(0.0f);
        super.onBackPressed();
    }

    public void bottomSheetClick() {
       /* mBinding.uiNavigation.uiBottomsheet.imgCamera.setOnClickListener(v -> {
            Log.e("camera Clicked", "-----------------");
            imgPick();
            //  openImageFromGallery();
            postType = 1;

        });
        mBinding.uiBottomsheet.imgCamText.setOnClickListener(v -> {
            Log.e("text image Clicked", "-----------------");
            imgPick();
            // openImageFromGallery();
            postType = 2;

        });
        mBinding.uiNavigation.uiBottomsheet.imgVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("video Clicked", "-----------------");
                postType = 4;

                if (Build.VERSION.SDK_INT >= 23)
                    getPermission();
                else
                    showDialogForVideo();
            }
        });
        mBinding.uiNavigation.uiBottomsheet.imgText.setOnClickListener(v -> {
            Log.e("text Clicked", "-----------------");
            postType = 3;
//            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBinding.btnAddPost.setRotation(0.0f);
            int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                    .setDuration(400);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    Bundle bundle = new Bundle();
                    bundle.putInt("postType", postType);
                    bundle.putString("imageUri", "");
                    bundle.putString("videoUri", "");

                    Intent intent = new Intent(getApplicationContext(), PostFilteringActivity.class);
                    intent.putExtra("filterFragment", "textAddingPost");
                    intent.putExtras(bundle);
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    overridePendingTransition(0, 0);
*//*
                    Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.container, TextPostFragment.newInstance(bundle), "TextPostFragment")
//                        .addToBackStack(null)
                            .commit();*//*
//            }
//            fragmentView = 2;

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

        });*/
    }

    private void getPermission() {
        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;

        int hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(this, writeExternalStorage);
        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(this, readExternalStorage);
        List<String> permissions = new ArrayList<String>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);

        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }
        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(SelectPostReachActivity.this,
                    params,
                    100);
        } else
            showDialogForVideo();
    }

    private void showDialogForVideo() {

        new AlertDialog.Builder(SelectPostReachActivity.this)
                .setTitle("Select video")
                .setMessage("Please choose video selection type")
                .setPositiveButton("CAMERA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        openVideoCapture();
                    }
                })
                .setNegativeButton("GALLERY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickFromGallery();
                    }
                })
                .show();
    }

    private void openVideoCapture() {
        Intent videoCapture = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(videoCapture, REQUEST_VIDEO_TRIMMER);
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale), REQUEST_STORAGE_READ_ACCESS_PERMISSION);
            } else {
                Intent intent = new Intent();
                intent.setTypeAndNormalize("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO_TRIMMER);
            }
        } else {
            Intent intent = new Intent();
            intent.setTypeAndNormalize("video/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO_TRIMMER);
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.permission_title_rationale));
            builder.setMessage(rationale);
            builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(SelectPostReachActivity.this, new String[]{permission}, requestCode);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(this, VideoTrimActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(this, uri));
        intent.putExtra(VIDEO_TOTAL_DURATION, getMediaDuration(uri));
        startActivityForResult(intent, 3000);
    }


    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(this, uriOfFile);
        int duration = mp.getDuration();
        return duration;
    }

}
