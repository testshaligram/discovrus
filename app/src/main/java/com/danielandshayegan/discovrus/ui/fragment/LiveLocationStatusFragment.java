package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentLiveLocationStatusBinding;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LiveLocationStatusFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LiveLocationStatusFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentLiveLocationStatusBinding mBinding;
    public LiveLocationStatusFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LiveLocationStatusFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LiveLocationStatusFragment newInstance() {
        LiveLocationStatusFragment fragment = new LiveLocationStatusFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding= DataBindingUtil.inflate(inflater,R.layout.fragment_live_location_status, container, false);
        mBinding.uiLiveLocation.toolbar.txtTitle.setText(R.string.str_title_live_location);
        mBinding.uiLiveLocation.toolbar.imgBack.setOnClickListener(view->{
            getActivity().finish();
        });
        mBinding.uiLiveLocation.switchHighlight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    setLocationOnView();
                }
                else {
                    setLocationOffView();

                }
            }
        });
        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }

    private void setLocationOnView() {
        mBinding.uiLiveLocation.txtliveSince.setText("Live Since: ");
        mBinding.uiLiveLocation.txtTimeRemainig.setText("Time Remaining: ");
        mBinding.uiLiveLocation.txtliveSinceValue.setText("15:00 ");
        mBinding.uiLiveLocation.txtTimeRemainigValue.setText("00:20 minutes ");
        mBinding.uiLiveLocation.imgLocation.setImageResource(R.drawable.ic_location_on);
    }
    private void setLocationOffView() {
        mBinding.uiLiveLocation.txtliveSince.setText("Last Live: ");
        mBinding.uiLiveLocation.txtTimeRemainig.setText("Time: ");
        mBinding.uiLiveLocation.txtliveSinceValue.setText("11 October 18 ");
        mBinding.uiLiveLocation.txtTimeRemainigValue.setText("16:00 ");
        mBinding.uiLiveLocation.imgLocation.setImageResource(R.drawable.location_off);
    }
}
